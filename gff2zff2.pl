#!/usr/bin/perl
# gff2zff2.pl

=head1 DESCRIPTION

 gff2zff2.pl  
  convert GFF to SNAP ZFF format for SNAP prediction uses

=head1 AUTHOR
  
  Don Gilbert, gilbertd@indiana.edu, 2005/2006.
  for drosophila species and daphnia genome blast annotations
   
=cut  

#use strict;
#use warnings;

my $scoreabs=$ENV{score};
my $starts=$ENV{start};
my $introns=$ENV{intron};
my $hmm=0;
my $pctscore=0;
my $scoreadj=0;

use Getopt::Long;
my $optok= GetOptions(
#   "in|gff=s", \$gff, 
#   "out|zff=s", \$zff, 
  "hmm!", \$hmm,
# "scoreabs!", \$scoreabs,
  "pctscore!", \$pctscore,
  "scoreadj=s", \$scoreadj,
  "starts!", \$starts,
  "introns!", \$introns,
  );

die <<USAGE unless($optok);
usage: gff2zff [-scoreadj=2.5 -[no]pctscore -[no]starts -[no]introns -[no]hmm] < in.gff > out.zff
USAGE

zffForXdef() unless($hmm);
zffForHmm() if($hmm);

sub zffForXdef {
  my $zcmd="ADJ"; # or "SET" ?
  my $dt="."; # or "0" ?
  my ($pid, $lpid); $pid="id1";
  while(<>){
    next unless(/^\w/);
    chomp;  
    my ($ref,$src,$type,$b,$e,$score,$strand,$shft,$attr)= split "\t";
    if($attr =~ m/(Parent|ID)=([^;\s]+)/) { $pid=$2; $pid =~ s/_[GSo]\d+$//; }
    $strand="+" if($strand eq "."); # dang
  
    if($ref ne $lref) {
      my($sb,$se,$tp)=($le,$le);
      $tp= ($lstrand eq '-') ? "Start" : "Stop";
      print join("\t", $tp,$sb,$se,$lstrand,'+50',$dt,$dt,$dt,$zcmd,$lref),"\n" if($starts and $le); 
      $le=0;
      #print ">$ref\n";
      $lref= $ref; $lpid='';
    }
    $score =~ s/\s+//g;
    if($scoreadj =~ /\d/ && $scoreadj != 0) {
      $score= $score * $scoreadj;
    } elsif($pctscore) { 
     $score=100 if($score>100); $score= $score/100;  
     $score=0.1 if ($score<0.1);
     }
    ## for Coding, use values in 0.1 .. 1,2 range ??; higher for Start/Stop
    ## for tilex data, is score<0 == Intron hint??
    next if (!$introns and $score <= 0);

    if($pid ne $lpid) {
      my($sb,$se,$tp)=($le,$le);
      $tp= ($lstrand eq '-') ? "Start" : "Stop";
      print join("\t", $tp,$sb,$se,$lstrand,'+50',$dt,$dt,$dt,$zcmd,$lref),"\n" if($starts and $le); 
      $le=0;
      print ">$ref:$pid\n";
      ($sb,$se)=($b,$b);
      $tp= ($strand eq '-') ? "Stop" : "Start";
      print join("\t", $tp,$sb,$se,$strand,'+50',$dt,$dt,$dt,$zcmd,$ref),"\n" if($starts);
      $lpid= $pid;
    }
    $tp="Coding";
    if($introns && $score<0) { $tp="Intron"; $score= -$score; } #? fixme
    print join("\t", $tp, $b,$e,$strand,'+'.$score,$dt,$dt,$dt,$zcmd,$ref),"\n";
    ($lb,$le,$lstrand,$lscore)=($b,$e,$strand,$score);
  }
}

=item hmm zff
>scaffold_4845
Einit   6571    6681    +       10.789  0       0       0       scaffold_4845-SNAP_dm.1
Eterm   6840    7199    +       11.685  0       0       2       scaffold_4845-SNAP_dm.1

=cut

sub zffForHmm {
  my ($pid, $lpid, @lv);
  my $etype="Einit";
  while(<>){
    next unless(/^\w/);
    chomp;  
    
    my @v= split "\t";
    my ($ref,$src,$type,$b,$e,$score,$strand,$shft,$attr)= @v;
    if($attr =~ m/(Parent|ID)=([^;\s]+)/) { $pid=$2; $pid =~ s/_[GSo]\d+$//; }
  
    $endgene= ($lpid ne $pid || $ref ne $lref);
    
    $score =~ s/\s+//g;
    $score = int($score); #? int($score/50); #?
    ## for Coding, use values in 0.1 .. 1,2 range ??; higher for Start/Stop
  
    $etype= "Eterm" if($etype eq "Exon" && $endgene);
    $etype= "Esngl" if($etype eq "Einit" && $endgene);
    print join("\t", $etype,$lb,$le,$lstrand,'+'.$lscore,0,0,0,$lpid,$lref),"\n" if($le); $le=0;

    if($ref ne $lref) {
      print ">$ref\n"; 
      $lref= $ref;  $lpid='';
    }
  
    next if ($score <= 0);
    if($pid ne $lpid) {
      $lpid= $pid;
      $etype="Einit";
    } else { 
      $etype="Exon"; 
    }
    ($lb,$le,$lstrand,$lscore)=($b,$e,$strand,$score);
    @lv= @v;
  }

  $etype= "Eterm" if($etype eq "Exon" && $endgene);
  $etype= "Esngl" if($etype eq "Einit" && $endgene);
  print join("\t", $etype,$lb,$le,$lstrand,'+'.$lscore,0,0,0,$lpid,$lref),"\n" if($le); $le=0;
}
