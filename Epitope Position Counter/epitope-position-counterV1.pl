#!/usr/bin/perl -w

# burkesquires
# Aug 2010

my $file = "./epitope-data-pb1-f2.tab";

open( inputData, $file ) || die "File not found\n";

my $outputfile = $file . '.out';

open outFile, ">$outputfile"
  or die "unable to open $outputfile $!";

my %positions;
my $previousID    = 0;
my $currentID     = 0;
my $currentStartPosition = 0;
my $previousStartPosition = 0;
my $currentEndPosition   = 0;
my $previousEndPosition   = 0;
my $i = 0;

foreach $line (<inputData>) {

    chomp($line);

    my @fields = split( /\t/, $line );

    $currentID     = $fields[0];
    $currentStartPosition = $fields[1];
    $currentEndPosition   = $fields[2];

    if ( ($currentID != $previousID) && ($currentStartPosition != $previousStartPosition) && ($currentEndPosition != $previousEndPosition)) {

        for ( $i = $currentStartPosition ; $i <= $currentEndPosition ; $i++ ) {

            if (exists $positions{$i}) {

                      $positions{$i} = $positions{$i} + 1;
    
              } else {

                      $positions{$i} = 1;

                }

        }

        $previousID = $currentID;
        $previousStartPosition = $currentStartPosition;
        $previousEndPosition = $currentEndPosition; 

    }

}

print outFile "Epitope Position Counter\n\n";

print outFile "Position\tNo of Epitopes at Position\n\n";

foreach my $key ( sort { $a <=> $b } keys %positions )
{
  print outFile "key: " . $key . " value: " . $positions{$key} . "\n";
}

close(inputData);
close(outFile);
