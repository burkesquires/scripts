#!/usr/bin/perl -w


  
$numArgs = $#ARGV + 1;
print "thanks, you gave me $numArgs command-line arguments.\n";

foreach $argnum (0 .. $#ARGV) {

	print "$ARGV[$argnum]\n";
	
}


#process each file in a directory
#!/usr/bin/perl -w

#opendir(DIR, ".");
#@files = readdir(DIR);
#closedir(DIR);

#foreach $file (@files) {
#   print "$file\n";
#}


open(fileNames, "influenza_NA_conversion.txt") || die "File not found\n";
open(fileTree, "influenza_NA_tree.ph") || die "File not found\n";
open(fileTreeOut, ">influenza_NA_tree_corrected.ph");

#Load the names hashs
while (<fileNames>) {

	chomp;
	
	($oldName, $newName) = split(/\t/, $_);
	
	$proteinNames{$oldName} = $newName;
	
}

#run through the tree file
#read tree file line


while (<fileTree>) {
	
	chomp;
	
	$treeString = $_;
	
	#Extract node name
	if (index($treeString, "|") > 0) {
	
		#find name in hash - using substitutions
		($oldName) = split(/:/, $treeString);
		
		$newName = "";
		
		$newName = $proteinNames{$oldName};
		
		#replace anme in tree file
		if ($newName ne "") {
			
			$oldName =~ s/\|/::/g;
		
			$treeString =~ s/\|/::/g;
			
			$treeString =~ s/$oldName/$newName/;
	
		} 
		
	}
	
	print fileTreeOut $treeString;
	
}

close(fileNames);
close(fileTree);
close(fileTreeOut);