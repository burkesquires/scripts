#!/usr/bin/perl
#
#	Created by R. Burke Squires on Dec-2012
#	Email: richard.squires@nih.gov
#	NIAID Bioinformatics and Computational Bioscience Branch (BCBB)
#	December 2012
#

use strict;
use warnings;
use diagnostics;

use Bio::SearchIO;
use Bio::Tools::Run::StandAloneBlastPlus;
use File::Basename;
use Getopt::Long;
use Pod::Usage;

if ($#ARGV == -1) {
    &usage;
}

#my %opts = ();
#GetOptions("d:i:o:p:l:r:", \%opts);
#my $database 	= $opts{'d'} || '/gpfs/bio_data/blast_db/all_virus_genome_nt';
#my $inputfile	= $opts{'i'};
#my $outputfile	= $opts{'o'} || 'results.txt';
#my $percent_id	= $opts{'p'} || 75;
#my $min_hit_length = $opts{'l'} || 50;
#my $num_results	= $opts{'r'} || 100;


#my %opts = ('outputfile' => 'results.txt');
my %opts = ();

my $outputfile = 'results.txt';
GetOptions( \%opts, 
            'db|database=s' 		=> \my $database, 
            'o|outputfile:s' 		=> \$outputfile,
            'i|inputfile=s' 		=> \my $inputfile,
			'p|percent_id:i'		=> \my $percent_id || 75,
			'l|min_hit_length:i' 	=> \my $min_hit_length || 50,
			'r|num_results:i'		=> \my $num_results || 100
    		) or die "Invalid parameters!";

my @suffixes = (".fasta",".fa");

my($filename, $directories, $suffix) = fileparse($inputfile, @suffixes);
my $blastoutputfile = $directories . $filename . ".bls";

# BLAST the sequences
my $fac = Bio::Tools::Run::StandAloneBlastPlus->new(-db_name => $database);
my $result = '';
$result = $fac->blastn( -query => $inputfile,
                        -outfile => $blastoutputfile,
                        -method_args => [ '-num_alignments' => $num_results ] );
#print 'outputfile =' . $outputfile;
open(OUTPUT,">$outputfile");

# Parse BLAST results
my $in = new Bio::SearchIO(-format => 'blast',
                           -file   => $blastoutputfile);
while( my $result = $in->next_result ) {
  ## $result is a Bio::Search::Result::ResultI compliant object
  while( my $hit = $result->next_hit ) {
    ## $hit is a Bio::Search::Hit::HitI compliant object
    while( my $hsp = $hit->next_hsp ) {
      ## $hsp is a Bio::Search::HSP::HSPI compliant object
      if( $hsp->length('total') > $min_hit_length ) {
        if ( $hsp->percent_identity >= $percent_id ) {
          	print OUTPUT "Query =\t",   $result->query_name,
            " Hit =\t",        $hit->name,
            " Length =\t",     $hsp->length('total'),
            " Percent_id =\t", $hsp->percent_identity, "\n";
        }
      }
    }  
  }
}

#-----------------------------------------------------------------------
sub usage {
    print "\nUsage: blastFASTAFile.pl\n\n";
    print "Parameters:\n\n";
    print "-db | database\t\tThe formatted database to search through\n";
    print "-i | input\t\tThe fasta file of seqeunce(s) to BLAST\n";
    print "-o | output\t\tThe BLASTresults file (Optional)\n";
    print "-p | percent_identity\tThe minimum percent identity to save from the BLAST results (Optional)\n";
    print "-l | min_hit_length\tThe minimum length for a BLAST hit to be saved (Optional)\n";
	print "-r | num_results\tThe number of results to return from each BLAST search (Optional)\n\n";
	print "  This script takes a fasta formatted file of one or more target sequences and BLASTs them against\n";
	print "a formated BLAST database. The program then returns all hits that match or surpass a minimum percent identity,\n";
	print "and a minimum hit length.\n\n";
	print "Output includes: a BLAST results file and a text file with a concatinated list of all hits.\n\n";
    print "R. Burke Squires\n";
	print "Email: richard.squires\@nih.gov\n";
	print "NIAID Bioinformatics and Computational Bioscience Branch (BCBB)\n";
    print "December 2012\n\n";
    exit;
}
#-----------------------------------------------------------------------