<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 transitional//EN"
							 "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>blast_parse_0.pl</title>
		<style type="text/css">
			<!--
			body  		{ background-color: white; }
			.progflow	{ color: blue; }
			.function	{ color: red; }
			.operator	{ color: orange; }
			.subroutine { color: darkred; }
			.variable	{ color: 990033; }
			.number  	{ color: purple; }
			.comment 	{ color: 009933; }
			.string  	{ color: 9933FF; }
			//-->
		</style>
	</head>
	<body>
		<h3>blast_parse_0.pl</h3>
		<pre>
<span class="comment">#!/usr/local/bin/perl -w</span>

<span class="comment"># Parsing BLAST reports with BioPerl's Bio::SearchIO module</span>
<span class="comment"># WI Biocomputing course - Bioinformatics for Biologists - October 2003</span>

<span class="comment"># See help at http://www.bioperl.org/HOWTOs/html/SearchIO.html for all data that can be extracted</span>

<span class="progflow">use</span> Bio::SearchIO;

<span class="comment"># Prompt the user for the file name if it's not an argument</span>
<span class="comment"># NOTE: BLAST file must be in text (not html) format</span>
<span class="progflow">if</span> (! <span class="variable">$ARGV</span>[<span class="number">0</span>])
{
   <span class="function">print</span> <span class="string">"What is the BLAST file to parse? "</span>;

   <span class="comment"># Get input and remove the newline character at the end</span>
   <span class="function">chomp</span> (<span class="variable">$inFile</span> = &#60;STDIN>);
}
<span class="progflow">else</span>
{
   <span class="variable">$inFile</span> = <span class="variable">$ARGV</span>[<span class="number">0</span>];
}

<span class="variable">$report</span> = <span class="progflow">new</span> Bio::SearchIO(
         -file=><span class="string">"$inFile"</span>,
              -format => <span class="string">"blast"</span>); 

<span class="function">print</span> <span class="string">"QueryAcc\tHitDesc\tHitSignif\tHSP_rank\t\%ID\teValue\tHSP_length\n"</span>;

<span class="comment"># Go through BLAST reports one by one              </span>
<span class="progflow">while</span>(<span class="variable">$result</span> = <span class="variable">$report</span>->next_result) 
{
   <span class="comment"># Go through each each matching sequence</span>
   <span class="progflow">while</span>(<span class="variable">$hit</span> = <span class="variable">$result</span>->next_hit) 
   { 
      <span class="comment"># Go through each each HSP for this sequence</span>
        <span class="progflow">while</span> (<span class="variable">$hsp</span> = <span class="variable">$hit</span>->next_hsp)
         { 
            <span class="comment"># Print some tab-delimited data about this HSP</span>
            
            <span class="function">print</span> <span class="variable">$result</span>->query_accession, <span class="string">"\t"</span>;
            <span class="function">print</span> <span class="variable">$hit</span>->description, <span class="string">"\t"</span>;
            <span class="function">print</span> <span class="variable">$hit</span>->significance, <span class="string">"\t"</span>;
            <span class="function">print</span> <span class="variable">$hsp</span>->rank, <span class="string">"\t"</span>;
            <span class="function">print</span> <span class="variable">$hsp</span>->percent_identity, <span class="string">"\t"</span>;
            <span class="function">print</span> <span class="variable">$hsp</span>->evalue, <span class="string">"\t"</span>;
            <span class="function">print</span> <span class="variable">$hsp</span>->hsp_length, <span class="string">"\n"</span>;
      } 
   } 
}


		</pre>
	</body>
</html>

