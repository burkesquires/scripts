#!/usr/bin/perl
#
# Script parse_blast.pl is used for parsing BLAST results.  Reports basic information about each HSP in a tab-delimited table.  
#
# R. Burke Squires
# Computational Biology Section
# Bioinformatics and Computational Biosciences Branch (BCBB)
# OCICB/OSMO/OD/NIAID/NIH
# Bethesda, MD 20892
#
# burkesquires@gmail.com
# 



use strict;
use warnings;
use diagnostics;

use Bio::SearchIO;

use File::Basename;
use Getopt::Long;
use Pod::Usage;

if ($#ARGV == 0) {
    &usage;
}

my %opts = ();
my $inputfile = "";
my $percent_id = 75;
my $min_hit_length = 50;
my $min_hsp_length = 50;
my $num_results = 100;

my $outputfile = 'results.txt';
GetOptions( \%opts, 
            'i|inputfile=s' 		=> \$inputfile,
			'o|outputfile:s' 		=> \$outputfile,
            'p|percent_id:i'		=> \$percent_id,
			'l|min_hit_length:i' 	=> \$min_hit_length,
			's|min_hsp_length:i' 	=> \$min_hsp_length,
			'r|num_results:i'		=> \$num_results
    		) or die "Invalid parameters!";

open(OUTPUT,">$outputfile");

print OUTPUT "Hit_name\tHSP_rank\tHSP_length\tHSP_Percent_identity\tHSP_evalue\tquery_start\tquery_end\tquery_length\tquery_strand\thit_start\thit_end\thit_length\thit_strand\n";

# Parse BLAST results
my $in = new Bio::SearchIO(-format => 'blast',
                           -file   => $inputfile);
print $in;
while( my $result = $in->next_result ) {
  ## $result is a Bio::Search::Result::ResultI compliant object
  while( my $hit = $result->next_hit ) {
    ## $hit is a Bio::Search::Hit::HitI compliant object
    while( my $hsp = $hit->next_hsp ) {
      ## $hsp is a Bio::Search::HSP::HSPI compliant object
      if( $hsp->length('total') > $min_hit_length ) {
        if ( $hsp->percent_identity >= $percent_id ) {
          	print OUTPUT "Query =\t",   $result->query_name,
            " Hit =\t",        $hit->name,
            " Length =\t",     $hsp->length('total'),
            " Percent_id =\t", $hsp->percent_identity, "\n";
        }
      }
    }  
  }
}


#-----------------------------------------------------------------------
sub usage {
    print "\nUsage: parse_blast_file.pl\n\n";
    print "Parameters:\n\n";
    print "-i | input\t\tThe BLAST output file\n";
    print "-o | output\t\tThe parsed BLAST results file\n";
    print "-p | percent_identity\tThe minimum percent identity to save from the BLAST results (Optional)\n";
    print "-l | min_hit_length\tThe minimum length for a BLAST hit to be saved (Optional)\n";
    print "-s | min_hsp_length\tThe minimum length for high scoring pairs to be saved (Optional)\n";
	print "-r | num_results\tThe number of results to return from each BLAST search (Optional)\n\n";
	print "  This script takes a BLAST results file and returns all hit and high scoring pari information\n";
	print "in a tab-delimited format that match or surpass a minimum percent identity and a minimum hit length.\n\n";
	print "Output fields:\n";
	print "  Hit_name, HSP_rank, HSP_length, HSP_Percent_identity, HSP_evalue, query_start, query_end, query_length, query_strand, hit_start, hit_end, hit_length, hit_strand\n\n";
    exit;
}
#-----------------------------------------------------------------------