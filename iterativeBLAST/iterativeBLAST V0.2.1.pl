#!/usr/bin/perl
#
#	Created by R. Burke Squires on Dec-2012
#	Email: richard.squires@nih.gov
#	NIAID Bioinformatics and Computational Bioscience Branch (BCBB)
#	December 2012
#

use strict;
use warnings;
use diagnostics;

use Bio::SearchIO;
use Bio::Tools::Run::StandAloneBlastPlus;
use Getopt::Long;
use Pod::Usage;

if ($#ARGV == -1) {
    &usage;
}

my $blastdb   = "/gpfs/bio_data/blast_db/all_virus_genome_nt";
my $input = "";
my $percent_ID = 75;
my $min_hit_length = 50;
my $result = GetOptions ("db=s" => \$blastdb,    # string
						"input=s"   => \$input,      # numeric
						"percent_identity=i"	=> \$percent_ID,
						'min_length=i'	=> \$min_hit_length)
						or pod2usage(2);     # string

my @suffixes = (".fasta",".fa");
my($filename, $directories, $suffix) = fileparse($input, @suffixes);
my $output = $directories . $filename . "-" . ".bls";

#$blastdb = "fludb-pre-2009";
#$input = "A-Cali-04-2009.fasta";

# BLAST the sequences
my $fac = Bio::Tools::Run::StandAloneBlastPlus->new(-db_name => $blastdb);
$result = '';
$result = $fac->blastn( -query => $input,
                        -outfile => $output,
                        -method_args => [ '-num_alignments' => 10 ] );

# Parse BLAST results
my $in = new Bio::SearchIO(-format => 'blast',
                           -file   => $output);
while( my $result = $in->next_result ) {
  ## $result is a Bio::Search::Result::ResultI compliant object
  while( my $hit = $result->next_hit ) {
    ## $hit is a Bio::Search::Hit::HitI compliant object
    while( my $hsp = $hit->next_hsp ) {
      ## $hsp is a Bio::Search::HSP::HSPI compliant object
      if( $hsp->length('total') > $min_hit_length ) {
        if ( $hsp->percent_identity >= $percent_ID ) {
          print "Query = ",   $result->query_name,
            " Hit = ",        $hit->name,
            " Length = ",     $hsp->length('total'),
            " Percent_id = ", $hsp->percent_identity, "\n";
        }
      }
    }  
  }
}

#-----------------------------------------------------------------------
sub usage {
    print "\nUsage: blastSeqFile.pl\n\n";
    print "Parameters:\n\n";
    print "-db\t\t\tThe formatted database to search through\n\n";
    print "-input\t\t\tThe fasta file of seqeunce(s) to BLAST\n\n";
    print "-percent_identity\tThe minimum percent identity to save from the BLAST results\n\n";
    print "-min_hit_length\t\tThe minimum length for a BLAST hit to be saved\n\n\n";
    print "  This script takes a previously formated BLAST database file (-db), an input file (-in, fasta format)\n";
    print "with one or more sequences in it, a minimum percent identity (-percent_identity), and a minimum length\n";
    print "of hits (-min_hit_length) and returns a BLAST results file as well as a list of the hits.\n\n\n";
    print "R. Burke Squires\n";
	print "Email: richard.squires\@nih.gov\n";
	print "NIAID Bioinformatics and Computational Bioscience Branch (BCBB)\n";
    print "December 2012\n\n";
    exit;
}
#-----------------------------------------------------------------------