#"/usr/local/bin/python3

def parse_full_path(full_path):
    import os.path
    if os.path.exists(full_path):
        (file_path, file_name_with_ext) = os.path.split(full_path)
        (file_name, extension) = os.path.splitext(file_name_with_ext)
        return file_path, file_name, extension
    else:
        return "", "", ""


def main(args):
    from Bio.Blast import NCBIXML
    from Bio.Seq import Seq
    from Bio.SeqRecord import SeqRecord
    from Bio import SeqIO
    from Bio.Alphabet import generic_protein

    result_handle = open(args.blast_file)
    blast_record = NCBIXML.read(result_handle)
    file_path, file_name, extension = parse_full_path(args.blast_file)
    output_file = "%s/%s.%s.%s" % (file_path, file_name, extension, "fasta")
    sequences = []
    for alignment in blast_record.alignments:
        seqrec = SeqRecord(Seq(alignment.hsps[0].sbjct), id=alignment.hit_id, description=alignment.hit_def)
        sequences.append(seqrec)
    SeqIO.write(sequences, output_file, "fasta")


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    files = parser.add_argument_group('files')
    files.add_argument("-i", "--blast_file", required=True, help="The BLAST XML file to be parsed")
    files.add_argument("-o", '--output_file', type=argparse.FileType('wt'), help="The parsed output file (default: input file name.fasta)")

    args = parser.parse_args()

    main(args)
