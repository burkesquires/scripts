#!/usr/bin/perl -w
#
#	Created by Burke Squires on 27-Jan-2011
#	Copyright (C) 2011 UT Southwestern  All Rights Reserved

use DBI;
use Bio::SeqIO;

# Read in fasta seqeucne file

my $seqio = Bio::SeqIO->new(-file => "influenza.fna", '-format' => 'Fasta');
	
while(my $seq = $seqio->next_seq) {

	# primary_id "gi|6092754|gb|A79849|Sequence"
	# desc = strain name

	my $sequence = $seq->seq;
	
	my @identifiers = split("|",$seq->primary_id);
	my $giAccession = $identifiers[1];
	my $gbAccession = $identifiers[3];

	# commit to svn repository
	
		# Influenza C virus (C/Yamagata/10/89) M gene for M1 protein and CM2 protein, complete cds
	my $description = $seq->desc;
	
	#what about proper strain anmes with subtype in parenthesis?
	
	$description =~ m/\((.*?)\)/;
	my $strainName = $1;
	
	if ($strainName <> "") {
		# check for the format of the strain name (3, 4, 5 slashes)
		my @attributes = split("/", $strainName);
		
	
	
		# Import each sequences into the database

		$sql = insert into fludb (giAccession, gbAccession, strain name, nucleotide sequence, year)
	}
}	




# Read in metadata file


$dbh = DBI->connect('dbi:mysql:perltest','root','password')
or die "Connection Error: $DBI::errstr\n";

  
$sql = "select * from samples";
 
$sth = $dbh->prepare($sql);
 
$sth->execute
or die "SQL Error: $DBI::errstr\n";
 
while (@row = $sth->fetchrow_array) {
	print "@row\n";
}