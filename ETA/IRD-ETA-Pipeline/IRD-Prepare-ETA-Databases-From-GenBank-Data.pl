#!/usr/bin/env perl


use strict;
use warnings;

use Bio::SeqIO;
use DBI;

my @metaData     = "";


loadDatabase();
#createBLASTDatabases();



sub loadDatabase {

my %hashMetaData;

	# Load sequence data into database
	my $dataFile = "./influenza_na.dat";
	my $seqFile  = "./influenza.fna";

	unless ( open( dataFILE, "<$dataFile" ) ) {
		print "Cannot open data file $dataFile\n";
		exit;
	}

	# unless ( open( seqFILE, "<$seqFile" ) ) {
	#	print "Cannot open data file $seqFile\n";
	#	exit;
	#}

	# influenza.fna
	# >gi|4520258|gb|AB000612|Influenza C virus (C/Yamagata/8/88) M gene for M1 protein and CM2 protein, complete cds
	# AACAATGGCACATGAAATATTAATTGCCGAAACAGAGGCATTTCTAAAAAATGTTGCTCCTGAGACCAGG

	# influenza_na.dat
	# EU521940	Human	4	H3N2	Peru	2007	1083	Influenza A Virus (A/Arequipa/FLU7200/2007(H3N2))

	# Read sequence metadata file

	my $dbh = DBI->connect('DBI:Mysql:flu-db', 'burkesquires', 'password') || die "Could not connect to database: $DBI::errstr";
	
	while ( my $record = <dataFILE> ) {

		@metaData = split "\t", $record;

					
		# fields: idstrain, type, host, location, isolate, isolateyear, hostcategory, nucleotidesequence, subtype-h, subtype-n, accession

		# EU521940	Human	4	H3N2	Peru	2007	1083	Influenza A Virus (A/Arequipa/FLU7200/2007(H3N2))

 		$dbh->do("INSERT INTO sequence (idstrain, type, host, location, isolate, isolateyear, hostcategory, nucleotidesequence, subtype-h, subtype-n, accession) 
		VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)","", "", $metaData[1], $metaData[4], "", $metaData[5], $metaData[1], "", "", $metaData[0]);
		
		
	}
		
		my $inseq = Bio::SeqIO->new(-file   => "<$seqFile",-format => "fasta");

    		# Now that we have a seq stream,
    		# we need to tell it to give us a $seq.
    		# We do this using the 'next_seq' method of SeqIO.

		
		# Delete everything in the database
	
		while (my $seq = $inseq->next_seq) {
    
    			my @metaData = $hashMetaData{$seq->accession_number};
    
    			my $host = "";
			my $isolate =""; 
			#$metaData[5], 
			#$metaData[4], 
			#$seq->sequence, 
			#$subtype-h, 
			#$subtype-n, 
			#$seq->accession_number				
				
				
			# fields: idstrain, type, host, location, isolate, isolateyear, 
			# hostcategory, nucleotidesequence, subtype-h, subtype-n, accession

			# EU521940	Human	4	H3N2	Peru	2007	1083	Influenza A Virus (A/Arequipa/FLU7200/2007(H3N2))


	
	 		#$dbh->do("INSERT INTO sequence (idstrain, type, host, location, isolate, isolateyear, hostcategory, nucleotidesequence, subtype-h, subtype-n, accession) 
			#VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",'DEFAULT', $metaData[1], $host, $isolate, $metaData[5], $metaData[4], $seq->sequence, $subtype-h, $subtype-n, $seq->accession_number);


	
	
	
	}	
	
	$dbh->disconnect();	
	
}

sub createBLASTDatabases {

	# loop through isolation years

	# Select sequences for previous year to earliest year

	# Save FASTA file

	# create binary BLAST database

	sub readdatabase {
		my ($dbfile) = @_;

		# Open the database file

		unless ( open( DATAFILE, "<$dbfile" ) ) {
			print "Cannot open data file $dbfile\n";
			exit;
		}

		my %database = ();

		# Set input separator.
		# This allows us to get an entire record as one scalar value
		# A blank line is just a newline following another newline.

		$/ = "\n\n";

		# Read the data

		while ( my $record = <DATAFILE> ) {

			#
			# Get the first line as a key, and the remaining lines as the value
			#

			# Turn the scalar $record back into an array of lines
			# to get the key
			my @lines = split( /\n/, $record );
			my $key = shift @lines;

			# And turn the remaining array into a scalar for storing
			# as a value in the hash
			my $value = join( "\n", @lines ) . "\n";

			$database{$key} = $value;
		}

		close(DATAFILE);

		# reset input separator to normal default value
		$/ = "\n";

		return %database;
	}
}

