#!/usr/bin/perl -w
use strict;
use Bio::AlignIO;

# August 26, 2009
# This script with compute a quantitative measure of the similarity of sequence within 
# a multiple sequence alignment.
# NOTE: If duplicates are present this script will not work; all results are wrong.
# So, remove duplicates and procced.


# read in alignment file

my $filename = '/Users/burkesquires/Desktop/MSA analysis 201003/ET-MSA-HA.align';
my $str = Bio::AlignIO->new(-file => $filename);
my $aln = $str->next_aln();

my $seqlength = $aln->length;
my $numseqs = $aln->no_sequences; #This is the number of sequecnes in the alignment

#create an array of arrays to hold resides
my @array;

# loop though alignment
foreach my $seq ($aln->each_seq) {

	push @array, [split(//,$seq->seq)];

}

# Correct for space and the beginning and at the ned of sequecnes.

my $i=0;

foreach my $arrayTemp (@array) {
	my $j=0;
	
	foreach my $cell (@$arrayTemp) {
	    
	    #print "\$i = $i, \$j = $j, $foo[$i][$j]\n";
    	if ($array[$i][$j] eq '-') { 
    		$array[$i][$j] = ' '; 
    	}
    	$j++;
  }  $i++;
}

# find columns of differing amino acids

my @rawScores;
my @adjustedScores;
my $totalRawScore = 0;
my $totalAdjustedScore = 0;
my $lastResidue;

#loop though the columns
for (my $column = 0; $column < $seqlength; $column++) {

	my $previousResidue = '';
	my $tempScore = 0;

	#loop through each sequence
	for (my $sequence = 0; $sequence < $numseqs; $sequence++) {
	
		#we now ahve access to a column of residues;
		# we want to chack to changes in the residues
		
		if ($previousResidue eq '') { 
				
			$previousResidue = $array[$sequence][$column];
			
		} else {
			
			my $currentResidue = $array[$sequence][$column];
			
			if ($currentResidue eq $previousResidue) {
			
				#do nothing
			
			} elsif (($previousResidue ne ' ')  && ($currentResidue eq ' ')) {
			
				$lastResidue = $previousResidue;
			
			} elsif (($previousResidue eq ' ')  && ($currentResidue ne ' ')) {
			
				if ($lastResidue eq '') {
				
						$previousResidue = $currentResidue;
				
				} elsif ($lastResidue ne $currentResidue) {

						$tempScore++;
				
						$previousResidue = $currentResidue;

				}			
			
			} elsif ($currentResidue eq ' ') {
				
				$previousResidue = ' ';
			
			} else {
			
				# quantitate differences

				$tempScore++;
				
				$previousResidue = $currentResidue;
			
			}
		}
	}
	
	push @rawScores, $tempScore;
	
	push @adjustedScores, ($tempScore / $numseqs);

	$totalRawScore = $totalRawScore + $tempScore;
	
	$totalAdjustedScore = $totalAdjustedScore + ($tempScore / $numseqs);
	
}

$filename = $filename . '.out';

open outFile, '>' . $filename or die $!;

# print report

print outFile $filename . "\n\n";

print outFile 'Total Raw Score: ' . $totalRawScore . "\n" or die $!;

print outFile 'Total Normalized Score: ' . $totalAdjustedScore . "\n\n" or die $!;

my $columnCount = @rawScores;

print outFile "Num. of columns: " . $columnCount . " (" . $seqlength . ")\n" or die $!;

print outFile "Num. of sequences: " . $numseqs . "\n\n" or die $!;

print outFile "Column scores:\n" . join( ',', @rawScores ) or die $!;

print outFile "\n\nAdjusted scores:\n" . join( ',', @adjustedScores ) or die $!;



close outFile or die $!;