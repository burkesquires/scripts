##########################################################
# This program was designed for combine a serious of cluster #
# files into a single file with cluster ID added     \   #
# Author Megan Kong 3/6/2006                             #
##########################################################

#!/usr/bin/perl -w


#read in two file names
unless (open (FILES, "config.txt")) {
	print "Can not open file \"config.txt\" ! \n";
	exit;
}

my @files=();


while (my $filename = <FILES>) {
	push @files, $filename;
}

close (FILES);

my $outputfile="combined_RowNormlized_Clusters.txt";

unless(open(OUTPUT, ">$outputfile")){
	print "Can not open file \"outputfile\" ! \n";
	exit;
}



##process 20 pairs of files

for (my $x = 0; $x <= $#files; $x++){

## assign file name to variables

my $filename1 = $files[$x];

print $filename1,"\n";

my @data = ();

open(DATAFILE, $filename1);

unless(open(DATAFILE, $filename1)){
	die("Could not open file $filename1\n");
	exit;
}


while (my $rec = <DATAFILE>) {
	push  @data, $rec;
}
close(DATAFILE);


my $total_record1;
for (my $m = 0; $m <= $#data; $m++){
    my $rec = $data[$m];
   print OUTPUT $rec;
}
#print OUTPUT "\n";

#prnt the cluster indicator
for (my $m = 0; $m <= 47; $m++){
print OUTPUT ($x+2)/10, "\t";
}
print OUTPUT "\n";
} ##end of one iteration


close(OUTPUT);

exit;
