#!/usr/bin/env perl
$|=1;
use strict;
use warnings;
use Bio::SearchIO;
use Getopt::Long;
use aomisc;		#Custom Perl module with some useful subroutines, by Andrew Oler 

#Add use lib statement to assume there is a directory at the same level as bin in which the script is run, called 'lib'
use FindBin;
use lib "$FindBin::Bin/../lib";
use lib "$FindBin::Bin";



# Script parse_blast.pl is used for parsing BLAST results.  Reports basic information about each HSP in a tab-delimited table.  

# Andrew J. Oler, PhD
# Computational Biology Section
# Bioinformatics and Computational Biosciences Branch (BCBB)
# OCICB/OSMO/OD/NIAID/NIH
# Bethesda, MD 20892
#
# andrew.oler@gmail.com
# 
#This package is free software; you can redistribute it and/or modify
#it under the terms of the GPL (either version 1, or at your option,
#any later version) or the Artistic License 2.0.

my $min_hsp_length = 0;

GetOptions( 'min_hsp_length=s' => \$min_hsp_length,);
my $usage="
parse_blast.pl [options] <blast_report1.txt> <blast_report2.txt> <etc.>
OPTIONS
--min_hsp_length	Default = 0.
";

die $usage unless $ARGV[0];
my $files = $ARGV[0];
if (scalar(@ARGV)>1){	#This will allow you to use wildcard to pass files to the script from the command line. e.g, script.pl *.txt, and it will run through each.
	$files = join ",", @ARGV;
}
my @files = get_files($files);

print "#File_name\tHit_name\tHSP_rank\tHSP_length\tHSP_Percent_identity\tHSP_evalue\tquery_start\tquery_end\tquery_length\tquery_strand\thit_start\thit_end\thit_length\thit_strand\n";
foreach my $file (@files){
	my $searchio = Bio::SearchIO->new( 	-format => 'blast',
						-file => $file,
						-report_type => 'blastn'	
	);
	while (my $result = $searchio->next_result() ){
		while (my $hit = $result->next_hit ){
			# process the Bio::Search::Hit::HitI object	http://search.cpan.org/~cjfields/BioPerl-1.6.901/Bio/Search/Hit/HitI.pm
			my $hit_name = $hit->name();
			HSP: while (my $hsp = $hit->next_hsp ){
				#process the Bio::Search::HSP::HSPI object	http://search.cpan.org/~cjfields/BioPerl-1.6.901/Bio/Search/HSP/HSPI.pm
				my $rank = $hsp->rank;
				my $total_length = $hsp->length('total');
				next HSP if ($total_length < $min_hsp_length);
				my $percent_identity = $hsp->percent_identity();
				my $evalue = $hsp->evalue();
				print "$file\t$hit_name\t$rank\t$total_length\t$percent_identity\t$evalue";
				foreach my $type (qw(query hit)){
					my ($start,$end) = $hsp->range($type);
					my $length = $hsp->length($type);
					my $strand = $hsp->strand($type);
					print "\t$start\t$end\t$length\t$strand";
				}
				print "\n";
			}
		}
	}
}
