#!/usr/bin/perl

#use warnings;

print "Hello, World...\n";


############# Parameters

my $HLA_Locus = "DRB1";
#my $HLA_Locus = "Cw";
my $Ref_HLA_Locus = "DRB1";
#my $Ref_HLA_Locus = "Cw";
my $Species = "Hsa";
my $genesys = "HLA";
#my $Locus = "HLA-DQB1";
my $Locus = "HLA-".$HLA_Locus;

my $Seqfeatlabel = $Species."_".$Locus;
my $Seqfeatname = $Seqfeatlabel."_SF";
my $Typename = "VT";
my $Alleleseqfeat = $Seqfeatlabel."_allele";
my $alleledigits = 4;
my $reftype = 1;

my $alignrefallele = "010101";
my $ref4digallele = "0101";
my $fullalignrefallele = $Ref_HLA_Locus."*".$alignrefallele;
my $fullref4digallele = $Ref_HLA_Locus."*".$ref4digallele;
my $metafullrefallele = quotemeta $fullalignrefallele;
my $metafullref4digallele =	quotemeta $fullref4digallele;


my $vardefdelim = "_";
my $motifdelim = "_";
#my $seqposfrmtdelim = " .. ";
my $seqposfrmtdelim = "..";
my $seqposdelim = ", ";
my $allelegrpdelim = ", ";
my $featnamedelim = "; ";
my $feattypedelim = "; ";
my $featnodelim = "; ";

my $tempmetastr;

$vardefmetadelim = quotemeta $vardefdelim;
$motifmetadelim = quotemeta $motifdelim;
$seqposfrmtmetadelim = quotemeta $seqposfrmtdelim;
$seqposmetadelim = quotemeta $seqposdelim;
$allelegrpmetadelim = quotemeta $allelegrpdelim;
$featnamemetadelim = quotemeta $featnamedelim;
$feattypemetadelim = quotemeta $feattypedelim;
$featnometadelim = quotemeta $featnodelim;

#my $Alleleseqfeat = "Allele";
my $tempallelect = 0;
#my $alignref4digallele = 0501;
#my $alleleseqfeat = "Allele";
#my $alleleseqfeat = $HLA_Locus." Allele";
#my $alleleseqfeat = $Seqfeatlabel."_Allele";
#my $Typename = "Type ";

#my $Ver = "25Apr08";
#my $Ver = "16Jan09";
my $Ver = "4Feb09";
#my $Ver = "25Apr2008";
#my $alignVer = "2-23-0";
my $alignVer = "2-24-0";
$prevalignVer = "2-24-0";
#my $customVer = "1";

####### In matprotlen and others below consider the ref allele's del positions as 1 position
###### align matprotseqst considers each del position as 1 position
my $matprotlen = 237;
my $alignprotlen = 266;
#my $matprotlen = 191;
#my $matprotlen = 341;
my @delposits = ();
#my @delposits = (226);
#my @delposits = (295);

####### Using the starting no. not comp. ref. nos.
my $signalseqst = 1;
my $signalseqed = 29;
my $matprotseqst = 30;

#my @customPosits = (1..$matprotlen);
my @customPosits = (1..$alignprotlen);
#my @customPosits = (1..$alignprotlen);

#my $custom1 = "IndivPos";
#my $custom2 = "Mat_alleles";
#my $custom3 = "MatprotSeq";
my $custom = "Custom";

my $insertion = "ins";
my $deletion = "del";
my $insdel = "insdel";


#my $Sqftname = $HLA_Locus." mature protein position";

#my @Sqftname = ("Allele", "mature protein", "mature protein position");
my $seqftstartct = 1;
my $startct = 1;
$possplitthresh = 4;
#my $Sqftname = "mature protein position";

########## Using comp. col nos.

#my $seqlineallelecol = 0;
#my $seqlinematseqcol = 1;
#my $seqlinevardefseqcol = 2;
#my $alignsetidentfrcol = 0;
#my $alignsetidentfr = "Prot";

my $featlineallelecol = 0;
my $featlinematseqcol = 1;
my $featlinevardefcol = 2;

my $SFfilenamecol = 0;
my $SFfileseqfeattypcol = 1;
my $SFfileposcol = 2;
########## The order of the feature types are the same as the order of printing the featuretypes in the seqfeature report
#my @Featuretypes = ("Structural", "Functional", "Evolutionary - Single polymorphic amino acids", "Structural_Functional Combination", "Functional_Evolutionary Combination", "Structural_Evolutionary Combination", "Structural - Complete protein", "Structural - Domain","Structural - Secondary structure motif");
#my @Featuretypes = ("Structural", "Functional", "Evolutionary", "Structural_Functional Combination", "Functional_Evolutionary Combination", "Structural_Evolutionary Combination", "Structural - Complete protein", "Structural - Domain", "Structural - Secondary structure motif", "Evolutionary - Single polymorphic amino acids");
my @Sqftname = ($Seqfeatlabel."_allele", $Seqfeatlabel."_mature protein", $Seqfeatlabel."_variant position", $Seqfeatlabel."_indel position", $Seqfeatlabel."_full-length protein", $Seqfeatlabel."_signal peptide");
my @Featuretypes = ("Structural", "Functional", "Sequence Alteration", "Structural_Functional Combination", "Functional_Sequence Alteration Combination", "Structural_Sequence Alteration Combination", "Structural - Complete protein", "Structural - Cleaved peptide region", "Structural - Domain", "Structural - Secondary structure motif", "Sequence Alteration - Single amino acid variation", "Sequence Alteration - Insertions and Deletions");
#my @Featuretypesz = ("Structural", "Functional", "Structural_Functional Combination", "Structural - variant location", "Structural - Complete protein", "Structural - Domain","Structural - Secondary structure motif");
#my %featuretypeorders = {};
#my @Majortypes = ("Structural", "Functional", "Evolutionary");
my @Majortypes = ("Structural", "Functional", "Sequence Alteration");

############# The col nos are the computational friendly codes 

my $defsffilenamecol = 1;
my $defsffileseqfeattypcol = 2;
my $defsffileallelegrpcol = 3;
my $defsffilemotifcol = 4;
my $defsffilevarnttypcol = 5;
my $defsffileposcol = 6;
my $defsffilevardefcol = 7;
my %defseqfeatflag;
my %preasssfvtflag;

my $defTypelabel = "Type ";
my $unkmotiflabel = "Unknown Motif Sequence";
my $unktypelabel = "Type Unknown";

my $preassTypefrmt = $Seqfeatname."[\\d a-zA-Z]+"."_"."([\\d a-zA-Z]+)";
my $preassunkmotiflabel = "Unknown Motif Sequence";
my $preassunktypelabel = "Type Unknown";
my $preassSFfrmt = $Seqfeatname."("."[\\d a-zA-Z]+".")"."_"."[\\d a-zA-Z]+";

my $defavfilenamerow = 0;
my $defavseqfeatnorow = 0;

my $defavfilenamecol = 1;

my $defavfileallelecol = 0;
my $defavfilemotifcol = 1;
my $defavfilevarnttypcol = 2;
my $defavfilevardefcol = 3;

my ($preasssfnovtcol, $preasssfnosfnocol, $preasssfnoposcol, $preasssfnonamecol, $preasssfnomotifcol) = (6, 6, 1, 2, 5);

my @varntflag;
$varntflag[0] = "Non-Variant";
$varntflag[1] = "Variant";


#my @alignsymbs = ("A-Z", "-", ".", "*");
my %alignsymborder = ("-" => 2, "." => 3, "*" => 4);

my $alphabet;

@alphabets = 'A'..'Z';



my $nullcode = "N";


my $Alleles_CustSqft;
my $Motif_CustSqftType;
my $Alleles_CustUnkSqft;
my $Alleles_CustSqftType;
my $Alleles_Posamino;
my $Alleles_PosTypes;
my $Vardef_CustSqftType;
my $Alleles_Vardefunits;
my $allelesqftVardef;
my $allelesqftMotif;



my %possfvarntflag;
my %possfoccurs;
my %possfredundcys;
my %sortdtempseqpos;
my %sortdvarnttempseqposits;
my %sortdmodfdseqposits;
my %sortdmodfdvarntseqposits;


my $possfvarposits;

my @inpfiles;
my @outfiles;

######## Input files
#$inpfiles[0] = $HLA_Locus."_prot_tabdelim_".$alignVer.".txt";
#$inpfiles[0] = "Genrtd_".$Locus."_Alleles_MatProt_vardef_delim seqs_".$alignVer;
#$inpfiles[0] = "Genrtd_".$Locus."_Alleles_MatProt_vardef_delim seqs_".$alignVer.".txt";
$inpfiles[0] = "Genrtd_".$Locus."_Alleles_MatProt_vardef_delim seqs_".$alignVer;
#$inpfiles[0] = "Genrtd_".$HLA_Locus."_Alleles_SeqFeat_Varnts_".$Ver." Matrix";
#$inpfiles[0] = "Genrtd_".$HLA_Locus."_Alleles_SeqFeat_Varnts_".$Ver." Matrix";
#$inpfiles[1] = "Genrtd_".$Locus."_SeqFeats_inc_IndivPos_".$Ver;
#$inpfiles[1] = "Genrtd_".$Locus."_SeqFeats_inc_IndivPos_".$Ver.".txt";
$inpfiles[1] = "Genrtd_".$Locus."_SeqFeats_inc_IndivPos_".$Ver;
#$inpfiles[1] = "Genrtd_".$Locus."_".$Custom."_SeqFeats".$customVer;
#$inpfiles[2] = "Genrtd_".$Locus."_Defined SeqFeats_".$Ver.".txt";
#$inpfiles[3] = "Genrtd_".$Locus."_Defined Allelevarnts_".$Ver.".txt";
#$inpfiles[2] = "Genrtd_".$Locus."_Defined SeqFeats_".$Ver;
#$inpfiles[3] = "Genrtd_".$Locus."_Defined Allelevarnts_".$Ver;
$inpfiles[2] = "Genrtd_IndivPos_Mat_alleles_".$Locus."_Motif_Variants_".$alignVer;
$inpfiles[3] = "Genrtd_IndivPos_Mat_alleles_".$Locus."_Allele_Variants_".$alignVer;

$inpfiles[4] = "PrevGenrtd_Custom_HLA-DRB1_Motif_Variants_2-24-0.txt";
#$inpfiles[4] = "Genrtd_Custom_HLA-DQB1_Motif_Variants_2-24-0_04Feb09";

######## Output files

#$outfiles[0] = "Genrtd_".$custom."_".$HLA_Locus."_Motif_Variants";
#$outfiles[1] = "Genrtd_".$custom."_".$HLA_Locus."_Allele_Variants";
#$outfiles[2] = "Genrtd_".$custom."_".$HLA_Locus."_Unknown and Non Variants";
#$outfiles[3] = "Genrtd_".$HLA_Locus."_".$custom."_Seqfeatures";

$outfiles[0] = "Genrtd_".$custom."_".$Locus."_Motif_Variants_".$alignVer;
$outfiles[1] = "Genrtd_".$custom."_".$Locus."_Allele_Variants_".$alignVer;
$outfiles[2] = "Genrtd_".$custom."_".$Locus."_Unknown and Non Variants_".$alignVer;
$outfiles[3] = "Genrtd_".$Locus."_".$custom."_Seqfeatures_".$Ver;
$outfiles[4] = "Debug_Report";
$outfiles[5] = "Log_Report_".$Locus."_".$custom;

$outfiles[6] = $Locus."_SeqFeat Report_inc_IndivPos_PosSF_".$Ver;
$outfiles[7] = $Locus."_SeqFeat VarTypes Report_inc_IndivPos_".$Ver;
$outfiles[8] = $Locus."_PosSeqFeat VarTypes Report_inc_IndivPos_".$Ver;
$outfiles[9] = $Locus."_IndivPos_Posseqfeat Occurance Report_".$Ver;
$outfiles[10] = $Locus."_IndivPos_Posseqfeat Redundancy Report_".$Ver;

#$outfiles[6] = "Genrtd_".$custom3."_".$HLA_Locus."_Allele_Variants_".$alignVer;

#my $tempfilename = "Genrtd_".$custom."_".$HLA_Locus."_Motif_Variants";
#my $tempfilename = "Genrtd_".$custom."_".$HLA_Locus."_Sqft_Variants";

#open SqftVarntsfile, ">$otufiles[0]";
#print SqftVarntsfile "Locus Name\tHLA Protein Feature Name\tHLA Protein Feature Type\tAllele Group\tMotif\tVariant Type\tFeature Location - Amino Acid Positions\tVariant Type Definition\n";

#open AlleleVarntsfile, ">$outfiles[1]";
#print AlleleVarntsfile "HLA Allele Name";

#$tempfilename = "Genrtd_".$custom."_".$HLA_Locus."_Unknown and Non Variants";
#open Unk_NonVarntsfile, ">$outfiles[2]";
#print Unk_NonVarntsfile "Locus Name\tAmino Acid Position\tAmino Acid Residue\tAllele Group\n";


#my @customPosits = (1..192);

#my @customPosits = (1..341);

open debug, ">$outfiles[4]";
open logrept, ">$outfiles[5]";
#open debug, ">Debug_Report";



#foreach $alphabet (@alphabets) 
foreach $alphabet ('A'..'Z') 
	{
		##print debug "\n$alphabet\tBefore: \t$alignsymborder{$alphabet}";
		$alignsymborder{$alphabet} = 1;
		##print debug "\n$alphabet\tAfter: \t$alignsymborder{$alphabet}";
	}

#---------------------------------------------------------
sub max
	{
		my ($max_so_far) = shift @_;
		foreach  (@_) 
			{
				if ($_ > $max_so_far) 
					{
						$max_so_far = $_;
					}
			}
		$max_so_far;
	}

sub by_number
{
	if (($a*1) < ($b*1)) { -1 } elsif (($a*1) >= ($b*1)) { 1 } else { 0 }
}


sub Splitrow
{
	my @params = @_;
	my $i = 0;
	my $temprow = $params[0];
	
	my $tempdelim = quotemeta $params[1];
	#print debug "\nLine 163: tempdelim = $tempdelim";
	#my @splitrows = split /$params[1]/, $temprow;
	#my @Rowsplits = split /$params[1]/, $temprow;
	my @Rowsplits = split /$tempdelim/, $temprow;
	
	foreach $splits (@Rowsplits) 
		{
				$Rowsplits[$i] = Trim($Rowsplits[$i]); 
				$i++;
		}
@Rowsplits;
}

sub Trim
{
	my $term = $_[0];
	$_ = $term;
	s/^ *//;
	s/ *$//;
	##print debug "In the Seq imgt while and Seqsplits for loop, before mod Seqsplits = $Seqsplits\n";
	##print debug "In the Seq imgt while and Seqsplits for loop, currSeqsplits = $_ \n";
	$trimterm = $_;
	$trimterm;
}

#*********************************************************
#---------------------------------------------------------


#Func Motif Check start*****************************

sub Motifchk
{
	my $motif = $_[0];
	##print debug "\nInside Motifchk:\tMotif = $motif";
	my $motifflag = 0;
	if ($motif =~ /^([A-Z]*)$/i) 
		{
				$motifflag = 1;
		}
	##print debug "\nInside Motifchk:\tMotifflag = $motifflag";
	$motifflag;
}

sub MotifPoschk
{
	my @motif = @_;
	##print debug "\nInside MotifPoschk:\tMotif[0] = $motif[0]\tMotif[1] = $motif[1]";
	my $motifflag = 0;
	if ($motif =~ /^[A-Z]$/i) 
		{
				$motifflag = 1;
		}
	##print debug "\nInside Motifchk:\tMotifflag = $motifflag";
	$motifflag;
}

#End Func Motif check*******************************
#Func Get Positions*******************************

sub GetPosits
{

my $Posit = $_[0];
my $delim1 = $_[1];
my $delim2 = $_[2];
my $Pos;
my $tempdelim1 = quotemeta $delim1;
my $tempdelim2 = quotemeta $delim2;

#my @Posits = Splitrow ($Posit, ", ");
my @Posits = Splitrow ($Posit, $delim2);
my $posct = 0;

my @finPosits;

foreach $Pos (@Posits) 
	{	
		
		#if ($Pos =~ /-/) 
		if ($Pos =~ /$tempdelim1/i) 
			{
				#$temp = $Pos;
				#my $temp =~ s/ *- */../g;
				#my @tempposits = Splitrow ($Pos, "-");
				my @tempposits = Splitrow ($Pos, $delim1);
				#my $temp = join "..", @tempposits;
				#my $temp = join ", ", $tempposits[0]..$tempposits[1];
#				my $temp = join $delim2, $tempposits[0]..$tempposits[1];
				
				push @finPosits, ($tempposits[0]..$tempposits[1]);
				print debug "\nLine 184: In getposits \tTemposits = $tempposits[0]\ttempposits[1] = $tempposits[1]\tfinposits in the Getposits function: = ",(@finPosits);
#				print debug "\nLine 185: In getposits befor pop\tPosits = ",(@Posits);
#				$Posits[$posct] = $temp;
#				pop @Posits;
#				print debug "\nLine 184: In getposits After pop\tPosits = ",(@Posits);
#				push @Posits, $tempposits[0]..$tempposits[1];
				
#				print debug "\nLine 189: In getposits after push\tPosits in the Getposits function:\t@Posits";
			}
		else
			{
				push @finPosits, $Pos;
			}
		$posct++;
	}
#@Posits;
@finPosits;
}

#End Get Positions*******************************

#Func Format seqpos*******************************
				
sub Formatseqpos
	{
		#my @params = @_;
		my ($delim1, $frmtthresh, $delim2, $givendelref, @tempseqposits) = @_;
		@givendelposits = @$givendelref;
		#my (@tempseqposits, $delim1, $frmtthresh, @givendelposits, $delim2) = @_;
		my $tempdelim1 = quotemeta $delim1;
		my $tempdelim2 = quotemeta $delim2;

		print debug "\nLine 179: In formatseqpos beginning\tdelim1 = $delim1\tdelim2 = $delim2\tfrmtthresh = $frmtthresh\ttempseqposits = @tempseqposits\tgivendelposits = @givendelposits";
		#my $Posit = $_[0];
		my $insertion = "ins";
		my $deletion = "del";
		my $insdel = "insdel";
		
		my $Pos;
		#my $delim1 = ", ";
 		
		#my $delim1 = $params[1];
		#my $delim2 = $params[4];
		#my @givendelposits = @{$params[3]};
		#my $frmtthresh = $params[2];
		#my @	tempseqposits =  @{$params[0]};
		
		#my @Posits = Splitrow (@tempseqposits, $delim1);
		
		my $tempseqpos = join $delim1, @tempseqposits;
		my $temptempseqpos = $tempseqpos;

		$tempseqpos = $delim1.($tempseqpos).",";	
		$tempseqpos =~ s/$tempdelim1[0-9]+($deletion),/,/gi;
		$tempseqpos =~ s/$tempdelim1[0-9]+($insertion),/,/gi;
		$tempseqpos =~ s/$tempdelim1[0-9]+($insdel),/,/gi;
		
		$tempseqpos =~ s/^($tempdelim1)//i;
		$tempseqpos =~ s/(,)$//i;
		
		my $tempfrmtseqpos = join ", ", ($tempseqposits[0]..$tempseqposits[-1]);
		$tempfrmtseqpos = ", ".($tempfrmtseqpos).", ";
		$tempfrmtseqpos =~ s/, (0), /, /i;
		$tempfrmtseqpos =~ s/^(, )//i;
		$tempfrmtseqpos =~ s/(, )$//i;
		$tempfrmtseqpos =~ s/, /$delim1/gi;
		print debug "\nLine 184: In formatseqpos before formatted tempfrmtseqpos = \t$tempfrmtseqpos\ttempseqpos = $tempseqpos";

		
		
		if (($tempseqpos eq $tempfrmtseqpos) && (@tempseqposits > $frmtthresh))
			{
				$tempfrmtseqpos = $tempseqposits[0].($delim2).($tempseqposits[-1]);
			}
		else
			{
				$tempfrmtseqpos = $temptempseqpos;
			}
		
		#$tempfrmtseqpos;
		
		$tempseqpos = $delim1.$tempseqpos.",";
		my $tempdelposit;
		my $delpositflag = 0;
		print debug "\nLine 550: Outside the tempdelposit loop for tempseqpos = $tempseqpos";

		################### Check for any del positions and include them in the tempseqpos
		#my $tempseqposit;
		#foreach $tempseqposit (@tempseqposits) 
		#	{
		#	}
#
#		foreach $tempdelposit (@givendelposits) 
#			{
#				#if ($tempseqpos =~ /$tempdelim1$tempdelposit$deletion,/i) 
#				if ($tempseqpos =~ /$tempdelim1$tempdelposit,/i) 
#					{
#						$tempseqpos =~ s/($delim1($tempdelposit),)/$delim1\2$delim1\2$deletion,/i;
#						#$tempfrmtseqpos =~ s/($tempdelim1($tempdelposit),)/$delim1$2$delim1$2$deletion,/i;
#						$delpositflag = 1;
#						#print debug "\nLine 346: Inside the formatseqpos, tempdelposit loop for tempdelposit = $tempdelposit\ttempseqpos = $tempseqpos\tMatch = $1\t$2\$3";
#					}		
#			}		
#
#
#
#		$tempseqpos =~ s/^($tempdelim1)//i;
#		$tempseqpos =~ s/(,)$//i;
		
		$tempseqpos = $temptempseqpos;

		################### End of Check for any del positions and include them in the tempseqpos
		print debug "\nLine 244: In formatseqpos ending\ttempfrmtseqpos = $tempfrmtseqpos\ttempseqpos = $tempseqpos\tdelpositflag = $delpositflag";
		($tempfrmtseqpos, $tempseqpos, $delpositflag);
	}
				
#End Func Format seqpos*******************************


#Func Getseqpos*******************************
#my $tempseqpositsref;
sub Getseqpos
	{
		my ($frmtseqpos, $delim2, $delim1, @givendelposits) = @_;
		#my @params = @_;
		
		#my $Posit = $_[0];
		my $insertion = "ins";
		my $deletion = "del";
		my $insdel = "insdel";
		
		my $tempdelim1 = quotemeta $delim1;
		my $tempdelim2 = quotemeta $delim2;
		
		my $Pos;
		#my $delim1 = ", ";
		#my $delim2 = $params[1];
		#my $delim1 = $params[3];
		#my @givendelposits = @{$params[2]};
		#my $frmtthresh = $params[2];
		#my $frmtseqpos = $params[0];
		#$frmtseqpos =~ /^([0-9]+) ?$delim2 ?([0-9]+)$/i;
		#my @tempseqposits =  (($1)..($2));
		
		my @tempseqposits =  GetPosits($frmtseqpos, $delim2, $delim1);
		
		#my $tempseqpos = join $delim1, @tempseqposits;
		my $tempseqpos = join ", ", @tempseqposits;
		
		$tempseqpos = ", ".($tempseqpos).", ";
		$tempseqpos =~ s/, (0), /, /i;
		$tempseqpos =~ s/^(, )//i;
		$tempseqpos =~ s/(, )$//i;
		$tempseqpos =~ s/, /$delim1/gi;
		
		print debug "\nLine 275: In Getseqpos beginning\ttempseqposits = @tempseqposits\tgivendelposits = @givendelposits\ttempseqpos = $tempseqpos";

		$tempseqpos = $delim1.$tempseqpos.",";
		my $tempdelposit;
		my $delpositflag = 0;
		#print debug "\nLine 550: Outside the tempdelposit loop for tempseqpos = $tempseqpos";

		################### Check for any del positions and include them in the tempseqpos
		#my $tempseqposit;
		#foreach $tempseqposit (@tempseqposits) 
		#	{
		foreach $tempdelposit (@givendelposits) 
			{
				if (($tempseqpos =~ /($tempdelim1($tempdelposit),)$/i) || ($tempseqpos =~ /^($tempdelim1($tempdelposit),)$/i) )
					{
						next;
					}
				elsif ($tempseqpos =~ /$tempdelim1$tempdelposit,/i) 
					{
						#$tempseqpos =~ s/($delim1($tempdelposit),)/$delim1\2$delim1\2$deletion,/i;
						$tempseqpos =~ s/($tempdelim1($tempdelposit),)/$delim1$2$delim1$2$deletion,/i;
						$delpositflag = 1;
						#print debug "\nLine 313: Inside the tempdelposit loop for tempdelposit = $tempdelposit\ttempseqpos = $tempseqpos\tMatch = $1\t$2\$3";
					}		
			}		
		#	}
		
		$tempseqpos =~ s/^($tempdelim1)//i;
		$tempseqpos =~ s/(,)$//i;
		################### End of Check for any del positions and include them in the tempseqpos
		@tempseqposits = Splitrow ($tempseqpos,$delim1);
		print debug "\nLine 302: In getseqpos ending\ttempseqpos = $tempseqpos\ttempseqposits = @tempseqposits\tdelpositflag = $delpositflag";
		#$tempseqpositsref = \@tempseqposits;
		#($tempseqpos, $tempseqpositsref, $delpositflag);
		($tempseqpos, $delpositflag, @tempseqposits);
	}
				
#End Func Getseqpos*******************************


#my @files;
#@files = @ARGV;
#open SeqFeat, "<$files[0]";
#open phen, "<$files[1]";
#open matches, ">matchedSeqFeat";

#my %SeqFeats_Pos;
#my @SeqFeat;
#my %CustSeqFTalels;
$tempdatetime = scalar localtime();

#print logrept "\nLog Report for $Locus $custom variants generation from Allele motif and verdef sequences derived from Alignment data of Version: ",$alignVer," as of ", $tempdatetime, "\n";


open AlleleSeqFeats, "<$inpfiles[0]";
open SeqFeatdefs, "<$inpfiles[1]";
open DefSeqfeats, "<$inpfiles[2]";
open DefAllelevarnts, "<$inpfiles[3]";
open PreassSFVTnos, "<$inpfiles[4]";
#my $tempfilename = "Genrtd_".$custom."_".$HLA_Locus."_Motif_Variants";
#my $tempfilename = "Genrtd_".$custom."_".$HLA_Locus."_Sqft_Variants";

#open SqftVarntsfile, ">$tempfilename";
open SqftVarntsfile, ">$outfiles[0]";
#print SqftVarntsfile "Locus Name\tHLA Protein Feature Name\tHLA Protein Feature Type\tAllele Group\tMotif\tVariant Type\tFeature Location - Amino Acid Positions\tVariant Type Definition\n";
#print SqftVarntsfile "Locus Name\tFeature Location - Amino Acid Position(s)\tHLA Protein Feature Name(s)\tHLA Protein Feature Type(s)\tAllele Group\tMotif\tVariant Type\tFeature Location - Amino Acid Position(s)\tVariant Type Definition\n";
print SqftVarntsfile "Sequence Feature No.\tSequence Feature Location - Amino Acid Position(s)\tSequence Feature Name(s)\tSequence Feature Type(s)\tSequence Feature Variant Type\tMotif sequence\tVariant Type Definition\tAllele Group\n";

#$tempfilename = "Genrtd_".$custom."_".$HLA_Locus."_Allele_Variants";
#open AlleleVarntsfile, ">$tempfilename";
open AlleleVarntsfile, ">$outfiles[1]";
#print AlleleVarntsfile "HLA Allele Name";
print AlleleVarntsfile "Sequence feature Names";

#$tempfilename = "Genrtd_".$custom."_".$HLA_Locus."_Unknown and Non Variants";
#open Unk_NonVarntsfile, ">$tempfilename";
open Unk_NonVarntsfile, ">$outfiles[2]";
#print Unk_NonVarntsfile "Locus Name\tAmino Acid Position\tAmino Acid Residue\tAllele Group\n";
print Unk_NonVarntsfile "Locus Name\tHLA Sequence Feature No.\tFeature Location - Amino Acid Position(s)\tAmino Acid Residue(s)\tAllele Group\tSequence Feature Name(s)\n";


open IndivSeqfeat, ">$outfiles[3]";
#print IndivSeqfeat "Locus\tHLA Sequence Feature No.\tFeature Location - Amino Acid Position(s)\tHLA Protein Feature Name(s)\tHLA Protein Feature Type(s)\tComments\tReferences\n";
print IndivSeqfeat "HLA Sequence Feature No.\tSequence Feature Location - Amino Acid Position(s)\tSequence Feature Name(s)\tSequence Feature Type(s)\tComments\tReferences\n";

open Occrcyrept, ">$outfiles[9]";
#print Occrcyrept "$Locus\tAmino Acid Positions\tHLA Sequence Feature No.\tSequence Feature Names\tVariant Status\tNumber of variant types of the sequence feature\tNumber of Variant Positions in the sequence feature\tThe variant positions in the sequence feature\tSequence Feature Types\tContained in Sequence features\tSequence Feature Names of the constituting sequence features\tNumber of constituting unique sequence features\tNumber of constituting sequence feature names\tThe amino acid positions of the constituting unique sequence features\n";
print Occrcyrept "Locus\tAmino Acid Position(s)\tHLA Sequence Feature No.\tSequence Feature Name(s)\tSequence Feature Type(s)\tVariant Status\tContained in Sequence feature(s)\tNumber of constituting unique sequence feature(s)\tSequence Feature Name(s) of the constituting sequence feature(s)\tNumber of constituting sequence feature name(s)\tNumber of variant types of the sequence feature\tNumber of Variant Positions in the sequence feature\tThe variant position(s) in the sequence feature\tThe amino acid position(s) of the constituting unique sequence feature(s)\n";

open Redundrept, ">$outfiles[10]";
print Redundrept "Locus\tAmino Acid Position(s)\tHLA Sequence Feature No.\tSequence Feature Name(s)\tSequence Feature Type(s)\tVariant Status\tUnique Sequence feature(s) that share some or all of the variant positions and also have equal number of variant types\tNumber of unique sequence feature(s) that share some or all of the variant positions and also have equal number of variant types\tSequence Feature Name(s) of the sequence feature(s) that share some or all of the variant positions and also have equal number of variant types\tNumber of sequence feature name(s) that share some or all of the variant positions and also have equal number of variant types\tNumber of variant types of the sequence feature\tNumber of Variant Positions in the sequence feature\tThe variant position(s) in the sequence feature\tThe amino acid position(s) of the unique sequence features that share some or all of the variant positions and also have equal number of variant types\n";
#print Redundrept "$Locus\tAmino Acid Positions\tHLA Sequence Feature No.\tSequence Feature Names\tSequence Feature Types\tVariant Status\tSequence features that share the variant positions that solely contribute to the total number of variant types\tNumber of unique sequence features that share some or all of the variant positions and also have equal number of variant types\tSequence Feature Names of the sequence features that share some or all of the variant positions and also have equal number of variant types\tNumber of sequence feature names that share some or all of the variant positions and also have equal number of variant types\tNumber of variant types of the sequence feature\tNumber of Variant Positions in the sequence feature\tThe variant positions in the sequence feature\tThe amino acid positions of the unique sequence features that share some or all of the variant positions and also have equal number of variant types\n";


my $i = 0;


my @AlleleSeqFeathdrs;
my @SeqFeathdrs;


#print SqftVarntsfile "$SeqFeathdr\n";
my %fin4digAlleleSeqs;
my %fin4digmatprotvardefs;
#my @refseqnumb;
my @alignseqnumb;
my $refseqnumb;
my $Alleletag;



################### Reading the Reference Allele Variants for Mature Protein sequence and var def sequence and getting the individual vardef and motif units

$AlleleSeqFeathdrs[$i] = <AlleleSeqFeats>;
$i++;
#$AlleleSeqFeathdrs[$i] = <AlleleSeqFeats>;
#$i++;

#my $AlleleVarntshdr;
my @AlleleVarntshdrs;


my %SeqFeats_Pos;
my @SeqFeat;
my %SeqFeatTypes;
my %uniqSeqFeatTypes;
my %SeqFeatVarTypes;
#my $Seqfeatct = $seqftstartct;
#my %SeqFeatTypesall;
my $Seqfeatshdr;
my %SeqPos_Feattypes;
my %SeqPos_Feats;
#my @SeqPosFeats;
my %SeqPosFeats;
my %SeqPosFeatflag;
my $SeqPosfeatct = $seqftstartct;
my %SeqPos_Featfrmtpos;
my %SeqFeat_no;
my %SeqFeatlen;

$SeqFeat_no{$Alleleseqfeat} = 1;
$i = 0;

my $alleleseqfeatno = $Seqfeatname.($SeqFeat_no{$Alleleseqfeat});

################### Reading the pre-assigned nos for sequence features and types

my $preasssfvtnohdr = <PreassSFVTnos>;

while (<PreassSFVTnos>) 
	{
		chomp;
		my $preasssfvtline = $_;
		my @preasslineterms = Splitrow($preasssfvtline, "\t");
		
		if ($preasslineterms[$preasssfnonamecol] =~ /^($Alleleseqfeat)$/i) 
			{
				next;
			}

		if (($preasslineterms[$preasssfnonamecol] ne "") && ($preasslineterms[$preasssfnoposcol] ne ""))
		
			{				
				my $motifseq = $preasslineterms[$preasssfnomotifcol];
#				my $vardefseq = $preasslineterms[$preasssfnovardefcol];
#				my $seqfttype = $preasslineterms[$preasssfnoseqfeattypcol];
#				my $allelesgrp = $preasslineterms[$preasssfnoallelegrpcol];
				my $varnttype = $preasslineterms[$preasssfnovtcol];
				my $sfno = $preasslineterms[$preasssfnosfnocol];
				my $unknflag = 0;
				
				$sfno =~ /^($preassSFfrmt)$/i;
				$sfno = Trim($2);
#				$varnttype =~ s/^($preassTypelabel)//i;
				$varnttype =~ /^($preassTypefrmt)$/i;
				$varnttype = Trim($2);
				$varnttype =~ s/^$Typename//i;
				
				
				my $tempfrmtseqpos = $preasslineterms[$preasssfnoposcol];
				#$preasssfvtflag{$preasslineterms[$preasssfnonamecol]} = 1;
			#print debug "\nLIne 678: tempfrmtseqpos = $tempfrmtseqpos\tseqposfrmtdelim = $seqposfrmtdelim\tseqposdelim = $seqposdelim";
				my ($tempseqpos, $delpositflag, @tempseqposits) = Getseqpos ($tempfrmtseqpos, $seqposfrmtdelim, $seqposdelim, @delposits);
			#print debug "\nLIne 678: tempseqpos = $tempseqpos\tdelpositflag = $delpositflag\ttempseqposits = @tempseqposits";
			#print debug "\nLine 679: \tpreasssfnonamecol = $preasssfnonamecol\tpreasslineterms[$preasssfnonamecol] = $preasslineterms[$preasssfnonamecol]";
#				$SeqFeats_Pos{$preasslineterms[$preasssfnonamecol]} = $tempseqpos;
				$SeqFeatlen{$tempseqpos} = @tempseqposits;

#				$SeqPos_Featfrmtpos{$preasslineterms[$SFfilenamecol]} = $tempfrmtseqpos;
				$SeqPos_Featfrmtpos{$tempseqpos} = $tempfrmtseqpos;

				$preasssfvtflag{$preasslineterms[$preasssfnonamecol]} = 1;
				$preasssfvtflag{$tempseqpos} = 1;
				
				$SeqFeat_no{$tempseqpos} = $sfno;
				
				print debug "\nLine 732: SFVT is ",$preasslineterms[$preasssfnovtcol],"\tsfnofrmt = $preassSFfrmt\t$typefrmt = $preassTypefrmt\tsfno = $sfno\tvarnttype = $varnttype\ttempseqpos = $tempseqpos";
				#$SeqPos_Featfrmtpos{$tempseqpos} = 
				#if ($motifseq =~ /^([A-Z\.]+)$/i) 
#				if (!($varnttype =~ /Unknown/i))
				if (!($varnttype =~ /$preassunktypelabel/i))
					{
						$Alleles_PosTypes->{$tempseqpos}{$motifseq} = $varnttype;
#						$Vardef_CustSqftType->{$tempseqpos}->{$varnttype} = $vardefseq;
						$Motif_CustSqftType->{$tempseqpos}->{$varnttype} = $motifseq;
#						$Alleles_CustSqft->{$tempseqpos}{$motifseq} = $allelesgrp;				
					}
				else
					{
						$unknflag = 1;
#						$Alleles_CustUnkSqft->{$tempseqpos} = $allelesgrp;
					}

						
			}	
	}

################### End of Reading the pre-assigned nos for sequence features and types


################### Reading the already Defined Seqfeature file for motif variants and allele variants for the already defined sequence features

my $defseqfeathdr = <DefSeqfeats>;

while (<DefSeqfeats>) 
	{
		chomp;
		my $defSeqfeatline = $_;
		my @defFeatlineterms = Splitrow($defSeqfeatline, "\t");
		
		if ($defFeatlineterms[$defsffilenamecol] =~ /^($Alleleseqfeat)$/i) 
			{
				next;
			}

		if (($defFeatlineterms[$defsffilenamecol] ne "") && ($defFeatlineterms[$defsffileposcol] ne ""))
		
			{
				
				my $motifseq = $defFeatlineterms[$defsffilemotifcol];
				my $vardefseq = $defFeatlineterms[$defsffilevardefcol];
				my $seqfttype = $defFeatlineterms[$defsffileseqfeattypcol];
				my $allelesgrp = $defFeatlineterms[$defsffileallelegrpcol];
				my $varnttype = $defFeatlineterms[$defsffilevarnttypcol];
				my $unknflag = 0;
				
				$varnttype =~ s/^($defTypelabel)//i;
				$varnttype = Trim($varnttype);
				
				
				my $tempfrmtseqpos = $defFeatlineterms[$defsffileposcol];
				#$defseqfeatflag{$defFeatlineterms[$defsffilenamecol]} = 1;
			#print debug "\nLIne 678: tempfrmtseqpos = $tempfrmtseqpos\tseqposfrmtdelim = $seqposfrmtdelim\tseqposdelim = $seqposdelim";
				my ($tempseqpos, $delpositflag, @tempseqposits) = Getseqpos ($tempfrmtseqpos, $seqposfrmtdelim, $seqposdelim, @delposits);
			#print debug "\nLIne 678: tempseqpos = $tempseqpos\tdelpositflag = $delpositflag\ttempseqposits = @tempseqposits";
			#print debug "\nLine 679: \tdefsffilenamecol = $defsffilenamecol\tdefFeatlineterms[$defsffilenamecol] = $defFeatlineterms[$defsffilenamecol]";
				$SeqFeats_Pos{$defFeatlineterms[$defsffilenamecol]} = $tempseqpos;
				$SeqFeatlen{$tempseqpos} = @tempseqposits;

				$SeqPos_Featfrmtpos{$defFeatlineterms[$SFfilenamecol]} = $tempfrmtseqpos;
				$SeqPos_Featfrmtpos{$tempseqpos} = $tempfrmtseqpos;

				$defseqfeatflag{$defFeatlineterms[$defsffilenamecol]} = 1;
				$defseqfeatflag{$tempseqpos} = 1;

				#$SeqPos_Featfrmtpos{$tempseqpos} = 
				#if ($motifseq =~ /^([A-Z\.]+)$/i) 
				if (!($varnttype =~ /Unknown/i))
					{
						$Alleles_PosTypes->{$tempseqpos}{$motifseq} = $varnttype;
						$Vardef_CustSqftType->{$tempseqpos}->{$varnttype} = $vardefseq;
						$Motif_CustSqftType->{$tempseqpos}->{$varnttype} = $motifseq;
						$Alleles_CustSqft->{$tempseqpos}{$motifseq} = $allelesgrp;				
					}
				else
					{
						$unknflag = 1;
						$Alleles_CustUnkSqft->{$tempseqpos} = $allelesgrp;
					}

				if ($varnttype == 1) 
					{
						$possfvarntflag{$tempseqpos} = 0;
					#print debug "\nLine 625: varnttype = $varnttype\tpossfvarntflag = $possfvarntflag{$tempseqpos}";
					}
				elsif ($varnttype > 1) 
					{ 
						$possfvarntflag{$tempseqpos} = 1;
					#print debug "\nLine 631: varnttype = $varnttype\tpossfvarntflag = $possfvarntflag{$tempseqpos}";
					}				
			}	
	}
################### End of Reading the already Defined Seqfeature file for motif variants and allele variants for the already defined sequence features

################### Reading the already Defined Allele varnts file for motif variants and allele variants for the already defined sequence features
my $rowct = 0;
my @defavfileSFnames;
my $sfnamesct;
while (<DefAllelevarnts>) 
	{
		chomp;
		my $defAlleleline = $_;
		my @defFeatlineterms = Splitrow($defAlleleline, "\t");
		my $tempct = 0;
		
		if ($rowct == $defavfilenamerow) 
			{
				$sfnamesct = (@defFeatlineterms - 1)/3;
				foreach $tempct (0..$sfnamesct-1) 
					{
						$defavfileSFnames[$tempct] = $defFeatlineterms[($tempct*3)+1];
						$defavfileSFnames[$tempct] =~ s/ *- *Motif//i;
					}
				$rowct++;
				next;
			}
		
		if ($defFeatlineterms[$defavfileallelecol] =~ /^( *)$/i) 
			{
				next;
			}
		
		my $currAllele = $defFeatlineterms[$defavfileallelecol];
		my $currsfname;
		my $currsfnamesstr;
		$tempct = 0;
		$unknflag = 0;
		
	#print debug "\nLine 751: currallele = $currAllele\trowct = $rowct\tsfnamesct = $sfnamesct";
		foreach $currsfnamesstr (@defavfileSFnames) 
			{
				$currsfnamesstr = $currsfnamesstr.$featnamedelim;
				if ($currsfnamesstr =~ /($Alleleseqfeat)$featnamedelim/i) 
					{
						$tempct = $tempct + 3;
						next;
					}
				$currsfnamesstr =~ s/$featnamedelim$//i;
				my @currsfnames = Splitrow ($currsfnamesstr, $featnamedelim);
				
				my $motifseq = $defFeatlineterms[$tempct + $defavfilemotifcol];
				my $varnttype = $defFeatlineterms[$tempct + $defavfilevarnttypcol];
				my $vardefseq = $defFeatlineterms[$tempct + $defavfilevardefcol];
				
				$varnttype =~ s/^($defTypelabel)//i;
				$varnttype = Trim($varnttype);

				my $tempseqpos = $SeqFeats_Pos{$currsfnames[0]};
				
				$allelesqftVardef->{$currAllele}->{$tempseqpos} = $vardefseq;
				$allelesqftMotif->{$currAllele}->{$tempseqpos} = $motifseq;
				
				if ($varnttype =~ /Unknown/i)
					{
						$Alleles_CustSqftType->{$tempseqpos}{$currAllele}{Unknown} = "Unknown Variant Type";
					}
				else
					{
						$Alleles_CustSqftType->{$tempseqpos}{$currAllele}{$motifseq} = $varnttype;
					}
			#print debug "\nLine 783: currallele = $currAllele\trowct = $rowct\tsfnamesct = $sfnamesct\tcurrsfnamesstr = $currsfnamesstr\tcurrsfnames[0] = $currsfnames[0]\tSeqFeats_Pos[currsfnames[0]] = $SeqFeats_Pos[$currsfnames[0]]\ttempseqpos = $tempseqpos\tallelescustsqfttype = $Alleles_CustSqftType->{$tempseqpos}{$currAllele}{$motifseq}\tmotifseq = $motifseq\tvarntype = $varnttype";
				$tempct = $tempct + 3;
			}
		
		$rowct++;
	}
################### End of Reading the already Defined Allele varnts file for motif variants and allele variants for the already defined sequence features


while (<AlleleSeqFeats>) 
	{
		chomp;
		my $Seqfeatline = $_;
		my @Featlineterms = Splitrow($Seqfeatline, "\t");
		
		#if ($Featlineterms[0] ne "") 
		if (($Featlineterms[$featlinematseqcol] eq "") || ($Featlineterms[$featlineallelecol] =~ /[0-9]+($nullcode)$/i))
			{
				#print logrept "\nThe allele \t$Featlineterms[$featlineallelecol]\t is a null allele due to the code $1 in its name";
				next;
			}
		
		if ($Featlineterms[$featlineallelecol] ne "") 
			{
				#my $Allele = $Featlineterms[0];
				my $Allele = $Featlineterms[$featlineallelecol];
				my $AlleleMatProt = $Featlineterms[$featlinematseqcol];
				my $AlleleMatProtVardef = $Featlineterms[$featlinevardefcol];

				$Alleletag = $Allele;
				$Alleletag =~ s/((\*)?([0-9]+))$//i;
				#$Alleletag =~ s/([0-9]+)([A-Z]+)?$//i;
				
				print debug "\nInside the allele read loop, line 444:\tAlleletag = $Alleletag\tAllele = $Allele\t";

				$fin4digAlleleSeqs{$Allele} = $AlleleMatProt;
				$fin4digmatprotvardefs{$Allele} = $AlleleMatProtVardef;
				##print debug "\nInside the allele loop, line 374:\tfin4digAlleleSeqs = $fin4digAlleleSeqs{$Allele}\tfin4digmatprotvardefs = $fin4digmatprotvardefs{$Allele}";
				
				#$Allele =~ s/$HLA_Locus\*//;
				#$Allele =~ s/$Alleletag(\*)?//i;
				#$Allele =~ s/$Alleletag\*//;
				#my $AlleleMatProt = $Featlineterms[1];
				
				#my $Alleles_PosTypes;
				print debug "\nInside the allele read loop, line 456:\tAlleletag = $Alleletag\tAllele after format = $Allele\t";

				#my @seqaminos = split ($motifdelim, $AlleleMatProt);
				#my @seqaminos = split /$motifdelim/, $AlleleMatProt;
				my @seqaminos = split("$motifdelim",$AlleleMatProt);
				$Alleles_Posamino->{$Allele} = \@seqaminos;
				#my @tempalleleaminos = @{$Alleles_Posamino->{$Allele}};
			#print debug "\nInside the allele loop, line 461:\tThe seqaminos are = @seqaminos\talleles posamino = (@{$Alleles_Posamino->{$Allele}})";
								
				#my @vardefunits = split ($vardefdelim, $AlleleMatProtVardef);
				my @vardefunits = split("$vardefdelim",$AlleleMatProtVardef);
				$Alleles_Vardefunits->{$Allele} = \@vardefunits;
			#print debug "\nInside the allele loop, line 465:\tThe vardefunits are = @vardefunits\talleles vardefunits = (@{$Alleles_Vardefunits->{$Allele}})";
				##print debug "\nAllele - $Allele\t",$Alleles_Posamino->{$Allele},"\tPositions: ",$Alleles_Posamino->{$Allele}[7],":: ",$Alleles_Posamino->{$Allele}[7],"::: ",${$Alleles_Posamino->{$Allele}}[7],":::: ",$Alleles_Posamino->{$Allele}[7]," ::::: ",@tempalleleaminos," all aminos- ",(@tempalleleaminos);
				
				$tempallelect++;
			}
	}

################################ End of Reading the Reference Allele Variants for Mature Protein sequence and var def sequence and getting the individual vardef and motif units


################################ Assigning the vardef units and identifying vardefsymbols from the reference allele's mature protein sequence and var def units

my $refcharct = 0;
#my $temprefallele = $HLA_Locus."*".$ref4digallele;
#my $refallele = $Alleletag."*".$ref4digallele;
my $refallele = $Ref_HLA_Locus."*".$ref4digallele;
#my $refallele = $Alleletag.$ref4digallele;
my $refallelematseq = $fin4digAlleleSeqs{$refallele};
#$refseqnumb[$currpos];

#my @refalleleaminos = split("",$refallelematseq);
#$RefAllele_Posamino = \@refalleleaminos;
my $alignpos=0;
my $vardefct = 0;
my %translseqfeatpos;
#foreach $refpos (0..$alignprotlen) 
print debug "\nLine 489: ref4digallele = $ref4digallele\tAllelesvardefunits = ", @{$Alleles_Vardefunits->{$refallele}};
foreach $refvardefunit (@{$Alleles_Vardefunits->{$refallele}}) 
	{
		my $vardefsymb = $refvardefunit;
		#$vardefsymb	=~ s/[A-Z]+//i;
		#$vardefsymb	=~ /([0-9]+[($insdel)($insertion)($deletion)])([A-Z]+)$/i;
		#$vardefsymb	=~ /([0-9]+($insdel)?)([A-Z]+)$/i;
		#$vardefsymb	=~ /([0-9]+($insertion)?)([A-Z]+)$/i;
		#$vardefsymb	=~ /([0-9]+($deletion)?)([A-Z]+)$/i;
		$vardefsymb	=~ /((-?[0-9]+(($insdel)|($insertion)|($deletion))?)([A-Z]+)?)$/i;
		#$vardefsymb	=~ /([0-9]+)(($insdel)|($insertion)|($deletion))?([A-Z]+)$/i;
		#$vardefsymb	= ($1).($2);
		$vardefsymb	= $2;
		
		#$vardefsymb	=~ s/([0-9]+($insdel))([A-Z]+)$/\1/i;
		#$vardefsymb	=~ s/([0-9]+($insertion))([A-Z]+)$/\1/i;
		#$vardefsymb	=~ s/([0-9]+($deletion))([A-Z]+)$/\1/i;
		
	#print debug "\nLine 506: The refvardefunit = $refvardefunit\tvardefsymb = $vardefsymb\tThe refvardefunit is: $refvardefunit\tThe matched subsymbols 1 = $1 ,2 = $2 ,3 = $3 ,4 = $4 ,5 = $5 ,6 = $6 ,7 = $7 ,8 =$8";
		#my $delct = 1;
		#$1 = 1;
		##print debug "\nThe vardefsymb = $verdefsymb\tThe refpos is: $refpos\tThe assigned subsymbol = $1 after match ";

		my $refpos = $vardefsymb;
		#$refpos =~ s/\.([0-9]+)$//i;
		#$delct = $1;
		##print debug "\nThe vardefsymb = $verdefsymb\tThe refpos is: $refpos\tThe matched subsymbol = $1";

		#$refseqnumb->{$refpos}->{$1} = $vardefsymb;
		#$refseqnumb->{$vardefsymb}->{$1} = $vardefsymb;
		#$translseqfeatpos{$vardefsymb} = $alignpos;
		$translseqfeatpos{$vardefsymb} = $vardefct;
		#$translseqfeatpos[$verdefsymb] = $translseqfeatpos[$refpos].",".($refpos+$delct);
		$alignseqnumb[$vardefct] = $vardefsymb;
		#$alignseqnumb[$alignpos] = $vardefsymb;
		#$alignpos++;
		##print debug "\nAllele = $refallele\tvardefct = $vardefct\tvardefsymb = $vardefsymb\ttranslseqfeatpos = $translseqfeatpos{$vardefsymb}\talignseqnumb = $alignseqnumb[$vardefct]";
		$vardefct++;
	}

################################ End of Assigning the vardef units and identifying vardefsymbols from the reference allele's mature protein sequence and var def units



################################## Reading the Sequence feature definitions file

$Seqfeatshdr = <SeqFeatdefs>;

while (<SeqFeatdefs>) 
	{
		chomp;
		my $Seqfeatline = $_;
		my @Featlineterms = Splitrow($Seqfeatline, "\t");
		
		if (($Featlineterms[$SFfilenamecol] ne "") && ($Featlineterms[$SFfileposcol] ne ""))
		#if (($Featlineterms[0] ne "") && ($Featlineterms[2] ne ""))
			{
				#$SeqFeats_Pos{$Featlineterms[$SFfilenamecol]} = $Featlineterms[$SFfileposcol];
				#$SeqFeats_Pos{$Featlineterms[0]} = $Featlineterms[2];

				#my @tempseqposits = GetPosits($Featlineterms[2]);
				$tempfrmtseqpos = $Featlineterms[$SFfileposcol];

				my ($tempseqpos, $delpositflag, @tempseqposits) = Getseqpos ($tempfrmtseqpos, $seqposfrmtdelim, $seqposdelim, @delposits);
				

				##print debug "\nIn Seqfeatparser tempseqpos = \t$tempseqpos";
				$SeqFeats_Pos{$Featlineterms[$SFfilenamecol]} = $tempseqpos;
				$SeqFeatlen{$tempseqpos} = @tempseqposits;
				
				
				##print debug "\nIn Seqfeatparser after formatted tempfrmtseqpos = \t$tempfrmtseqpos";
				$SeqPos_Featfrmtpos{$Featlineterms[$SFfilenamecol]} = $tempfrmtseqpos;	
				$SeqPos_Featfrmtpos{$tempseqpos} = $tempfrmtseqpos;

				if (defined $SeqPos_Feats{$tempseqpos}) 
					{
						#$tempmetastr = quotemeta $featnamedelim;
						#if (!($SeqPos_Feats{$tempseqpos} =~ /$Featlineterms[0]; /i)) 
						if (!($SeqPos_Feats{$tempseqpos} =~ /($featnamemetadelim)$Featlineterms[$SFfilenamecol]($featnamemetadelim)/i)) 
						#if (!($SeqPos_Feats{$tempseqpos} =~ /$Featlineterms[$SFfilenamecol]; /i)) 
							{
								$SeqPos_Feats{$tempseqpos} = $SeqPos_Feats{$tempseqpos}.$Featlineterms[$SFfilenamecol].($featnamedelim);
								#$SeqPos_Feats{$tempseqpos} = "; ".$SeqPos_Feats{$tempseqpos}.$Featlineterms[$SFfilenamecol]."; ";
								#$SeqPos_Feats{$tempseqpos} = $SeqPos_Feats{$tempseqpos}.$Featlineterms[0]."; ";
								#$SeqFeat_no{$Featlineterms[0]} = $SeqFeat_no{$tempseqpos};	
								$SeqFeat_no{$Featlineterms[$SFfilenamecol]} = $SeqFeat_no{$tempseqpos};	
								#if (!($SeqPos_Feattypes{$tempseqpos} =~ /$Featlineterms[1]; /i)) 
								#$tempmetastr = quotemeta $feattypedelim;
								if (!($SeqPos_Feattypes{$tempseqpos} =~ /($feattypemetadelim)$Featlineterms[$SFfileseqfeattypcol]($feattypemetadelim)/i)) 
								#if (!($SeqPos_Feattypes{$tempseqpos} =~ /$Featlineterms[$SFfileseqfeattypcol]; /i)) 
									{
										$SeqPos_Feattypes{$tempseqpos} = $SeqPos_Feattypes{$tempseqpos}.$Featlineterms[$SFfileseqfeattypcol].($feattypedelim);
										#$SeqPos_Feattypes{$tempseqpos} = $SeqPos_Feattypes{$tempseqpos}."; ".$Featlineterms[$SFfileseqfeattypcol]."; ";
										#$SeqPos_Feattypes{$tempseqpos} = $SeqPos_Feattypes{$tempseqpos}.$Featlineterms[1]."; ";
									}	
							}	
					}
				else
					{
						#$SeqPosFeats[$SeqPosfeatct-$seqftstartct] = $tempseqpos;
#						$SeqPosFeats[$SeqPosfeatct-1] = $tempseqpos;
						#$SeqPos_Feats{$tempseqpos} = $Featlineterms[$SFfilenamecol]."; ";
						$SeqPos_Feats{$tempseqpos} = $featnamedelim.$Featlineterms[$SFfilenamecol].$featnamedelim;
						#$SeqPos_Feats{$tempseqpos} = $Featlineterms[0]."; ";
						$SeqPos_Feattypes{$tempseqpos} = $feattypedelim.$Featlineterms[$SFfileseqfeattypcol].$feattypedelim;
						#$SeqPos_Feattypes{$tempseqpos} = $Featlineterms[$SFfileseqfeattypcol]."; ";
						#$SeqPos_Feattypes{$tempseqpos} = $Featlineterms[1]."; ";
						if (defined $SeqFeat_no{$tempseqpos}) 
							{
								$SeqPosfeatct = $SeqFeat_no{$tempseqpos};
#								$SeqPosFeats{$SeqPosfeatct} = $tempseqpos;
							}
						else
							{
								$SeqPosfeatct = max (values %SeqFeat_no);
								$SeqPosfeatct++;
								$SeqFeat_no{$tempseqpos} = $SeqPosfeatct;
#								$SeqPosFeats{$SeqPosfeatct} = $tempseqpos;
#								$SeqFeat_no{$Featlineterms[$SFfilenamecol]}  = $SeqPosfeatct;
							}
#						$SeqPosfeatct++;
#						$SeqFeat_no{$tempseqpos} = $SeqPosfeatct;
						$SeqFeat_no{$Featlineterms[$SFfilenamecol]}  = $SeqPosfeatct;
						$SeqPosFeats{$SeqPosfeatct} = $tempseqpos;
						print debug "\nLine 1127: \tSeqposfeatct = $SeqPosfeatct\tSeqPosFeats{$SeqPosfeatct} = $SeqPosFeats{$SeqPosfeatct}\tSeqFeat_no{tempseqpos} = $SeqFeat_no{$tempseqpos}\ttempseqpos = $tempseqpos";
						#$SeqFeat_no{$Featlineterms[0]}  = $SeqPosfeatct;
					}
											
				#$SeqFeat[$i] = $Featlineterms[0];
				$SeqFeat[$i] = $Featlineterms[$SFfilenamecol];
				
				#if ($Featlineterms[2] ne "")
				if ($Featlineterms[$SFfileposcol] ne "")
				#if (($Featlineterms[2] ne "") || ($Featlineterms[0] =~ /^Allele$/i)) 
					{
						$Seqfeatct++;
						#my $currseqfeattype;
						#my @Majortypes = ("Structural", "Functional", "Evolutionary");
						my $currseqfeattype;
						foreach  $currseqfeattype (@Majortypes) 
							{
								#if ($Featlineterms[$SFfileseqfeattypcol] =~ /^($currseqfeattype ?- ?)/i) 
								if ($Featlineterms[$SFfileseqfeattypcol] =~ /^($currseqfeattype - )/i) 
								#if ($Featlineterms[1] =~ /^Structural - /i) 
									{
										#$currseqfeattype =  "Structural";
										#$SeqFeatTypes{"structural"}++;
										$SeqFeatTypes{lc($currseqfeattype)}++;
										#$uniqSeqFeatTypes{lc($currseqfeattype)} = $SeqFeatTypes{lc($currseqfeattype)};
									}	
							}
						#else
						#	{
						#		$currseqfeattype =  $Featlineterms[1];
						#	}
						#$SeqFeatTypes{lc($currseqfeattype)}++; 					
						#$SeqFeatTypesall{lc($Featlineterms[1])}++;
						$SeqFeatTypes{lc($Featlineterms[$SFfileseqfeattypcol])}++;
						#$uniqSeqFeatTypes{lc($Featlineterms[$SFfileseqfeattypcol])} = $SeqFeatTypes{lc($Featlineterms[$SFfileseqfeattypcol])};
						#$SeqFeatTypes{lc($Featlineterms[1])}++;
						#$Seqfeatshdr = $Seqfeatshdr."\t".$HLA_Locus." ".$SeqFeat[$i];
						$Seqfeatshdr = $Seqfeatshdr."\t".$SeqFeat[$i];
					}

				$i++;
			}
	}

################################## End of Reading the Sequence feature definitions file


#####################      To accumulate the seq features where the indiv pos and posseqfeat occur as part of a bigger posseqfeat
my $tempseqpos;
my $seqposfeatkey;
#foreach $tempseqpos (@SeqPosFeats) 
foreach $seqposfeatkey (sort by_number keys %SeqPosFeats) 
	{
		$tempseqpos = $SeqPosFeats{$seqposfeatkey};
		#@tempseqposits = split /, /, $tempseqpos; 
		my @tempseqposits = Splitrow($tempseqpos, $seqposdelim); 
		my $resdpos;
		#my $tempseqposno = $SeqFeat_no{$tempseqpos}-$seqftstartct-1;
#		my $tempseqposno = $SeqFeat_no{$tempseqpos}-2;
		my $tempseqposno = $SeqFeat_no{$tempseqpos};
		my @temptempseqposits = sort(@tempseqposits);
		#if (!(defined $sortdtempseqpos{$tempseqpos})) 
		#	{
		#		$sortdtempseqpos{$tempseqpos} = join ", ", @temptempseqposits;
		#		$sortdtempseqpos{$tempseqpos} = ", ".$sortdtempseqpos{$tempseqpos}.",";
		#	}
		
		#my $modfdtempseqpos = join ", ", sort(@tempseqposits);
		my $modfdtempseqpos;
		my $currsortdtempseqpos;
		#print debug "\nLine 719: inside the seqfeat definitions near possfoccurs, temptempseqposits are:\t@temptempseqposits";
		foreach $resdpos (@temptempseqposits) 
			{
				if (defined $possfoccurs{$resdpos}) 
					{
						#if (!($possfoccurs{$resdpos} =~ /(; )$Seqfeatname$SeqFeat_no{$tempseqpos}(; )/i))
						#print debug "\nLine 726: Inside the resdpos loop for defined resdpos = $resdpos\tFor tempseqposno = $tempseqposno:\tThe match = \t$1\t$2\t$3\tfor posofoccurs = $possfoccurs{$resdpos}";
						#$tempmetastr = quotemeta $featnodelim;
						if (!($possfoccurs{$resdpos} =~ /(($featnometadelim)$tempseqposno($featnometadelim))/i))
							{
								#$possfoccurs{$resdpos} = $possfoccurs{$resdpos}.$Seqfeatname$SeqFeat_no{$tempseqpos}."; ";
								#print debug "\nLine 730: Inside the resdpos loop for defined resdpos = $resdpos\tFor tempseqposno = $tempseqposno:\tThe match = \t$1\t$2\t$3\tfor posofoccurs = $possfoccurs{$resdpos}";
								$possfoccurs{$resdpos} = $possfoccurs{$resdpos}.$tempseqposno.($featnodelim);
							}
					}
				else
					{
						#$possfoccurs{$resdpos} = "; ".$Seqfeatname$SeqFeat_no{$tempseqpos}."; ";
						$possfoccurs{$resdpos} = $featnodelim.$tempseqposno.$featnodelim;
						#print debug "\nLine 737: Inside the resdpos loop for not defined resdpos = $resdpos\tFor tempseqposno = $tempseqposno:\tThe match = \t$1\t$2\t$3\tfor posofoccurs = $possfoccurs{$resdpos}";
					}
				$modfdtempseqpos = $modfdtempseqpos."( ".$resdpos.",){1}.*";
				$sortdtempseqpos{$tempseqpos} = $sortdtempseqpos{$tempseqpos}." ".$resdpos.",";
				
				#$modfdtempseqpos = $modfdtempseqpos."( ".$resdpos.",){1}.*";
				#$sortdtempseqpos{$tempseqpos} = $sortdtempseqpos{$tempseqpos}." ".$resdpos.",";
			}
		$sortdtempseqpos{$tempseqpos} = ",".$sortdtempseqpos{$tempseqpos};
		$sortdmodfdseqposits{$tempseqpos} = $modfdtempseqpos;

		my $currtempseqpos;
		#foreach  $currsortdtempseqpos(sort values %sortdtempseqpos)
		foreach  $currtempseqpos(sort keys %sortdtempseqpos) 
			{
				
				my $largerseqpos;
				my $smallerseqpos;

				if ($SeqFeatlen{$currtempseqpos} <= $SeqFeatlen{$tempseqpos}) 
					{
						$largerseqpos = $tempseqpos;
						$smallerseqpos = $currtempseqpos;
					}
				elsif ($SeqFeatlen{$currtempseqpos} > $SeqFeatlen{$tempseqpos})
					{
						$largerseqpos = $currtempseqpos;
						$smallerseqpos = $tempseqpos;
					}
				#$tempseqposno = $SeqFeat_no{$largerseqpos}-$seqftstartct-1;
#				$tempseqposno = $SeqFeat_no{$largerseqpos}-2;
				$tempseqposno = $SeqFeat_no{$largerseqpos};
				my $currsortdtempseqpos = $sortdtempseqpos{$largerseqpos};
				my $currmodfdtempseqpos = $sortdmodfdseqposits{$smallerseqpos};
				#print debug "\nLine 764: Inside the seqfeat definitions near possfoccurs = $possfoccurs{$smallerseqpos}\tfor tempseqposno = $tempseqposno\tFor smallerpos = $smallerseqpos, largerpos = $largerseqpos:\tcurrsortdtempseqpos = $currsortdtempseqpos\tcurrmodfdtempseqpos = $currmodfdtempseqpos";
				if ($currsortdtempseqpos =~ /($currmodfdtempseqpos)/i)
					{
						print debug "\nLine 767: Inside the match of \t$1\$2\tTo currsortdtempseqpos = $currsortdtempseqpos";
						if (defined $possfoccurs{$smallerseqpos}) 
							{
								#if (!($possfoccurs{$resdpos} =~ /(; )$Seqfeatname$SeqFeat_no{$tempseqpos}(; )/i))
								#$tempmetastr = quotemeta $featnodelim;
								if (!($possfoccurs{$smallerseqpos} =~ /(($featnometadelim)$tempseqposno($featnometadelim))/i))
									{
										#$possfoccurs{$resdpos} = $possfoccurs{$resdpos}.$Seqfeatname$SeqFeat_no{$tempseqpos}."; ";
										print debug "\nLine 773: Inside thecurrsortdtempseqpos loop for defined:\tsmallerpos = $smallerseqpos :The match = \t$1\t$2\$3\tfor posofoccurs = $possfoccurs{$smallerseqpos}";
										$possfoccurs{$smallerseqpos} = $possfoccurs{$smallerseqpos}.$tempseqposno.$featnodelim;
									}
							}
						else
							{
								#$possfoccurs{$resdpos} = "; ".$Seqfeatname$SeqFeat_no{$tempseqpos}."; ";
								$possfoccurs{$smallerseqpos} = $featnodelim.$tempseqposno.$featnodelim;
								print debug "\nLine 781: Inside thecurrsortdtempseqpos loop for not defined:\tsmallerpos = $smallerseqpos :The match = \t$1\t$2\$3\tfor posofoccurs = $possfoccurs{$smallerseqpos}";
							}
					}
			}
	} 
				
				
#####################      End of To accumulate the seq features where the indiv pos and posseqfeat occur as part of a bigger posseqfeat



#$SeqFeathdrs[$i] = <AlleleSeqFeats>;




############################## Get reference allele var def/motif seq for each sequence feature and then assign var type = 1

my $alignseqposits;
my $refcurrsqftVardef;
my $refcurrsqftMotif;


#foreach  $PosSeqfeat (@SeqPosFeats)

foreach  $seqposfeatkey (sort by_number keys %SeqPosFeats)

	{
		$PosSeqfeat = $SeqPosFeats{$seqposfeatkey};	
		#$SeqPos_Feattypes{$PosSeqfeat} =~ s/(; ?)$//i;
		#$SeqPos_Feattypes{$PosSeqfeat} =~ s/^(; ?)//i;

		#$SeqPos_Feats{$PosSeqfeat} =~ s/(; ?)$//i;
		#$SeqPos_Feats{$PosSeqfeat} =~ s/^(; ?)//i;
		#$tempmetastr = quotemeta $feattypedelim;
		$SeqPos_Feattypes{$PosSeqfeat} =~ s/($feattypemetadelim)$//i;
		$SeqPos_Feattypes{$PosSeqfeat} =~ s/^($feattypemetadelim)//i;

		#$tempmetastr = quotemeta $featnamedelim;
		$SeqPos_Feats{$PosSeqfeat} =~ s/($featnamemetadelim)$//i;
		$SeqPos_Feats{$PosSeqfeat} =~ s/^($featnamemetadelim)//i;

		
		if ($defseqfeatflag{$PosSeqfeat} == 1) 
			{
				next;
			}


		my @currseqfeat = Splitrow ($PosSeqfeat, $seqposdelim);
		#my @currseqfeat = split /, /, $PosSeqfeat;
		my $currseqfeatlen = scalar @currseqfeat;
		my $seqfeatpos;
		#my $refcurrsqftVardef;
		#my $refcurrsqftMotif;
		
	#print debug "\nInside the posseqfeat loop, line 964:\tposseqfeat = $PosSeqfeat\tcurrseqfeat = @currseqfeat\tSeqpos_feats = $SeqPos_Feats{$PosSeqfeat}";
		
		foreach $seqfeatpos (@currseqfeat)
			{
				#my $delct = 0;
				
				my $refpossymb;
				#$refcurrsqftVardef = $refcurrsqftVardef."_".$seqfeatpos."_".$Alleles_Vardefunits->{$Allele}->{$refcurr};
				#$refpossymb = $alignseqnumb[$translseqfeatpos{$refpossymb}];
				$refpossymb = $seqfeatpos;
				#foreach $refpossymb (keys %{$refseqnumb->{$seqfeatpos}}) 
					#{
						#my $curralignseqpos = $translseqfeatpos{$refpossymb};
						my $currseqpos = $translseqfeatpos{$refpossymb};
						#my $currrefresd = $Alleles_Vardefunits->{$refallele}->{$curralignseqpos};
						my $currrefresd = $Alleles_Posamino->{$fullref4digallele}[$currseqpos];
						my $currvardefresd = $Alleles_Vardefunits->{$fullref4digallele}[$currseqpos];

#					print debug "\nInside the posseqfeat loop, line 982:\ttranslpos = $translseqfeatpos{$refpossymb}\talignseqnummb = $alignseqnumb[$currseqpos]\tcurralignseqpos = $currseqpos\trefpossymb = $refpossymb\tcurrrefresidue = $currrefresd\tcurrvardefresidue = $currvardefresd";
#					print debug "\nInside the posseqfeat loop, line 982:\ttranslpos = $translseqfeatpos{$refpossymb}\talignseqnummb = $alignseqnumb[$currseqpos]\tcurralignseqpos = $currseqpos\trefpossymb = $refpossymb\tcurrrefresidue = $currrefresd\tcurrvardefresidue = $currvardefresd" if ($PosSeqfeat eq "");
						#$refcurrsqftVardef = $refcurrsqftVardef."_".$seqfeatpos."_".$currrefresd;
						#$refcurrsqftMotif = $refcurrsqftMotif.$currrefresd;
						
						#$refcurrsqftVardef->{$seqfeatpos} = $refcurrsqftVardef->{$seqfeatpos}."_".$seqfeatpos."_".$currrefresd;
						#$refcurrsqftMotif->{$seqfeatpos} = $refcurrsqftMotif->{$seqfeatpos}.$currrefresd;
						
						#$allelesqftVardef->{$refallele}->{$PosSeqfeat} = $allelesqftVardef->{$refallele}->{$PosSeqfeat}."_".$seqfeatpos."_".$currrefresd;
						#$allelesqftMotif->{$refallele}->{$PosSeqfeat} = $allelesqftMotif->{$refallele}->{$PosSeqfeat}.$currrefresd;
						$allelesqftVardef->{$fullref4digallele}->{$PosSeqfeat} = $allelesqftVardef->{$fullref4digallele}->{$PosSeqfeat}.$vardefdelim.$currvardefresd;
						$allelesqftMotif->{$fullref4digallele}->{$PosSeqfeat} = $allelesqftMotif->{$fullref4digallele}->{$PosSeqfeat}.$currrefresd;

						push @{$alignseqposits->{$PosSeqfeat}}, $currseqpos;
#					print debug "\nInside the posseqfeat loop, line 995\talignseqposits after pushing $currseqpos = @{$alignseqposits->{$PosSeqfeat}}";
					#}					
			}
		#$tempmetastr = quotemeta $vardefdelim;
		$allelesqftVardef->{$fullref4digallele}->{$PosSeqfeat} =~ s/^($vardefmetadelim)//i;
		
		my $aminocombo = $allelesqftMotif->{$fullref4digallele}->{$PosSeqfeat};
		my $vardefseq = $allelesqftVardef->{$fullref4digallele}->{$PosSeqfeat};
		
		print debug "\nLine 1003: posseqfeat = $PosSeqfeat\taminocombo = $aminocombo\tvardefseq = $vardefseq";
		############# Check to see if the aminocombo is unknown or not before giving the "Variant Type" value

		if ($aminocombo =~ /^([A-Z\.]+)$/i)
			{
				$Alleles_CustSqftType->{$PosSeqfeat}{$fullref4digallele}{$aminocombo} = $reftype;
				$Alleles_PosTypes->{$PosSeqfeat}{$aminocombo} = $reftype; 
				$Vardef_CustSqftType->{$PosSeqfeat}->{$reftype} = $vardefseq; 
				$Motif_CustSqftType->{$PosSeqfeat}->{$reftype} = $aminocombo;
				#$Alleles_CustSqft->{$PosSeqfeat}{$aminocombo} = $Alleles_CustSqft->{$PosSeqfeat}{$aminocombo}.", ".$HLA_Locus."*".$ref4digallele;
				$Alleles_CustSqft->{$PosSeqfeat}{$aminocombo} = $Alleles_CustSqft->{$PosSeqfeat}{$aminocombo}.$allelegrpdelim.$Ref_HLA_Locus."*".$ref4digallele;
			}
		else
			{
				#$Alleles_CustUnkSqft->{$PosSeqfeat} = $Alleles_CustUnkSqft->{$PosSeqfeat}.", ".$HLA_Locus."*".$ref4digallele;
				$Alleles_CustUnkSqft->{$PosSeqfeat} = $Alleles_CustUnkSqft->{$PosSeqfeat}.$allelegrpdelim.$Ref_HLA_Locus."*".$ref4digallele;
				#$Alleles_CustSqftType->{$PosSeqfeat}{$fullref4digallele}{Unknown} = "Unknown Variant Type";
				$Alleles_CustSqftType->{$PosSeqfeat}{$fullref4digallele}{Unknown} = "Unknown Variant Type";
			}
	}
############################## Get reference allele var def/motif seq for each sequence feature and then assign var type = 1


############################## Get each allele var def/motif seq for each sequence feature and then assign var types

foreach $dig4allele (sort keys %fin4digAlleleSeqs) 
	{
		
		my $seqfeatposct = 1;
		my $Typect = 0;
		
		my $PosSeqfeat;

		my $Allele = $dig4allele; 
		##print debug "\nLine 646:Allele = $Allele";
		$Allele =~ s/$Alleletag\*//i; 
		


		#if ($Allele =~ /^($ref4digallele)$/i) 
		if ($dig4allele =~ /^($Ref_HLA_Locus(\*)$ref4digallele)$/i) 
			{
				##print debug "\nEncountered reference allele - $dig4allele\tAllele = $Allele";
#				print SqftVarntsfile "\n$Locus\t\t$Alleleseqfeat\t$Featuretypes[6]\t$HLA_Locus*$Allele\t\t$HLA_Locus*$Allele\t\t" if ($HLA_Locus =~ /^ *($Ref_HLA_Locus) *$/i);
				print SqftVarntsfile "\n$alleleseqfeatno\t\t$Alleleseqfeat\t$Featuretypes[6]\t$HLA_Locus*$Allele\t\t\t$HLA_Locus*$Allele" if ($HLA_Locus =~ /^ *($Ref_HLA_Locus) *$/i);
				next;
			}
		#$Allele = s/$Alleletag//; 
		##print debug "\nInside the fin4digalleleseqs loop, line near 646:\tdig4allele = $dig4allele\tAllele = $Allele\tAllele tag = $Alleletag";
		
		print SqftVarntsfile "\n$alleleseqfeatno\t\t$Alleleseqfeat\t$Featuretypes[6]\t$HLA_Locus*$Allele\t\t\t$HLA_Locus*$Allele";
#		print SqftVarntsfile "\n$Locus\t\t$Alleleseqfeat\t$Featuretypes[6]\t$HLA_Locus*$Allele\t\t$HLA_Locus*$Allele\t\t" if ($HLA_Locus =~ /^ *($Ref_HLA_Locus) *$/i);

#		foreach  $PosSeqfeat (@SeqPosFeats)
		foreach  $seqposfeatkey (sort by_number keys %SeqPosFeats)			
			{
				$PosSeqfeat = $SeqPosFeats{$seqposfeatkey};	
				##print debug "\nInside the fin4digalleleseqs loop's seqposfeats loop, line 650:\tPosseqfeat = $PosSeqfeat";
				
				if ($defseqfeatflag{$PosSeqfeat} == 1) 
					{
						next;
					}	
				#my @currseqfeat = split /,/, $PosSeqfeat;
				#my $currseqfeatlen = scalar @currseqfeat;
				#my $seqfeatpos;
				my $curralignseqpos;
				#my $refcurrsqftVardef;
				#my $refcurrsqftMotif;
		
				my @currseqfeat = @{$alignseqposits->{$PosSeqfeat}};				
				
				##print debug "\nInside the posseqfeat loop, line 671:\tcurrseqfeat = @currseqfeat";

				foreach $curralignseqpos (@currseqfeat) 
					{
						#my $delct = 0;
				
						#my $refpossymb;
						#$refcurrsqftVardef = $refcurrsqftVardef."_".$seqfeatpos."_".$Alleles_Vardefunits->{$Allele}->{$refcurr};
						my $seqfeatpos = $alignseqnumb[$curralignseqpos];

						my $currresd = $Alleles_Posamino->{$dig4allele}->[$curralignseqpos];
						my $currvardefresd = $Alleles_Vardefunits->{$dig4allele}->[$curralignseqpos];

						#$allelesqftVardef->{$Allele}->{$PosSeqfeat} = $allelesqftVardef->{$Allele}->{$PosSeqfeat}."_".$seqfeatpos."_".$currresd;
						$allelesqftVardef->{$dig4allele}->{$PosSeqfeat} = ($allelesqftVardef->{$dig4allele}->{$PosSeqfeat}).$vardefdelim.$currvardefresd;
						$allelesqftMotif->{$dig4allele}->{$PosSeqfeat} = ($allelesqftMotif->{$dig4allele}->{$PosSeqfeat}).$currresd;
						##print debug "\nInside the currseqfeat loop, line 685:\tcurralignseqpospos = $curralignseqpos\tseqfeatpos = $seqfeatpos\tcurrresidue = $currresd\tcurrvardefresidue = $currvardefresd";
					}
				#foreach $amino (@seqaminos) 
				
				#$allelesqftVardef->{$Allele}->{$PosSeqfeat} =~ s/^_//i;
				#$tempmetastr = quotemeta $vardefdelim;
				$allelesqftVardef->{$dig4allele}->{$PosSeqfeat} =~ s/^($vardefmetadelim)//i;
		

				my $vardefseq = $allelesqftVardef->{$dig4allele}->{$PosSeqfeat};
				my $aminocombo = $allelesqftMotif->{$dig4allele}->{$PosSeqfeat};
				
				##print debug "\nInside the posseqfeat loop, line 693:\tpos = $PosSeqfeat\tvardefseq = $vardefseq\taminocombo = $aminocombo";
					
				if ($aminocombo =~ /^([A-Z\.]+)$/i)
					{
						#$tempcharct = $charct-1;
#						$Typect = keys %{$Motif_CustSqftType->{$PosSeqfeat}};
						$Typect = max (keys %{$Motif_CustSqftType->{$PosSeqfeat}});
						##print debug "\nThe Allele-$Allele\tAmino - $amino\tTypect - $Typect";
						##print debug "\n Before exist for amino alleles are - \t",$Alleles_CustSqft->{$charct}{$amino};
						
						if (!(exists $Alleles_CustSqft->{$PosSeqfeat}{$aminocombo})) 
							{
								#$Alleles_CustSqftType->{$Allele}{$seqfeatposct}{$amino} = $Typect+1;
								
								
								if (!(defined $Alleles_PosTypes->{$PosSeqfeat}{$aminocombo})) 
									{
										$Alleles_PosTypes->{$PosSeqfeat}{$aminocombo} = $Typect+1;
										$Alleles_CustSqftType->{$PosSeqfeat}{$dig4allele}{$aminocombo} = $Typect+1;
									}
								else
									{
										$Alleles_CustSqftType->{$PosSeqfeat}{$dig4allele}{$aminocombo} = $Alleles_PosTypes->{$PosSeqfeat}{$aminocombo};
									}
#								$Alleles_PosTypes->{$PosSeqfeat}{$aminocombo} = $Typect+1;
#								$Motif_CustSqftType->{$PosSeqfeat}->{$Alleles_PosTypes->{$PosSeqfeat}{$aminocombo}} = $aminocombo;
								##print debug "\n In exist for amino, the \ttype is - $Alleles_CustSqftType->{$charct}{$Allele}{$amino}";
								$Vardef_CustSqftType->{$PosSeqfeat}->{$Alleles_PosTypes->{$PosSeqfeat}{$aminocombo}} = $vardefseq; 
								
							}
						else 
							{
								$Alleles_CustSqftType->{$PosSeqfeat}{$dig4allele}{$aminocombo} = $Alleles_PosTypes->{$PosSeqfeat}{$aminocombo};
							}

						#$Alleles_CustSqft->{$PosSeqfeat}{$aminocombo} = $Alleles_CustSqft->{$PosSeqfeat}{$aminocombo}.", ".$HLA_Locus."*".$Allele;
						$Alleles_CustSqft->{$PosSeqfeat}{$aminocombo} = $Alleles_CustSqft->{$PosSeqfeat}{$aminocombo}.$seqposdelim.$HLA_Locus."*".$Allele;
						##print debug "\nOutside Exist the Alleles are: \t$Alleles_CustSqft->{$charct}{$amino}";

						$Motif_CustSqftType->{$PosSeqfeat}{$Alleles_CustSqftType->{$PosSeqfeat}{$dig4allele}{$aminocombo}} = $aminocombo;
						##print debug "\nOutside Exist the charct- $charct Type- ",$Alleles_CustSqftType->{$charct}{$Allele}{$amino}," Allele- $Allele amino- $amino Motif is: \t",$Motif_CustSqftType->{$charct}{$Alleles_CustSqftType->{$charct}{$Allele}{$amino}};
					}
				else
					{
						#$Alleles_CustUnkSqft->{$PosSeqfeat} = $Alleles_CustUnkSqft->{$PosSeqfeat}.", ".$HLA_Locus."*".$Allele;
						$Alleles_CustUnkSqft->{$PosSeqfeat} = $Alleles_CustUnkSqft->{$PosSeqfeat}.$seqposdelim.$HLA_Locus."*".$Allele;
						#$Alleles_CustSqftType->{$Allele}{$charct}{Unknown} = "Unknown Variant Type";
						$Alleles_CustSqftType->{$PosSeqfeat}{$dig4allele}{Unknown} = "Unknown Variant Type";
						#$Alleles_CustSqftType->{$PosSeqfeat}{$Allele}{Unknown} = "Unknown Variant Type";
					}
				#$Alleles_Posamino->{$Allele}{$charct} = $amino;	
				#$seqfeatposct++;
					
			}
	}

############################## End of Get each allele var def/motif seq for each sequence feature and then assign var types




############################## Identifying in the posseqfeat redundancy report about the posseqfeats that share the currposseqfeat and has no change in no. of var types

#foreach $pos (@SeqPosFeats)
foreach $seqposfeatkey (sort by_number keys %SeqPosFeats)
	{
		$pos = $SeqPosFeats{$seqposfeatkey};
		my $tempposresd;
		my $temppostypes;
		my $tempmodfdvarntposits;

		#my @temppsfposists = split  /, /, $pos;
		#$tempmetastr = quotemeta $seqposdelim;
		my @temppsfposists = split  /$seqposmetadelim/, $pos;
		
		$SeqFeatVarTypes{$pos} = keys %{$Alleles_CustSqft->{$pos}};
		$temppostypes = $SeqFeatVarTypes{$pos};

		$possfvarntflag{$pos} = 0 if ($temppostypes <=1);
		$possfvarntflag{$pos} = 1 if ($temppostypes > 1);
		my $positsct = 0;
		
		foreach $tempposresd (sort @temppsfposists) 
			{
				my $tempposresdtypes = keys %{$Alleles_CustSqft->{$tempposresd}};
				$possfvarntflag{$tempposresd} = 0 if ($tempposresdtypes <=1);
				$possfvarntflag{$tempposresd} = 1 if ($tempposresdtypes > 1);
				
				if ($possfvarntflag{$tempposresd} == 1) 
					{
						$sortdvarnttempseqposits{$pos} = $sortdvarnttempseqposits{$pos}." ".$tempposresd.",";
						#$possfvarposits->{$pos}->[$positsct] = $tempposresd;
						push @{$possfvarposits->{$pos}}, $tempposresd;
						$tempmodfdvarntposits = $tempmodfdvarntposits."( ".$tempposresd.",){1}.*";
						$positsct++;
					}
			}
		$sortdvarnttempseqposits{$pos} = ",".$sortdvarnttempseqposits{$pos};
		$sortdmodfdvarntseqposits{$pos} = $tempmodfdvarntposits;

		my $currtempseqpos;
		#my $tempseqposno = $SeqFeat_no{$pos}-$seqftstartct-1;
#		my $tempseqposno = $SeqFeat_no{$pos}-2;
		my $tempseqposno = $SeqFeat_no{$pos};
		foreach  $currtempseqpos(sort keys %sortdvarnttempseqposits) 
			{
				my $largerseqpos;
				my $smallerseqpos;
				#my @currtempseqposits = split /,/, $sortdvarnttempseqposits{$pos};
				my @currtempseqposits = Splitrow($sortdvarnttempseqposits{$pos},$seqposdelim);
				my $complen = @currtempseqposits-1;
				print debug "\nLine 1048: Inside the currtempseqpos for varntposits: Complen = $complen\tpositsct = $positsct\tFor sortdvarnttempseqposits $sortdvarnttempseqposits{$pos}\tfor pos = $pos";
				#if ($SeqFeatlen{$currtempseqpos} <= $SeqFeatlen{$pos}) 
				if ($complen <= $positsct) 
					{
						$largerseqpos = $pos;
						$smallerseqpos = $currtempseqpos;
					}
				#elsif ($SeqFeatlen{$currtempseqpos} > $SeqFeatlen{$pos})
				elsif ($complen > $positsct)
					{
						$largerseqpos = $currtempseqpos;
						$smallerseqpos = $pos;
					}
				
				#$tempseqposno = $SeqFeat_no{$largerseqpos}-$seqftstartct-1;
#				$tempseqposno = $SeqFeat_no{$largerseqpos}-2;
				$tempseqposno = $SeqFeat_no{$largerseqpos};
#				my $smallertempseqposno = $SeqFeat_no{$smallerseqpos}-2;
				my $smallertempseqposno = $SeqFeat_no{$smallerseqpos};
				my $currsortdseqposits = $sortdvarnttempseqposits{$largerseqpos};
				my $currmodfdseqposits = $sortdmodfdvarntseqposits{$smallerseqpos};

				#my $currsortdtempseqpos = $sortdvarnttempseqposits{$currtempseqpos};
				print debug "\nLine 1135: Inside the posseqfeats loop near possfredundcys = $possfredundcys{$smallerseqpos}\tfor tempseqposno = $tempseqposno\tFor smallerpos = $smallerseqpos, largerpos = $largerseqpos:\tcurrsortdseqposits = $currsortdseqposits\tcurrmodfdseqposits = $currmodfdseqposits";
				if (($currsortdseqposits =~ /($currmodfdseqposits)/i) && ($SeqFeatVarTypes{$smallerseqpos} == $SeqFeatVarTypes{$largerseqpos}) && ($currsortdseqposits ne "") && ($currmodfdseqposits ne ""))
				#if ($currsortdseqposits =~ /$currmodfdseqposits/i)
					{
						#$tempmetastr = quotemeta $featnodelim;
						if (defined $possfredundcys{$smallerseqpos}) 
							{
								#if (!($possfoccurs{$resdpos} =~ /(; )$Seqfeatname$SeqFeat_no{$tempseqpos}(; )/i))
								print debug "\nLine 1147: Inside the match of \t$1\$2\tTo currsortdseqposits = $currsortdseqposits";
								
								if (!($possfredundcys{$smallerseqpos} =~ /($featnometadelim)$tempseqposno($featnometadelim)/i))
									{
										#$possfoccurs{$resdpos} = $possfoccurs{$resdpos}.$Seqfeatname$SeqFeat_no{$tempseqpos}."; ";
										$possfredundcys{$smallerseqpos} = $possfredundcys{$smallerseqpos}.$tempseqposno.$featnodelim;
										print debug "\nLine 1145: Inside thecurrsortdtempseqposits loop for defined:\tsmallerpos = $smallerseqpos :The match = \t$1\t$2\$3\tfor possfredundcys = $possfredundcys{$smallerseqpos}";
									}
							}
						else
							{
								#$possfoccurs{$resdpos} = "; ".$Seqfeatname$SeqFeat_no{$tempseqpos}."; ";
								$possfredundcys{$smallerseqpos} = $featnodelim.$tempseqposno.$featnodelim;
								print debug "\nLine 1153: Inside thecurrsortdtempseqposits loop for not defined:\tsmallerpos = $smallerseqpos :The match = \t$1\t$2\$3\tfor possfredundcys = $possfredundcys{$smallerseqpos}";
							}
						if (defined $possfredundcys{$largerseqpos}) 
							{
								#if (!($possfoccurs{$resdpos} =~ /(; )$Seqfeatname$SeqFeat_no{$tempseqpos}(; )/i))
								print debug "\nLine 1168: Largerseqpos = $largerseqpos\tpossfredundcys = $possfredundcys{$largerseqpos}\tInside the match of \t$1\$2\tTo currsortdseqposits = $currsortdseqposits";
								if (!($possfredundcys{$largerseqpos} =~ /($featnometadelim)$smallertempseqposno($featnometadelim)/i))
									{
										#$possfoccurs{$resdpos} = $possfoccurs{$resdpos}.$Seqfeatname$SeqFeat_no{$tempseqpos}."; ";
										$possfredundcys{$largerseqpos} = $possfredundcys{$largerseqpos}.$smallertempseqposno.$featnodelim;
										print debug "\nLine 1173: Inside thecurrsortdtempseqposits loop for defined:\tlargerseqpos = $largerseqpos :The match = \t$1\t$2\$3\tfor possfredundcys = $possfredundcys{$largerseqpos}";
									}
								if (!($possfredundcys{$largerseqpos} =~ /($featnometadelim)$tempseqposno($featnometadelim)/i))
									{
										#$possfoccurs{$resdpos} = $possfoccurs{$resdpos}.$Seqfeatname$SeqFeat_no{$tempseqpos}."; ";
										$possfredundcys{$largerseqpos} = $possfredundcys{$largerseqpos}.$tempseqposno.$featnodelim;
										print debug "\nLine 1179: Inside thecurrsortdtempseqposits loop for defined:\tlargerseqpos = $largerseqpos :The match = \t$1\t$2\$3\tfor possfredundcys = $possfredundcys{$largerseqpos}";
									}								
							}
						else
							{
								#$possfoccurs{$resdpos} = "; ".$Seqfeatname$SeqFeat_no{$tempseqpos}."; ";
								$possfredundcys{$largerseqpos} = $featnodelim.$smallertempseqposno.$featnodelim.$tempseqposno.$featnodelim;
								print debug "\nLine 1174: Inside thecurrsortdtempseqposits loop for not defined:\tlargerseqpos = $largerseqpos :The match = \t$1\t$2\$3\tfor possfredundcys = $possfredundcys{$largerseqpos}";
							}
					}
			}
	}	
	

############################## End of Identifying in the posseqfeat redundancy report about the posseqfeats that share the currposseqfeat and has no change in no. of var types



############################## Printing of the sequence feature variants file and creating the allele variants header

#$charct = @customPosits;
#my $pos = 1;
my $pos;
#my $alleleseqfeatno = $Seqfeatname."1";
#my $alleleseqfeatno = $Seqfeatname.($SeqFeat_no{$Alleleseqfeat});

#print IndivSeqfeat "\nAllele\t$Featuretypes[4]\t\t\t";
#print IndivSeqfeat "\n$Locus\t$alleleseqfeatno\t\t",$Seqfeatlabel,"_",$Alleleseqfeat,"\t$Featuretypes[4]\t\t";
if ($Alleleseqfeat =~ /$Seqfeatlabel/i) 
	{
#		print IndivSeqfeat "\n$Locus\t$alleleseqfeatno\t\t",$Alleleseqfeat,"\t$Featuretypes[6]\t\t";
		print IndivSeqfeat "\n$alleleseqfeatno\t\t",$Alleleseqfeat,"\t$Featuretypes[6]\t\t";
		$AlleleVarntshdrs[0] = "\t$Alleleseqfeat\t\t";
	}
else
	{
#		print IndivSeqfeat "\n$Locus\t$alleleseqfeatno\t\t",$Seqfeatlabel,"_",$Alleleseqfeat,"\t$Featuretypes[6]\t\t";
		print IndivSeqfeat "\n$alleleseqfeatno\t\t",$Seqfeatlabel,"_",$Alleleseqfeat,"\t$Featuretypes[6]\t\t";
		$AlleleVarntshdrs[0] = "\t",$Seqfeatlabel,"_",$Alleleseqfeat,"\t\t";
	}

#print IndivSeqfeat "\n$Locus\t$alleleseqfeatno\t\t",$Seqfeatlabel,"_",$Alleleseqfeat,"\t$Featuretypes[4]\t\t";
#print SqftVarntsfile "\n$Locus\t$Alleleseqfeat\t$Featuretypes[4]\t$HLA_Locus*$Allele\t\t$Allele\t\t";
#$AlleleVarntshdr = "\t$alleleseqfeatno-Motif\t$alleleseqfeatno-Variant Type\t$alleleseqfeatno-Variant Type Definition";
#$AlleleVarntshdrs[1] = "\t$alleleseqfeatno-Motif\t$alleleseqfeatno-Variant Type\t$alleleseqfeatno-Variant Type Definition";
$AlleleVarntshdrs[1] = "Allele name\t$alleleseqfeatno-Motif\t$alleleseqfeatno-Variant Type\t$alleleseqfeatno-Variant Type Definition";
#$AlleleVarntshdrs[1] = "\t$Alleleseqfeat\t\t";

#foreach $pos (@SeqPosFeats)
foreach $seqposfeatkey (sort by_number keys %SeqPosFeats)
	{
		$pos = $SeqPosFeats{$seqposfeatkey};
		my $tempseqfeatno= ($Seqfeatname).($SeqFeat_no{$pos});
		#$SeqFeatVarTypes{$pos} = ;
		#$SeqPos_Feattypes{$pos} =~ s/(; ?)$//i;
		#$SeqPos_Feats{$pos} =~ s/(; ?)$//i;
		print IndivSeqfeat "\n$tempseqfeatno\t$SeqPos_Featfrmtpos{$pos}\t$SeqPos_Feats{$pos}\t$SeqPos_Feattypes{$pos}\t\t";
						
		if (keys %{$Alleles_CustSqft->{$pos}} == 1) 
			{
				##print debug "\n In print Non Variant seqfeat Pos $pos \tkeys = ",keys %{$Alleles_CustSqft->{$pos}};
				my $tempresidue = $Motif_CustSqftType->{$pos}->{1};
				my $refresidue = $Motif_CustSqftType->{$pos}->{$reftype};
				my $tempvardefresidue = $Vardef_CustSqftType->{$pos}->{1};
				##print debug "\nInside the non variant sfvt  loop, line 742:\ttempresidue = $tempresidue\trefresidue = $refresidue\ttempvardefresidue = $tempvardefresidue";
				$SeqFeatVarTypes{$pos} = keys %{$Alleles_CustSqft->{$pos}};
				$possfvarntflag{$pos} = 0;
					
				
				##print debug "\n In print Non Variant seqfeat Pos $pos \ttempresidue = $tempresidue";
				#$Alleles_CustSqft->{$pos}{$tempresidue} =~ s/^, //;
				#$tempmetastr = quotemeta $allelegrpdelim;
				$Alleles_CustSqft->{$pos}{$tempresidue} =~ s/^($allelegrpmetadelim)//;
				#print Unk_NonVarntsfile "\nHLA-$HLA_Locus\t$pos\t$tempresidue\t$Alleles_CustSqft->{$pos}{$tempresidue}";
				print Unk_NonVarntsfile "\n$Locus\t$tempseqfeatno\t$SeqPos_Featfrmtpos{$pos}\t$tempresidue\t$Alleles_CustSqft->{$pos}{$tempresidue}\t$SeqPos_Feats{$pos}";
				##print debug "\n In print Non Variant seqfeat Pos $pos \talleles are = \t",$Alleles_CustSqft->{$pos}{$tempresidue};
				#print IndivSeqfeat "\n$Locus\t$tempseqfeatno\t$SeqPos_Featfrmtpos{$pos}\t$SeqPos_Feats{$pos}\t$SeqPos_Feattypes{$pos}\t\t";

			}
		if (keys %{$Alleles_CustSqft->{$pos}} >= 1)
			{
				#print IndivSeqfeat "\nmature protein position $pos\tStructural - variant location\t$pos\t\t";
				$SeqFeatVarTypes{$pos} = keys %{$Alleles_CustSqft->{$pos}};
				$possfvarntflag{$pos} = 1 if ($SeqFeatVarTypes{$pos} > 1);

				#print IndivSeqfeat "\n$Locus\t$tempseqfeatno\t$SeqPos_Featfrmtpos{$pos}\t$SeqPos_Feats{$pos}\t$SeqPos_Feattypes{$pos}\t\t";
				my @aminovarnttypes = sort {$a <=> $b} (values %{$Alleles_PosTypes->{$pos}});
#				@aminovarnttypes = sort {$a <=> $b} @aminovarnttypes;
#				foreach $aminotypect (1..keys %{$Alleles_CustSqft->{$pos}}) 
				foreach $aminotypect (@aminovarnttypes) 
					{
						##print debug "\n In print Seqfeat Pos $pos Aminoct $aminoct\tkeys = ",keys %{$Alleles_CustSqft->{$pos}};
						my $tempresidue = $Motif_CustSqftType->{$pos}->{$aminotypect};
						my $refresidue = $Motif_CustSqftType->{$pos}->{$reftype};
						my $tempvardefresidue = $Vardef_CustSqftType->{$pos}->{$aminotypect}; 
						

						##print debug "\nInside the aminotypect loop, line 762:\tpos = $pos\taminotypect = $aminotypect\ttempresidue = $tempresidue\trefresidue = $refresidue\ttempvardefresidue = $tempvardefresidue";

						##print debug "\n In print Seqfeat Pos $pos Aminoct $aminoct\ttempresidue = $tempresidue";
						#$Alleles_CustSqft->{$pos}{$tempresidue} =~ s/^, //;
						#$tempmetastr = quotemeta $seqposdelim;
						$Alleles_CustSqft->{$pos}{$tempresidue} =~ s/^$seqposmetadelim//;
						#print SqftVarntsfile "\nHLA-$HLA_Locus\t$Sqftname $pos\tStructural - variant location\t$Alleles_CustSqft->{$pos}{$tempresidue}\t$tempresidue\tType ",$aminoct,"\t$pos\t$pos$tempresidue";
#						print SqftVarntsfile "\n$Locus\t$SeqPos_Featfrmtpos{$pos}\t$SeqPos_Feats{$pos}\t$SeqPos_Feattypes{$pos}\t$Alleles_CustSqft->{$pos}{$tempresidue}\t$tempresidue\t",$tempseqfeatno,"_",$Typename.$aminotypect,"\t$SeqPos_Featfrmtpos{$pos}\t$tempvardefresidue";
						print SqftVarntsfile "\n$tempseqfeatno\t$SeqPos_Featfrmtpos{$pos}\t$SeqPos_Feats{$pos}\t$SeqPos_Feattypes{$pos}\t",$tempseqfeatno,"_",$Typename.$aminotypect,"\t$tempresidue\t$tempvardefresidue\t$Alleles_CustSqft->{$pos}{$tempresidue}";
						##print debug "\n In print Seqfeat Pos $pos \talleles are = \t",$Alleles_CustSqft->{$pos}{$tempresidue};
					}
				
				#$AlleleVarntshdr = $AlleleVarntshdr."\t$Sqftname $pos-Motif\t$Sqftname $pos-Variant Type\t$Sqftname $pos-Variant Type Definition";
				#$AlleleVarntshdr = $AlleleVarntshdr."\t$tempseqfeatno-Motif\t$tempseqfeatno-Variant Type\t$tempseqfeatno-Variant Type Definition";
				$AlleleVarntshdrs[1] = $AlleleVarntshdrs[1]."\t$tempseqfeatno-Motif\t$tempseqfeatno-Variant Type\t$tempseqfeatno-Variant Type Definition";
				$AlleleVarntshdrs[0] = $AlleleVarntshdrs[0]."\t$SeqPos_Feats{$pos}\t\t";
			}
		
		if (exists $Alleles_CustUnkSqft->{$pos})
			{
				
				#$Alleles_CustUnkSqft->{$pos} =~ s/^, //;
				#$tempmetastr = quotemeta $seqposdelim;
				$Alleles_CustUnkSqft->{$pos} =~ s/^($seqposmetadelim)//;
				if (keys %{$Alleles_CustSqft->{$pos}} >= 1) 
					{	
						#print SqftVarntsfile "\nHLA-$HLA_Locus\t$Sqftname $pos\tStructural - variant location\t$Alleles_CustUnkSqft->{$pos}\t*\tUnknown Motif Type\t$pos\tUnknown Variant Type";
#						print SqftVarntsfile "\n$Locus\t$SeqPos_Featfrmtpos{$pos}\t$SeqPos_Feats{$pos}\t$SeqPos_Feattypes{$pos}\t$Alleles_CustUnkSqft->{$pos}\t$unkmotiflabel\t$tempseqfeatno", "_", "$unktypelabel\t$SeqPos_Featfrmtpos{$pos}\t$unkmotiflabel";
						print SqftVarntsfile "\n$tempseqfeatno\t$SeqPos_Featfrmtpos{$pos}\t$SeqPos_Feats{$pos}\t$SeqPos_Feattypes{$pos}\t$tempseqfeatno", "_", "$unktypelabel\t$unkmotiflabel\t$unkmotiflabel\t$Alleles_CustUnkSqft->{$pos}";
						#print SqftVarntsfile "\n$Locus\t$pos\t$SeqPos_Feats{$pos}\t$SeqPos_Feattypes{$pos}\t$Alleles_CustUnkSqft->{$pos}\t*\tUnknown Motif Type\t$pos\tUnknown Variant Type";
					}
				print Unk_NonVarntsfile "\n$Locus\t$tempseqfeatno\t$SeqPos_Featfrmtpos{$pos}\t*\t$Alleles_CustUnkSqft->{$pos}\t$SeqPos_Feats{$pos}";
				#print Unk_NonVarntsfile "\nHLA-$HLA_Locus\t$pos\t*\t",$Alleles_CustUnkSqft->{$pos};
			}
	}

############################## End of Printing of the sequence feature variants file and creating the allele variants header




############################## Printing of the allele variants file

#print AlleleVarntsfile "$AlleleVarntshdr\n";
print AlleleVarntsfile "$AlleleVarntshdrs[0]\n";
print AlleleVarntsfile "$AlleleVarntshdrs[1]\n";

print debug "\nLine 1584: \tSeqPosFeats{2} = $SeqPosFeats{2}\t",keys %{$Alleles_CustSqftType->{$SeqPosFeats{$seqftstartct+1}}}; 
#print debug "\n\nLine 1584: The between Seqfeat and Allele print the AllelecustSqfttype - Type $Alleles_CustSqftType->{9}{0301}{E}";
my $temptypect = 0;
#foreach $Allele (sort keys %{$Alleles_CustSqftType->{1}}) 

foreach $Allele (sort keys %{$Alleles_CustSqftType->{$SeqPosFeats{$seqftstartct+1}}}) 
	{
		#print AlleleVarntsfile "\n$HLA_Locus*$Allele";
		#print AlleleVarntsfile "\n$HLA_Locus*$Allele\t\t",$Allele,"\t";
		#print AlleleVarntsfile "\n$Allele\t\t",$Allele,"\t";
		#print AlleleVarntsfile "\n$HLA_Locus*$Allele\t\t$HLA_Locus*$Allele\t";
		#print AlleleVarntsfile "\n$HLA_Locus*$Allele\t\t$HLA_Locus*$Allele\t";
		if ((!($HLA_Locus =~ /^ *($Ref_HLA_Locus) *$/i)) && ($Allele =~ /($Ref_HLA_Locus)/i))
			{
				next;
			}
		print AlleleVarntsfile "\n$Allele\t\t$Allele\t";
		#print AlleleVarntsfile "\n$HLA_Locus*$Allele\t\t",$Seqfeatlabel,"*",$Allele,"\t";
		$temptypect++;
		#print SqftVarntsfile "\n$Locus\tAllele\t$Featuretypes[6]\t$HLA_Locus*$Allele\t\t$HLA_Locus*$Allele\t\t";
		#print SqftVarntsfile "\n$Locus\tAllele\t$Featuretypes[4]\t$Allele\t\t$Allele\t\t";

#		foreach $pos (@SeqPosFeats)
		foreach $seqposfeatkey (sort by_number keys %SeqPosFeats)			
			{
				$pos = $SeqPosFeats{$seqposfeatkey};
				if (keys %{$Alleles_CustSqft->{$pos}} >= 1)
					{
						my $tempseqfeatno= $Seqfeatname.$SeqFeat_no{$pos};
						#my $tempresidue = $Alleles_Posamino->{$Allele}[$pos-1];
						my $tempresidue = $allelesqftMotif->{$Allele}->{$pos};
						my $tempvardefseq = $allelesqftVardef->{$Allele}->{$pos};
						##print debug "\nIn allele print Tempresidue = ",$tempresidue,"\t\t Alternatively the Alleles posamino at Allele- $Allele and at Pos- $pos = ", ${$Alleles_Posamino->{$Allele}}[$pos];
						if ($tempresidue =~ /\*/i)
						#if ($tempresidue =~ /[\*\?]/i) 
							{
								#print AlleleVarntsfile "\t$tempresidue\tUnknown Motif Type\tUnknown Variant Type";
								print AlleleVarntsfile "\t$tempresidue\t$tempseqfeatno", "_", "$unktypelabel\t$unkmotiflabel";
							}
						else
							{
								#print AlleleVarntsfile "\t$tempresidue\tType ",$Alleles_CustSqftType->{$pos}{$Allele}{$tempresidue},"\t$pos$tempresidue";
								print AlleleVarntsfile "\t$tempresidue\t",$tempseqfeatno,"_",$Typename,($Alleles_CustSqftType->{$pos}{$Allele}{$tempresidue}),"\t$tempvardefseq";
								##print debug "\nInside Allele Print Pos- $pos Allelel- $Allele Tempresidue- $tempresidue \t$Alleles_CustSqftType->{$pos}{$Allele}{$tempresidue} OR $Alleles_PosTypes->{$pos}{$tempresidue}";
							}
					}
			}
	}

############################## End of Printing of the allele variants file


############################## Printing the Occurances and Redundancy reports
my $occrcyseqpos;


foreach $occrcyseqpos (sort keys %possfoccurs) 
	{
		my @seqfeatnames;

		$possfoccurs{$occrcyseqpos} =~ s/($featnometadelim)$//i;
		$possfoccurs{$occrcyseqpos} =~ s/^($featnometadelim)//i;

		my @posseqfeats = Splitrow($possfoccurs{$occrcyseqpos},$featnodelim);
		#my @posseqfeats = split /; /, $possfoccurs{$occrcyseqpos};
		my $noposseqfeats = @posseqfeats;
		
		my $seqfeatnames;
		my $posseqfeats;
		my $posseqfeatnos;
		my $posseqpos; 
		my $tempseqfeatno;

		
		foreach $tempseqfeatno (@posseqfeats) 
			{
				my $tempseqpos = $SeqPosFeats{$tempseqfeatno};
				$posseqfeatnos = $posseqfeatnos.$featnodelim.($Seqfeatname.$SeqFeat_no{$tempseqpos});
				$seqfeatnames = $seqfeatnames.$featnodelim.$SeqPos_Feats{$tempseqpos};
				$posseqfeats = $posseqfeats."$featnodelim(".$SeqPos_Featfrmtpos{$tempseqpos}.")";
				##print debug "\nInside the occrcyseqpos loop:";
			}
		
		$posseqfeatnos =~ s/^($featnometadelim)//i;
		$seqfeatnames =~ s/^($featnometadelim)//i;
		$posseqfeats =~ s/^($featnometadelim)//i;
		
		#@seqfeatnames = split /; /, $seqfeatnames;
		@seqfeatnames = Splitrow ($seqfeatnames,$featnodelim);
		my $noseqfeatnames = @seqfeatnames;
		
		if (defined $SeqPos_Feats{$occrcyseqpos}) 
			{
				my $novartypes = $SeqFeatVarTypes{$occrcyseqpos};
				#my $varntposits = join ", ", (@{$possfvarposits->{$occrcyseqpos}});
				my $varntposits = join $seqposdelim, (sort {$a <=> $b} @{$possfvarposits->{$occrcyseqpos}});
				#my $varntposits = join ", ", (sort by_number @{$possfvarposits->{$occrcyseqpos}});
				print Occrcyrept "\n$Locus\t$occrcyseqpos\t",$Seqfeatname.$SeqFeat_no{$occrcyseqpos},"\t$SeqPos_Feats{$occrcyseqpos}\t$SeqPos_Feattypes{$occrcyseqpos}\t$varntflag[$possfvarntflag{$occrcyseqpos}]\t$posseqfeatnos\t$noposseqfeats\t$seqfeatnames\t$noseqfeatnames\t$novartypes\t",scalar @{$possfvarposits->{$occrcyseqpos}},"\t",$varntposits,"\t$posseqfeats";
				#print Occrcyrept "\n$HLA_Locus\t$occrcyseqpos\t",$Seqfeatname.$SeqFeat_no{$occrcyseqpos},"\t$SeqPos_Feats{$occrcyseqpos}\t$SeqPos_Feattypes{$occrcyseqpos}\t$varntflag[$possfvarntflag{$occrcyseqpos}]\t$possfoccurs{$occrcyseqpos}\t$noposseqfeats\t$seqfeatnames\t$noseqfeatnames\t$novartypes\t",scalar @{$possfvarposits->{$occrcyseqpos}},"\t",$varntposits,"\t$posseqfeats";
				
			}
		else
			{
				print Occrcyrept "\n$Locus\t$occrcyseqpos\tNot a Sequence feature\tNot a Sequence feature\tNot a Sequence feature\t",$varntflag[$possfvarntflag{$occrcyseqpos}],"\t",$posseqfeatnos,"\t$noposseqfeats\t$seqfeatnames\t$noseqfeatnames\tNot a Sequence feature\tNot a Sequence feature\tNot a Sequence feature\t$posseqfeats";
				#print Occrcyrept "\n$HLA_Locus\t$occrcyseqpos\tNot a Sequence feature\tNot a Sequence feature\tNot a Sequence feature\t",$varntflag[$possfvarntflag{$occrcyseqpos}],"\t",$possfoccurs{$occrcyseqpos},"\t$noposseqfeats\t$seqfeatnames\t$noseqfeatnames\tNot a Sequence feature\tNot a Variant\tNot a Variant";
			}
	}

my $redundseqpos;

foreach $redundseqpos (sort keys %possfredundcys) 
	{
		my @seqfeatnames;
		$possfredundcys{$redundseqpos} =~ s/($featnometadelim)$//i;
		$possfredundcys{$redundseqpos} =~ s/^($featnometadelim)//i;

		my @posseqfeats = Splitrow($possfredundcys{$redundseqpos},$featnodelim);
		#my @posseqfeats = split /; /, $possfredundcys{$redundseqpos};
		my $noposseqfeats = @posseqfeats;
		
		my $seqfeatnames;
		my $posseqfeats;
		my $posseqfeatnos;
		my $tempseqfeatno;
		my $novartypes = $SeqFeatVarTypes{$redundseqpos};
		
		foreach $tempseqfeatno (@posseqfeats) 
			{
				my $tempseqpos = $SeqPosFeats{$tempseqfeatno};
				$posseqfeatnos = $posseqfeatnos.$featnodelim.($Seqfeatname.$SeqFeat_no{$tempseqpos});
				$seqfeatnames = $seqfeatnames.$featnodelim.$SeqPos_Feats{$tempseqpos};
				$posseqfeats = $posseqfeats."$featnodelim(".$SeqPos_Featfrmtpos{$tempseqpos}.")";
			}
		
		$posseqfeatnos =~ s/^($featnometadelim)//i;
		$seqfeatnames =~ s/^($featnometadelim)//i;
		$posseqfeats =~ s/^($featnometadelim)//i;
		
		@seqfeatnames = Splitrow ($seqfeatnames,$featnodelim);
		#@seqfeatnames = split /; /, $seqfeatnames;
		my $noseqfeatnames = @seqfeatnames;
		my $varntposits = join $seqposdelim, (sort {$a <=> $b} @{$possfvarposits->{$redundseqpos}});
		#my $varntposits = join ", ", (@{$possfvarposits->{$redundseqpos}});
		#print Redundrept "\n$HLA_Locus\t$redundseqpos\t",$Seqfeatname.$SeqFeat_no{$redundseqpos},"\t$SeqPos_Feats{$redundseqpos}\t$SeqPos_Feattypes{$redundseqpos}\t$varntflag[$possfvarntflag{$redundseqpos}]\t$possfredundcys{$redundseqpos}\t$noposseqfeats\t$seqfeatnames\t$noseqfeatnames\t$novartypes\t",scalar @{$possfvarposits->{$redundseqpos}},"\t",$varntposits,"\t$posseqfeats";
		print Redundrept "\n$Locus\t$redundseqpos\t",$Seqfeatname.$SeqFeat_no{$redundseqpos},"\t$SeqPos_Feats{$redundseqpos}\t$SeqPos_Feattypes{$redundseqpos}\t$varntflag[$possfvarntflag{$redundseqpos}]\t$posseqfeatnos\t$noposseqfeats\t$seqfeatnames\t$noseqfeatnames\t$novartypes\t",scalar @{$possfvarposits->{$redundseqpos}},"\t",$varntposits,"\t$posseqfeats";
	}


############################## End of Printing the Occurances and Redundancy reports


############################## Printing the Seqfeature and Posseqfeat var reports
open Seqfeatrep, ">$outfiles[6]";
open Seqfeatvarrep, ">$outfiles[7]";
open PosSeqfeatvarrep, ">$outfiles[8]";


#print Seqfeatrep "HLA Locus\tTotal number of Sequence Features defined\tNumber of 'Structural' Sequence Features\tNumber of 'Functional' Sequence Features\tNumber of 'Structural_Functional Combination' Sequence Features\tNumber of 'Structural - Allele' Sequence Features\tNumber of '$Featuretypes[4]' Sequence Features\tNumber of 'Structural - Domain' Sequence Features\tNumber of 'Structural - Secondary Structure Motif' Sequence Feature\tAverage Number of Variant Types for the Sequence Features\tRange of Number of Variant Types for the Sequence Features\n";
#print Seqfeatrep "HLA Locus\tTotal number of Unique Sequence Feature Positions defined\tTotal number of Sequence Features defined\tNumber of 'Structural' Sequence Features\tNumber of 'Functional' Sequence Features\tNumber of 'Structural_Functional Combination' Sequence Features\tNumber of 'Structural - variant location' Sequence Features\tNumber of '$Featuretypes[4]' Sequence Features\tNumber of 'Structural - Domain' Sequence Features\tNumber of 'Structural - Secondary Structure Motif' Sequence Feature\tAverage Number of Variant Types for the Sequence Features\tRange of Number of Variant Types for the Sequence Features\n";
#print Seqfeatrep "HLA Locus\tTotal number of Unique Sequence Feature Positions defined\tTotal number of Sequence Features defined\tNumber of '",$Featuretypes[0],"' Sequence Features\tNumber of '",$Featuretypes[1],"' Sequence Features\tNumber of '",$Featuretypes[2],"' Sequence Features\tNumber of '",$Featuretypes[3],"' Sequence Features\tNumber of '",$Featuretypes[4],"' Sequence Features\tNumber of '",$Featuretypes[5],"' Sequence Features\tNumber of '",$Featuretypes[6],"' Sequence Features\tNumber of '",$Featuretypes[7],"' Sequence Features\tNumber of '",$Featuretypes[8],"' Sequence Feature\tNumber of '",$Featuretypes[9],"' Sequence Feature\tAverage Number of Variant Types for the Sequence Features\tRange of Number of Variant Types for the Sequence Features\n";
#print Seqfeatvarrep "Record No.\tHLA Locus\tSequence Feature No.\tSequence Feature Name\tFeature Location - Amino Acid Positions\tNumber of Variant Types\n";
print Seqfeatvarrep "Record No.\tLocus\tSequence Feature No.\tSequence Feature Name(s)\tFeature Location - Amino Acid Position(s)\tLength of the sequence feature(i.e. Number of residue positions defined in the sequence feature)\tNumber of Variant Types\n";
#print Seqfeatvarrep "\n1\t$Locus\t1\t$HLA_Locus Allele\t\t";
#print PosSeqfeatvarrep "HLA Locus\tSequence Feature No.\tFeature Location - Amino Acid Positions\tSequence Feature Name(s)\tHLA Protein Feature Type(s)\tNumber of Variant Types\n";
#print PosSeqfeatvarrep "Locus\tSequence Feature No.\tFeature Location - Amino Acid Positions\tSequence Feature Name(s)\tHLA Protein Feature Type(s)\tLength of the sequence feature(i.e. Number of residues defined in the sequence feature)\tNumber of Variant Types\n";
print PosSeqfeatvarrep "Locus\tSequence Feature No.\tFeature Location - Amino Acid Position(s)\tSequence Feature Name(s)\tSequence Feature Type(s)\tNumber of Variant Positions in the sequence feature\tThe variant position(s) in the sequence feature\tLength of the sequence feature(i.e. Number of residues defined in the sequence feature)\tNumber of Variant Types";
print PosSeqfeatvarrep "\tNumber of unique sequence features that share some or all of the variant positions and also have equal number of variant types\tUnique Sequence feature(s) that share some or all of the variant positions and also have equal number of variant types\tNumber of sequence feature names that share some or all of the variant positions and also have equal number of variant types\tSequence Feature Name(s) of the sequence features that share some or all of the variant positions and also have equal number of variant types\tThe amino acid position(s) of the unique sequence features that share some or all of the variant positions and also have equal number of variant types";
print PosSeqfeatvarrep "\tNumber of constituting unique sequence features\tContained in Sequence feature(s)\tNumber of constituting sequence feature names\tSequence Feature Name(s) of the constituting sequence features\tThe amino acid position(s) of the constituting unique sequence features\n";

#print PosSeqfeatvarrep "\n$Locus\t1\t\t$HLA_Locus Allele\t$Featuretypes[3]\t\t";
#print Seqfeatvarrep "\t$Seqfeatshdr\n";

$SeqFeature = "";
my $tempsfct = 0;
$tempsfct++;
$tempallelect = $tempallelect - 1 if (!($HLA_Locus =~ /^ *($Ref_HLA_Locus) *$/i));

print Seqfeatvarrep "\n$tempsfct\t$Locus\t$Seqfeatname",$SeqFeat_no{$Alleleseqfeat},"\t$Alleleseqfeat\t\t\t$tempallelect";
print PosSeqfeatvarrep "\n$Locus\t$Seqfeatname",$SeqFeat_no{$Alleleseqfeat},"\t\t$Alleleseqfeat\t$FeatureTypes[6]\t\t\t\t$tempallelect";

foreach $SeqFeature (@SeqFeat)
	{
		my $temppos = $SeqFeats_Pos{$SeqFeature};

		#if (defined ($SeqFeatVarTypes{$SeqFeature}))
		if (defined ($SeqFeatVarTypes{$temppos}))
			{
				$tempsfct++;
				#print Seqfeatvarrep "\n$tempsfct\t$HLA_Locus\t$SeqFeat_no{$SeqFeature}\t$HLA_Locus $SeqFeature\t$SeqPos_Featfrmtpos{$SeqFeature}\t$SeqFeatVarTypes{$SeqFeature}";
				#print Seqfeatvarrep "\n$tempsfct\t$HLA_Locus\t$SeqFeat_no{$SeqFeature}\t$SeqFeature\t$SeqPos_Featfrmtpos{$SeqFeature}\t$SeqFeatlen{$SeqFeature}\t$SeqFeatVarTypes{$SeqFeature}";
				print Seqfeatvarrep "\n$tempsfct\t$Locus\t$Seqfeatname",$SeqFeat_no{$SeqFeature},"\t$SeqFeature\t$SeqPos_Featfrmtpos{$SeqFeature}\t$SeqFeatlen{$temppos}\t$SeqFeatVarTypes{$temppos}";
				#print Seqfeatvarrep "\n$tempsfct\t$HLA_Locus\t$Seqfeatname$SeqFeat_no{$SeqFeature}\t$SeqFeature\t$SeqPos_Featfrmtpos{$SeqFeature}\t$SeqFeatlen{$SeqFeature}\t$SeqFeatVarTypes{$SeqFeature}";
				########### Print with DRB1 in the sequence feature name
				#print Seqfeatvarrep "\n$tempsfct\t$HLA_Locus\t$SeqFeat_no{$SeqFeature}\t$HLA_Locus $SeqFeature\t$SeqPos_Featfrmtpos{$SeqFeature}\t$SeqFeatlen{$SeqFeature}\t$SeqFeatVarTypes{$SeqFeature}";
			}
#		if ($SeqFeature =~ /^($Alleleseqfeat)$/i) 
#			{
#				$tempsfct++;
#				#print Seqfeatvarrep "\n$tempsfct\t$HLA_Locus\t$SeqFeat_no{$SeqFeature}\t$SeqFeature\t\t\t$tempallelect";
#				#print Seqfeatvarrep "\n$tempsfct\t$HLA_Locus\t$Seqfeatname$SeqFeat_no{$SeqFeature}\t$SeqFeature\t\t\t$tempallelect";
#				print Seqfeatvarrep "\n$tempsfct\t$Locus\t",$SeqFeat_no{$SeqFeature},"\t$SeqFeature\t\t\t$tempallelect";
#				#print PosSeqfeatvarrep "\n$HLA_Locus\t$SeqFeat_no{$SeqFeature}\t\t$SeqFeature\t$FeatureTypes[3]\t\t$tempallelect";
#				#print PosSeqfeatvarrep "\n$HLA_Locus\t$Seqfeatname$SeqFeat_no{$SeqFeature}\t\t$SeqFeature\t$FeatureTypes[6]\t\t$tempallelect";
#				print PosSeqfeatvarrep "\n$Locus\t$Seqfeatname",$SeqFeat_no{$SeqFeature},"\t\t$SeqFeature\t$FeatureTypes[6]\t\t$tempallelect";
#				########### Print with DRB1 in the sequence feature name
#				#print Seqfeatvarrep "\n$tempsfct\t$HLA_Locus\t$SeqFeat_no{$SeqFeature}\t$HLA_Locus $SeqFeature\t\t\t$tempallelect";
#				#print PosSeqfeatvarrep "\n$HLA_Locus\t$SeqFeat_no{$SeqFeature}\t\t$HLA_Locus $SeqFeature\t$FeatureTypes[3]\t\t$tempallelect";
#			}
	}

$SeqFeature = "";
$tempsfct = $startct;
%possftypevals;

$possftypevals{"Min"} = 1;

@typevals = ("Averageinc", "Averageex", "Min", "Max");


#foreach $SeqposFeature (@SeqPosFeats)
foreach $seqposfeatkey (sort by_number keys %SeqPosFeats)
	{
		$SeqposFeature = $SeqPosFeats{$seqposfeatkey};
		#$SeqPos_Feats{$SeqposFeature} =~ /^([\w-& ]+);?/i;
		#my $temppos = $SeqposFeature;
		my @sftypes;
		my $currsftype;
		my $temppossftypes = $SeqPos_Feattypes{$SeqposFeature};
		
		@sftypes = Splitrow ($temppossftypes, $feattypedelim);
		$temppossftypes = $feattypedelim.($SeqPos_Feattypes{$SeqposFeature}).$feattypedelim;
		
		foreach $currsftype (@sftypes) 
			{
				if ($currsftype =~ /^([\w ]+) ?- ?([\w ]+)$/i) 
					{
						$uniqSeqFeatTypes{lc(Trim($1))}++;		
					}
				$uniqSeqFeatTypes{lc($currsftype)}++;
			}
		
		if ($temppossftypes =~ /$feattypedelim($Featuretypes[2] ?- ?[\w ]+)$feattypedelim/i) 
			{
				if ($temppossftypes =~ /$feattypedelim($Featuretypes[1])$feattypedelim/i)
					{
						$uniqSeqFeatTypes{lc($Featuretypes[1])}--;		
					}
				if ($temppossftypes =~ /$feattypedelim($Featuretypes[0] ?- ?[\w ]+)$feattypedelim/i)
					{
						$uniqSeqFeatTypes{lc(Trim($1))}--;
						$uniqSeqFeatTypes{lc($Featuretypes[0])}--;
					}
				if ($temppossftypes =~ /$feattypedelim($Featuretypes[3])$feattypedelim/i)
					{
						$uniqSeqFeatTypes{lc($Featuretypes[3])}--;		
					}
			}
		elsif (($temppossftypes =~ /$feattypedelim($Featuretypes[1])$feattypedelim/i) && ($temppossftypes =~ /$feattypedelim($Featuretypes[0] ?- ?[\w ]+)$feattypedelim/i))
			{
#				$uniqSeqFeatTypes{lc(Trim($1))}--;
#				$uniqSeqFeatTypes{lc($Featuretypes[0])}--;
				$uniqSeqFeatTypes{lc($Featuretypes[1])}--;
				$uniqSeqFeatTypes{lc($Featuretypes[3])}--;
				print debug "\nLine 1855: For Seqposfeat $SeqposFeature\tuniqSeqFeatTypes{($Featuretypes[0])} = ",$uniqSeqFeatTypes{lc($Featuretypes[0])},"\tuniqSeqFeatTypes{($Featuretypes[1])} = ",$uniqSeqFeatTypes{lc($Featuretypes[1])},"\tuniqSeqFeatTypes{($Featuretypes[3])} = ",$uniqSeqFeatTypes{lc($Featuretypes[3])},"\tuniqSeqFeatTypes{($1)} = ",$uniqSeqFeatTypes{lc($1)};
			}
		elsif (($temppossftypes =~ /$feattypedelim($Featuretypes[3])$feattypedelim/i) && (($temppossftypes =~ /$feattypedelim($Featuretypes[0] ?- ?[\w ]+)$feattypedelim/i) || ($temppossftypes =~ /$feattypedelim($Featuretypes[1])$feattypedelim/i)))
			{
				$uniqSeqFeatTypes{lc($Featuretypes[3])}--;
				print debug "\nLine 1860: For Seqposfeat $SeqposFeature\tuniqSeqFeatTypes{($Featuretypes[0])} = ",$uniqSeqFeatTypes{lc($Featuretypes[0])},"\tuniqSeqFeatTypes{($Featuretypes[1])} = ",$uniqSeqFeatTypes{lc($Featuretypes[1])},"\tuniqSeqFeatTypes{($Featuretypes[3])} = ",$uniqSeqFeatTypes{lc($Featuretypes[3])},"\tuniqSeqFeatTypes{($1)} = ",$uniqSeqFeatTypes{lc($1)};
			}

		

		$SeqPos_Feats{$SeqposFeature} =~ /^([\w-&_ ]+);?/i;
		$SeqFeature = $1;
		print debug "\nLine 1882: Inside the final posseqfeatvarrep print, The seqfeature for seqposfeature $SeqposFeature is = \t$SeqFeature";
		#if (defined ($SeqFeatVarTypes{$SeqFeature}))
		if (defined ($SeqFeatVarTypes{$SeqposFeature}))
			{
				$tempsfct++;
				
				#################### Part from Redundrept
				my $redundseqpos = $SeqposFeature;
				my @seqfeatnames;
				
				my @posseqfeats = Splitrow($possfredundcys{$redundseqpos},$featnodelim);
				#my @posseqfeats = split /; /, $possfredundcys{$redundseqpos};
				my $noposseqfeats = @posseqfeats;
				
				my $seqfeatnames;
				my $posseqfeats;
				my $posseqfeatnos;
				my $tempseqfeatno;
				my $novartypes = $SeqFeatVarTypes{$redundseqpos};
				
				foreach $tempseqfeatno (@posseqfeats) 
					{
						my $tempseqpos = $SeqPosFeats{$tempseqfeatno};
						$posseqfeatnos = $posseqfeatnos.$featnodelim.($Seqfeatname.$SeqFeat_no{$tempseqpos});
						$seqfeatnames = $seqfeatnames.$featnamedelim.$SeqPos_Feats{$tempseqpos};
						$posseqfeats = $posseqfeats."$featnodelim(".$SeqPos_Featfrmtpos{$tempseqpos}.")";
					}
				
				$posseqfeatnos =~ s/^($featnometadelim)//i;
				$seqfeatnames =~ s/^($featnometadelim)//i;
				$posseqfeats =~ s/^($featnometadelim)//i;
				
				@seqfeatnames = Splitrow ($seqfeatnames,$featnamedelim);
				#@seqfeatnames = split /; /, $seqfeatnames;
				my $noseqfeatnames = @seqfeatnames;
				my $varntposits = join $seqposdelim, (sort {$a <=> $b} @{$possfvarposits->{$redundseqpos}});
				
				#################### End of Part from Redundrept

				#################### Part from Occrcyrept		
				my $occrcyseqpos = $SeqposFeature;
				my @occrseqfeatnames;
				
				my @occrposseqfeats = Splitrow($possfoccurs{$occrcyseqpos},$featnodelim);
				#my @posseqfeats = split /; /, $possfoccurs{$occrcyseqpos};
				my $occrnoposseqfeats = @occrposseqfeats;
				
				my $occrseqfeatnames;
				my $occrposseqfeats;
				my $occrposseqfeatnos;
				my $occrposseqpos; 
				my $occrtempseqfeatno;

				
				foreach $occrtempseqfeatno (@occrposseqfeats) 
					{
						my $occrtempseqpos = $SeqPosFeats{$occrtempseqfeatno};
						$occrposseqfeatnos = $occrposseqfeatnos.$featnodelim.($Seqfeatname.$SeqFeat_no{$occrtempseqpos});
						$occrseqfeatnames = $occrseqfeatnames.$featnamedelim.$SeqPos_Feats{$occrtempseqpos};
						$occrposseqfeats = $occrposseqfeats."$featnodelim(".$SeqPos_Featfrmtpos{$occrtempseqpos}.")";
						##print debug "\nInside the occrcyseqpos loop:";
					}
				
				$occrposseqfeatnos =~ s/^($featnometadelim)//i;
				$occrseqfeatnames =~ s/^($featnometadelim)//i;
				$occrposseqfeats =~ s/^($featnometadelim)//i;
				
				#@seqfeatnames = split /; /, $seqfeatnames;
				@occrseqfeatnames = Splitrow ($occrseqfeatnames,$featnamedelim);
				my $occrnoseqfeatnames = @occrseqfeatnames;
						
				#################### End of Part from Occrcyrept
				
				$possftypevals{"Averageinc"} = $possftypevals{"Averageinc"} + $SeqFeatVarTypes{$SeqposFeature};
				$possftypevals{"Averageex"} = $possftypevals{"Averageex"} + $SeqFeatVarTypes{$SeqposFeature};
				$possftypevals{"Min"} = $SeqFeatVarTypes{$SeqposFeature} if ($possftypevals{"Min"} > $SeqFeatVarTypes{$SeqposFeature});
				$possftypevals{"Max"} = $SeqFeatVarTypes{$SeqposFeature} if ($possftypevals{"Max"} < $SeqFeatVarTypes{$SeqposFeature});			
				#print PosSeqfeatvarrep "\n$HLA_Locus\t$SeqFeat_no{$SeqFeature}\t$SeqPos_Featfrmtpos{$SeqFeature}\t$SeqPos_Feats{$SeqposFeature}\t$SeqPos_Feattypes{$SeqposFeature}\t$SeqFeatVarTypes{$SeqFeature}";
				#print PosSeqfeatvarrep "\n$HLA_Locus\t$SeqFeat_no{$SeqFeature}\t$SeqPos_Featfrmtpos{$SeqFeature}\t$SeqPos_Feats{$SeqposFeature}\t$SeqPos_Feattypes{$SeqposFeature}\t$SeqFeatlen{$SeqFeature}\t$SeqFeatVarTypes{$SeqFeature}";
				#print PosSeqfeatvarrep "\n$HLA_Locus\t$Seqfeatname$SeqFeat_no{$SeqFeature}\t$SeqPos_Featfrmtpos{$SeqFeature}\t$SeqPos_Feats{$SeqposFeature}\t$SeqPos_Feattypes{$SeqposFeature}\t$SeqFeatlen{$SeqFeature}\t$SeqFeatVarTypes{$SeqFeature}";
				#print PosSeqfeatvarrep "\n$Locus\t$Seqfeatname$SeqFeat_no{$SeqFeature}\t$SeqPos_Featfrmtpos{$SeqFeature}\t$SeqPos_Feats{$SeqposFeature}\t$SeqPos_Feattypes{$SeqposFeature}\t$SeqFeatlen{$SeqposFeature}\t$SeqFeatVarTypes{$SeqposFeature}";
				print PosSeqfeatvarrep "\n$Locus\t$Seqfeatname$SeqFeat_no{$SeqFeature}\t$SeqPos_Featfrmtpos{$SeqFeature}\t$SeqPos_Feats{$SeqposFeature}\t$SeqPos_Feattypes{$SeqposFeature}\t",scalar @{$possfvarposits->{$redundseqpos}},"\t",$varntposits,"\t$SeqFeatlen{$SeqposFeature}\t$SeqFeatVarTypes{$SeqposFeature}\t$noposseqfeats\t$posseqfeatnos\t$noseqfeatnames\t$seqfeatnames\t$posseqfeats\t$occrnoposseqfeats\t$occrposseqfeatnos\t$occrnoseqfeatnames\t$occrseqfeatnames\t$occrposseqfeats";
				
				
				#truncated Redundrept "\t$posseqfeatnos\t$noposseqfeats\t$seqfeatnames\t$noseqfeatnames\t$novartypes\t",scalar @{$possfvarposits->{$redundseqpos}},"\t",$varntposits,"\t$posseqfeats";
				#truncated Occrcyrept "$occrposseqfeatnos\t$occrnoposseqfeats\t$occrseqfeatnames\t$occrnoseqfeatnames\t$posseqfeats";
				
				#print Redundrept "\n$Locus\t$redundseqpos\t",$Seqfeatname.$SeqFeat_no{$redundseqpos},"\t$SeqPos_Feats{$redundseqpos}\t$SeqPos_Feattypes{$redundseqpos}\t$varntflag[$possfvarntflag{$redundseqpos}]\t$posseqfeatnos\t$noposseqfeats\t$seqfeatnames\t$noseqfeatnames\t$novartypes\t",scalar @{$possfvarposits->{$redundseqpos}},"\t",$varntposits,"\t$posseqfeats";
				#print Occrcyrept "\n$Locus\t$occrcyseqpos\t",$Seqfeatname.$SeqFeat_no{$occrcyseqpos},"\t$SeqPos_Feats{$occrcyseqpos}\t$SeqPos_Feattypes{$occrcyseqpos}\t$varntflag[$possfvarntflag{$occrcyseqpos}]\t$posseqfeatnos\t$noposseqfeats\t$seqfeatnames\t$noseqfeatnames\t$novartypes\t",scalar @{$possfvarposits->{$occrcyseqpos}},"\t",$varntposits,"\t$posseqfeats";
			}

		#if ($SeqFeature =~ /^Allele$/i) 
		#	{
		#		$tempsfct++;
		#		print PosSeqfeatvarrep "\n$HLA_Locus\t$SeqFeat_no{$SeqFeature}\t\t$HLA_Locus $SeqFeature\t$FeatureTypes[3]\t\t$tempallelect";
		#	}	
	}


my $feattype;
my $feattypeline;
my $uniqfeattypeline;
my ($feattypehdr, $uniqfeattypehdr);

$SeqFeatTypes{lc($Featuretypes[6])} = $SeqFeatTypes{lc($Featuretypes[6])} + $startct;
$SeqFeatTypes{lc($Featuretypes[0])} = $SeqFeatTypes{lc($Featuretypes[0])} + $startct;

$uniqSeqFeatTypes{lc($Featuretypes[6])} = $uniqSeqFeatTypes{lc($Featuretypes[6])} + $startct; 
$uniqSeqFeatTypes{lc($Featuretypes[0])} = $uniqSeqFeatTypes{lc($Featuretypes[0])} + $startct;

#print Seqfeatrep "HLA Locus\tTotal number of Unique Sequence Feature Positions defined\tTotal number of Sequence Features defined\tNumber of '",$Featuretypes[0],"' Sequence Features\tNumber of '",$Featuretypes[1],"' Sequence Features\tNumber of '",$Featuretypes[2],"' Sequence Features\tNumber of '",$Featuretypes[3],"' Sequence Features\tNumber of '",$Featuretypes[4],"' Sequence Features\tNumber of '",$Featuretypes[5],"' Sequence Features\tNumber of '",$Featuretypes[6],"' Sequence Features\tNumber of '",$Featuretypes[7],"' Sequence Features\tNumber of '",$Featuretypes[8],"' Sequence Feature\tNumber of '",$Featuretypes[9],"' Sequence Feature\tAverage Number of Variant Types for the Sequence Features\tRange of Number of Variant Types for the Sequence Features\n";
print Seqfeatrep "HLA Locus\tTotal number of Unique Sequence Feature Positions defined\tTotal number of Sequence Features defined";

foreach $feattype (@Featuretypes) 
	{
		#$feattype = lc($feattype);
		if ( (!(defined $SeqFeatTypes{lc($feattype)})) || $SeqFeatTypes{lc($feattype)} =~ /^( *)$/i) 
			{
				$SeqFeatTypes{lc($feattype)} = 0;
				$uniqSeqFeatTypes{lc($feattype)} = 0;
			}	
#		$feattypeline = $feattypeline."\t".$SeqFeatTypes{lc($feattype)};	
#		print Seqfeatrep "\tNumber of '",$feattype,"' Sequence Features";
#		$feattypeline = $feattypeline."\t".$SeqFeatTypes{lc($feattype)}."\t".$uniqSeqFeatTypes{lc($feattype)};	
		$feattypeline = $feattypeline."\t".$SeqFeatTypes{lc($feattype)};	
		$uniqfeattypeline = $uniqfeattypeline."\t".$uniqSeqFeatTypes{lc($feattype)};	
		$feattypehdr = $feattypehdr."\tNumber of '".$feattype."' Sequence Features";
		$uniqfeattypehdr = $uniqfeattypehdr."\tNumber of Unique '".$feattype."' Sequence Features";
#		print Seqfeatrep "\tNumber of '",$feattype,"' Sequence Features\tNumber of Unique '",$feattype,"' Sequence Features";
	
		#if (defined ($SeqFeatTypes{$feattype})) 
		#	{
		#		$feattypeline = $feattypeline."\t".$SeqFeatTypes{$feattype};	
		#	}
		#else
		#	{
		#		$feattypeline = $feattypeline."\t".$SeqFeatTypesall{$feattype};
		#	}
	}
print Seqfeatrep $uniqfeattypehdr,$feattypehdr;

#$possftypevals{"Averageinc"} =  ($possftypevals{"Averageinc"} + $tempallelect)/@SeqPosFeats;
$possftypevals{"Averageinc"} =  ($possftypevals{"Averageinc"} + $tempallelect)/(keys %SeqPosFeats);

#$possftypevals{"Averageex"} = $possftypevals{"Averageex"}/@SeqPosFeats;
$possftypevals{"Averageex"} = $possftypevals{"Averageex"}/(keys %SeqPosFeats);


my ($temprangeinc, $temprangeex);
$temprangeex = $possftypevals{"Min"}."-".$possftypevals{"Max"};
$temprangeinc = $possftypevals{"Min"}."-".$tempallelect;
if ($possftypevals{"Max"} > $tempallelect) 
	{
		$temprangeinc = $possftypevals{"Min"}."-".$possftypevals{"Max"};
	}

#print Seqfeatrep "\tAverage Number of Variant Types for the Sequence Features\tRange of Number of Variant Types for the Sequence Features\n";
print Seqfeatrep "\tAverage Number of Variant Types for the Sequence Features (Including the number of Alleles)\tAverage Number of Variant Types for the Sequence Features (Excluding the number of Alleles)\tRange of the Number of Variant Types across the Sequence Features (Including the number of Alleles)\tRange of the Number of Variant Types across the Sequence Features (Excluding the number of Alleles)\n";
#print Seqfeatrep "\n$HLA_Locus\t$SeqPosfeatct\t$Seqfeatct$feattypeline\t\t\t\t";
#print Seqfeatrep "\n$Locus\t$SeqPosfeatct\t$Seqfeatct$feattypeline";
print Seqfeatrep "\n$Locus\t$SeqPosfeatct\t",$Seqfeatct,$uniqfeattypeline,$feattypeline;
printf Seqfeatrep "\t%-.0f\t%-.0f\t%s\t%s",$possftypevals{"Averageinc"},$possftypevals{"Averageex"}, $temprangeinc, $temprangeex;
#print Seqfeatrep "\n$HLA_Locus\t$SeqPosfeatct\t$Seqfeatct$feattypeline\t",$possftypevals{"Averageinc"},"\t",$possftypevals{"Averageex"},"\t",$temprangeinc,"\t",$temprangeex;


############################## End of Printing the Seqfeature and Posseqfeat var reports

	close IndivSeqfeat;
	close AlleleVarntsfile;
	close Unk_NonVarntsfile;
	close SqftVarntsfile;
	close AlleleSeqFeats;
	close DefSeqfeats;
	close DefAllelevarnts;

	close PosSeqfeatvarrep;
	close Seqfeatvarrep;
	close Seqfeatrep;
	

#	close debug;
