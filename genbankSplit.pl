use Bio::SeqIO.
my $in = Bio::SeqIO->new(-format => 'genbank', -file => $genfile);
while(my $seq = $in->next_seq ){ 
 my $acc =$seq->accession_number;
 my $out = Bio::SeqIO->new(-format => 'genbank',-file=>">$acc.gb");
 $out->write_seq($seq);
}