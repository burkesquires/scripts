use strict;
use Bio::SearchIO;

# Burke Squires
# BioHealthBase BRC
# May 17, 2009

my $outputfile = 'flu-h1n1-ns-parsedblast.out';

open OUTFILE, ">$outputfile" or die "unable to open $outputfile $!";

my $in = new Bio::SearchIO(-format => 'blastxml',
                           -file   => 'flu-h1n1-ns-blast.xml');

print OUTFILE "Strain\tLength\tIdentical Residues\tStrain Year\tAge Difference\tNucleotide Differences\tPercent Identity\n";

while( my $result = $in->next_result ) {

	while( my $hit = $result->next_hit ) {

		while( my $hsp = $hit->next_hsp ) {

  			my $strain = $hit->description;

  			$strain =~ m/Influenza A virus[a-zA-Z0-9\(\/\-\.\_\'\s]+\(H[0-9]+N[0-9]+\)/;

  			$strain = $&;

            $strain =~ s/Influenza A virus \(//;

            if ($strain) {

  			my $year = $strain;

  			$year =~ /[0-9\s]+\(/;

  			$year = $&;

  			$year =~ /[0-9]+/;

  			$year = $&;

  			#Test to see that year is numeric?

  			if ( m/[^0-9.]/ ) {

  				#do nothing

  				} else {

		  			if ($year < 10) {

						$year = $year + 2000;

					} else {

						if ($year < 100) {

							$year = $year + 1900;
						}
					}

		  			print OUTFILE 	$strain, "\t",
  									$hsp->hsp_length, "\t",
									$hsp->num_identical, "\t",
									$year, "\t",
									(2009 - $year), "\t",
                                    ($hsp->hsp_length - $hsp->num_identical), "\t",
                                    $hsp->percent_identity, "\n";
				}
			}
		}
	}
}
