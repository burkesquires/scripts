use strict;
use Bio::SearchIO;

 
# Burke Squires
# BioHealthBase BRC
# May 17, 2009

#hash definitions

my %serotypes = ();
my %hosts = ();
my %years = ();
my %locations = ();
my %year_host = ();
my %year_location = ();
my %host_location = ();
my $hashType = '';
my $count = 0;
my $decade = 0;


my $outputfile = 'a-cali-04-2009-h1n1-ha-megablast-parsedblast.out';
open OUTFILE, ">$outputfile" or die "unable to open $outputfile $!";

my $metaDataFile = 'a-cali-04-2009-h1n1-ha-megablast-parsedblast.summary';
open METAFILE, ">$metaDataFile" or die "unable to open $outputfile $!";


my $in = new Bio::SearchIO(-format => 'blastxml', -file   => 'a-cali-04-2009-h1n1-ha-megablast.xml');

print OUTFILE "Strain\tAccession\tLength\tIdentical Residues\tStrain Year\tAge Difference\tNucleotide Differences\tPercent Identity\n";

print METAFILE "Metadata\n";

while( my $result = $in->next_result ) {

	while( my $hit = $result->next_hit ) {

		while( my $hsp = $hit->next_hsp ) {

  			my $strain = $hit->description;

  			$strain =~ m/Influenza A virus[a-zA-Z0-9\(\/\-\.\_\'\s]+\(H[0-9]+N[0-9]+\)/;

  			$strain = $&;

            $strain =~ s/Influenza A virus \(//;

            if ($strain) {

				my $year = parseStrainName($strain);

				print OUTFILE 	$strain, "\t",
			  					$hit->accession, "\t",
	  							$hsp->hsp_length, "\t",
								$hsp->num_identical, "\t",
								$year, "\t",
								(2009 - $year), "\t",
	                            ($hsp->hsp_length - $hsp->num_identical), "\t",
	                            $hsp->percent_identity, "\n";
			}
		}
	}
}

print METAFILE "Year and Host:\n";
while( my ($k, $v) = each %year_host ) {
        print METAFILE "\t$k:\t$v\n";
    }
    

print METAFILE "\nYear and Location:\n";
while( my ($k, $v) = each %year_location ) {
        print METAFILE "\t$k:\t$v\n";
    }
    

print METAFILE "\nHost and Locations:\n";
while( my ($k, $v) = each %host_location ) {
        print METAFILE "\t$k:\t$v\n";
    }
    

print METAFILE "\nHosts:\n";
while( my ($k, $v) = each %hosts ) {
        print METAFILE "\t$k:\t$v\n";
    }
    
print METAFILE "\nYears:\n";
while( my ($k, $v) = each %years ) {
        print METAFILE "\t$k:\t$v\n";
    }
    
    
print METAFILE "\nLocations:\n";
while( my ($k, $v) = each %locations ) {
        print METAFILE "\t$k:\t$v\n";
    }
    
    
    
    
    

sub parseStrainName {

    my $strain = shift;

	my @characteristic = split /\//, $strain;
    
    my $year = '';
    my $type = '';
   	my $host = '';
    my $location = '';
	my $serotype = '';
    
    #count characteristics to determine if host is human or not
    
    my $count = @characteristic;
    
    if ($count == 4) {
    
    	#serotype, host, location, year
    	
    	updateHash(extractSerotype($characteristic[3]), "human", $characteristic[1], extractYear($characteristic[3]));
    
    } elsif ($count == 5) {
    
       	$type = $characteristic[0];
    
    	updateHash(extractSerotype($characteristic[4]), $characteristic[1], $characteristic[2], extractYear($characteristic[4]));
 
    } else {
    
    	print "Error with strain: $strain\n";
    }
    
    return $year;
    
}

sub extractYear {

	my $year = $_[0];

	if ( m/[^0-9.]/ ) {

  				#do nothing

  	} else {
				
		$year =~ /[0-9\s]+\(/;

		$year = $&;

		$year =~ /[0-9]+/;

		$year = $&;
	  	
	  	if ($year < 10) {
		
			$year = $year + 2000;

		} else {

			if ($year < 100) {

				$year = $year + 1900;
		
			}
		}

	return $year;
	}
}

sub extractSerotype {

	my @strain = split /\(/, $_[0];
	
	my $serotype = '';

	$serotype = $strain[1];
	
	$serotype =~ /[HN0-9]+/;
	
	return $&;
	
}

sub updateHash {

	#Order: serotype, host, location, year
	
	my $count = $serotypes{ $_[0] };
	$count = $count + 1;
	$serotypes{ $_[0] } = $count;

	$count = $hosts{ $_[1] };
	$count = $count + 1;
	$hosts{ $_[1] } = $count;

	$count = $locations{ $_[2] };
	$count = $count + 1;
	$locations{ $_[2] } = $count;

	$count = $years{ $_[3] };
	$count = $count + 1;
	$years{ $_[3] } = $count;
	
	#Combination
	
	$decade = substr($_[3], 0, 3) . "0";
	
	$count = $year_host{ $decade . "-" . $_[1] };
	$count = $count + 1;
	$year_host{  $decade . "-" . $_[1] } = $count;
	
	$count = $year_location{ $decade . "-" . $_[2] };
	$count = $count + 1;
	$year_location{ $decade . "-" . $_[2] } = $count;
	
	$count = $host_location{ $_[1] . "-" . $_[2] };
	$count = $count + 1;
	$host_location{ $_[1] . "-" . $_[2] } = $count;
	
}


