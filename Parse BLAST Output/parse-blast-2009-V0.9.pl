#!/usr/bin/perl
use strict;


use Bio::SearchIO;
use GD::Graph;
use GD::Graph::points;
 
# Burke Squires
# BioHealthBase BRC
# May 17, 2009

#hash definitions

my %serotypes = ();
my %hosts = ();
my %years = ();
my %locations = ();
my %year_host = ();
my %year_location = ();
my %host_location = ();
my %host_location_year = ();
my %regions = ();
my %regioncodes = ();
my %countries = ();
my $hashType = '';
my $count = 0;
my $decade = 0;
my @dataX = ();
my @dataY = ();
my @dataStrain = ();
my @lineageX = ();
my @lineageY = ();
my @lineageStrain = ();
my $maxX = 0;
my $maxY = 0;
my $maxAge = 0;
my $maxDiff = 0;

# '~/Documents/IRD/Analysis/Evolutionary trajectory analysis/02 - BLAST analysis/BLAST output 200905/flu-h1n1-np-blast.xml'
my $outputfile = '/Users/burkesquires/Desktop/BLAST output 200905/flu-h1n1-na-blast.out';
open OUTFILE, ">$outputfile" or die "unable to open $outputfile $!";

my $metaDataFile = '/Users/burkesquires/Desktop/BLAST output 200905/flu-h1n1-na-blast.summary';
open METAFILE, ">$metaDataFile" or die "unable to open $outputfile $!";

readCountryFile();
readRegionFile();

my $in = new Bio::SearchIO(-format => 'blastxml', -file   => '/Users/burkesquires/Desktop/BLAST output 200905/flu-h1n1-np-blast.xml');

print OUTFILE "Strain\tAccession\tLength\tIdentical Residues\tStrain Year\tAge Difference\tNucleotide Differences\tPercent Identity\tCountry\nE-Score\n";

print METAFILE "Metadata\n";

while( my $result = $in->next_result ) {

	while( my $hit = $result->next_hit ) {

		while( my $hsp = $hit->next_hsp ) {

  			my $strain = $hit->description;

  			$strain =~ m/Influenza A virus[a-zA-Z0-9\(\/\-\.\_\'\s]+\(H[0-9]+N[0-9]+\)/;

  			$strain = $&;

            $strain =~ s/Influenza A virus \(//;

            if ($strain) {

				# Type (A, B, C), host (including human), location, isolate, year, serotype
				
				my @strainData = parseStrainName($strain);

				if (@strainData > 3) {
				
					my $year = $strainData[4];
					
					updateHash($strainData[1], $strainData[2], $strainData[1], $strainData[4]);

					my $deltaDate = 2009 - $year;
					my $deltaNucleotide = $hsp->hsp_length - $hsp->num_identical;

					if ( $deltaDate > $maxAge ) { 
						
						$maxAge = $deltaDate; 
					 	
					 	$maxDiff = $deltaNucleotide; 
					 }

	 				my $country = convertLocationToCountry( $_[2] ),"\n";
	 				
	 				recordData($strain, $deltaDate, $deltaNucleotide);

					print OUTFILE 	$strain, "\t",
				  					$hit->accession, "\t",
		  							$hsp->hsp_length, "\t",
									$hsp->num_identical, "\t",
									$year, "\t",
									$deltaDate, "\t",
		                            $deltaNucleotide, "\t",
		                            $hsp->percent_identity, "\t",
		                            convertLocationToCountry( $strainData[2] ),"\n";
			
				}
			}
		}
	}
}


printMetaDataReport();

selectLineageStrains();

graph();


################################################################################


sub selectLineageStrains {

my $maxX = $maxAge;
my $maxY = $maxDiff;

	#step one, calulate the slope of the initial line		

	# y = mx + b; b = 0
	
	my $slope = $maxY / $maxX;
	
	foreach (@dataX) {

		my $tempY = $dataY[$_];
		
		if (($dataY[$_] > (( $_ * $slope) * -1.25)) && ($dataY[$_] < (( $_ * $slope) * 1.25))) {
		
			recordLineageData( $dataStrain[$_], $_, $dataY[$_]);
		
		}
	}
}

sub recordLineageData {

	# Input: Strain, delta Year (X), delta Nucleotide (Y)
	
	push(@lineageStrain, $_[0]);
	push(@lineageX, $_[1]);
	push(@lineageY, $_[2]);

}


sub recordData {

	# Input: Strain, delta Year (X), delta Nucleotide (Y)
	
	push(@dataStrain, $_[0]);
	push(@dataX, $_[1]);
	push(@dataY, $_[2]);

	if ( $_[1] > $maxX) { $maxX = $_[1]; }
	if ( $_[2] > $maxY) { $maxY = $_[2]; }

}

sub graph {

 my @data = ( [@lineageX], [@lineageY], [@lineageX]);
  
  my $graph = GD::Graph::points->new(800, 640);
  
  $graph->set( 
      x_label           => 'Year Difference',
      y_label           => 'Nucleotide Difference',
      title             => 'North American H1N1 Age versus Nucleotide Difference',
      y_max_value       => $maxY,
      y_tick_number     => $maxY,
      y_label_skip      => 2 
  ) or die $graph->error;
  
  my $gd = $graph->plot(\@data) or die $graph->error;
  
  open(IMG, '>file.gif') or die $!;
  binmode IMG;
  print IMG $gd->gif;
}


sub printMetaDataReport {

	print METAFILE "\nHost / Location / Year:\n";
	while( my ($k, $v) = each %host_location_year ) {
	        print METAFILE "\t$k:\t$v\n";
	    }
	    

	print METAFILE "\nYear and Host:\n";
	while( my ($k, $v) = each %year_host ) {
	        print METAFILE "\t$k:\t$v\n";
	    }
	    

	print METAFILE "\nYear and Location:\n";
	while( my ($k, $v) = each %year_location ) {
	        print METAFILE "\t$k:\t$v\n";
	    }
	    

	print METAFILE "\nHost and Locations:\n";
	while( my ($k, $v) = each %host_location ) {
	        print METAFILE "\t$k:\t$v\n";
	    }
	    

	print METAFILE "\nHosts:\n";
	while( my ($k, $v) = each %hosts ) {
	        print METAFILE "\t$k:\t$v\n";
	    }
	    
	print METAFILE "\nYears:\n";
	while( my ($k, $v) = each %years ) {
	        print METAFILE "\t$k:\t$v\n";
	    }
	    
	    
	print METAFILE "\nLocations:\n";
	while( my ($k, $v) = each %locations ) {
	        print METAFILE "\t$k:\t$v\n";
	    }

}


sub parseStrainName {

    my $strain = shift;

	my @strainData = split /\//, $strain;
    my @characteristic = ();
       
    #count characteristics to determine if host is human or not
    
    my $count = @strainData;
    
    # If (count == 1)
    #"X-31"
    
    if ($count == 3) {
    
    	# Usually human, without isolation number
    
    	if ($strainData[0] == 'A') {
    
        	push(@characteristic, $strainData[0]);
    	  	push(@characteristic, "human");
       		push(@characteristic, $strainData[1]);
       		push(@characteristic, "N/A");
       		push(@characteristic, extractYear($strainData[2]));
       		push(@characteristic, extractSerotype($strainData[2]));

		} else {

			#Mongolia/231/85 (H1N1)
			
        	push(@characteristic, "N/A");
    	  	push(@characteristic, "human");
       		push(@characteristic, $strainData[0]);
       		push(@characteristic, $strainData[1]);
       		push(@characteristic, extractYear($strainData[2]));
       		push(@characteristic, extractSerotype($strainData[2]));

		}

    } elsif ($count == 4) {
    
    	push(@characteristic, $strainData[0]);
       	push(@characteristic, "human");
       	push(@characteristic, $strainData[1]);
       	push(@characteristic, $strainData[2]);
       	push(@characteristic, extractYear($strainData[3]));
       	push(@characteristic, extractSerotype($strainData[3]));

    } elsif ($count == 5) {    
         
    	updateHash(extractSerotype($strainData[4]), $strainData[1], $strainData[2], extractYear($strainData[4]));

     	push(@characteristic, $strainData[0]);
       	push(@characteristic, $strainData[1]);
       	push(@characteristic, $strainData[2]);
       	push(@characteristic, $strainData[3]);
		push(@characteristic, extractYear($strainData[4]));
       	push(@characteristic, extractSerotype($strainData[4]));

    } else {
    
    	print "Error with strain: $strain\n";
    }
    
	
    return @characteristic;
    
}

sub extractYear {

	my $year = $_[0];

	if ( m/[^0-9.]/ ) {

  				#do nothing

  	} else {
				
		$year =~ /[0-9\s]+\(/;

		$year = $&;

		$year =~ /[0-9]+/;

		$year = $&;
	  	
	  	if ($year < 10) {
		
			$year = $year + 2000;

		} else {

			if ($year < 100) {

				$year = $year + 1900;
		
			}
		}

	return $year;
	}
}

sub extractSerotype {

	my @strain = split /\(/, $_[0];

	my $serotype = '';

	$serotype = $strain[1];
	
	$serotype =~ /[HN0-9]+/;
	
	return $&;
	
}

sub updateHash {

	# Order: host, location, year, serotype
	
	# Convert location to country
	
	
	my $country = convertLocationToCountry( $_[1] );
	
	
	my $count = $serotypes{ $_[3] };
	$count = $count + 1;
	$serotypes{ $_[3] } = $count;

	$count = $hosts{ $_[0] };
	$count = $count + 1;
	$hosts{ $_[0] } = $count;

	$count = $locations{ $country };
	$count = $count + 1;
	$locations{ $country } = $count;

	$count = $years{ $_[2] };
	$count = $count + 1;
	$years{ $_[3] } = $count;
	
	#Combination
	
	$decade = substr($_[2], 0, 3) . "0";
	
	$count = $year_host{ $decade . "-" . $_[0] };
	$count = $count + 1;
	$year_host{  $decade . "-" . $_[0] } = $count;
	
	$count = $year_location{ $decade . "-" . $country };
	$count = $count + 1;
	$year_location{ $decade . "-" .$country } = $count;
	
	$count = $host_location{ $_[0] . "-" . $country };
	$count = $count + 1;
	$host_location{ $_[0] . "-" . $country } = $count;
	
	$count = $host_location_year{ $_[0] . "-" . $country . "-" . $decade};
	$count = $count + 1;
	$host_location_year{ $_[0] . "-" . $country . "-" . $decade } = $count;
	
	
}

sub convertLocationToCountry {

	# test to see if a region is present
	
	my $location = $_[0];
	
	if ( exists $regions{ $location } ){
	 
		# Get country ID from location hash
	
		my $countrykey = $regions{$location};
		
		# get Country name from country hash using ID
		
		return $countries{$countrykey };
    
    } elsif ( exists $regioncodes{ $location } ){
	 
		# Get country ID from location hash
	
		my $countrykey = $regioncodes{$location};
		
		# get Country name from country hash using ID
		
		return $countries{$countrykey };
      
	} elsif ( exists $regions{ $location }  ) {
     
		return $location;
         
    } else {
    
    	print "$location not found in region or contry.\n";
    	
    }
}

sub readCountryFile {

# Fields: "CountryId","Country","FIPS104","ISO2","ISO3","ISON","Internet","Capital","MapReference","NationalitySingular","NationalityPlural","Currency","CurrencyCode","Population","Title","Comment"

open (LIST1, "/Users/burkesquires/Desktop/BLAST output 200905/GeoWorldMap/Countries.txt") || die "File not found\n";

while (my $line = <LIST1>) {
   
   chomp $line;
   
   # my @fields = split(/\,/,$line);
   
   my ($countryID, $country) = split(/\,/,$line);
   
   $countries{ $countryID } = $country;
} 

close LIST1;

}

sub readRegionFile {

# Fields: RegionID, CountryID, Region, Code, ADM1Code

open (LIST1, "/Users/burkesquires/Desktop/BLAST output 200905/GeoWorldMap/Regions.txt") || die "File not found\n";

while (my $line = <LIST1>) {
   
   chomp $line;
   
   my ($regionID, $countryID, $region, $code) = split(/\,/,$line);
   
   $regions{$region} = $countryID;
   
   $regioncodes{$code} = $countryID;
} 

close LIST1;

}

