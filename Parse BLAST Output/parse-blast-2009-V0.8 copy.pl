#!/usr/bin/perl
use strict;


use Bio::SearchIO;
use GD::Graph;
use GD::Graph::points;
 
# Burke Squires
# BioHealthBase BRC
# May 17, 2009

#hash definitions

my %serotypes = ();
my %hosts = ();
my %years = ();
my %locations = ();
my %year_host = ();
my %year_location = ();
my %host_location = ();
my %host_location_year = ();
my %regions = ();
my %regioncodes = ();
my %countries = ();
my $hashType = '';
my $count = 0;
my $decade = 0;
my @dataX = ();
my @dataY = ();
my @dataStrain = ();
my @lineageX = ();
my @lineageY = ();
my @lineageStrain = ();
my $maxX = 0;
my $maxY = 0;


my $outputfile = 'a-cali-04-2009-h1n1-ha-megablast-parsedblast.out';
open OUTFILE, ">$outputfile" or die "unable to open $outputfile $!";

my $metaDataFile = 'a-cali-04-2009-h1n1-ha-megablast-parsedblast.summary';
open METAFILE, ">$metaDataFile" or die "unable to open $outputfile $!";

readCountryFile();
readRegionFile();

my $in = new Bio::SearchIO(-format => 'blastxml', -file   => 'a-cali-04-2009-h1n1-ha-megablast.xml');

print OUTFILE "Strain\tAccession\tLength\tIdentical Residues\tStrain Year\tAge Difference\tNucleotide Differences\tPercent Identity\n";

print METAFILE "Metadata\n";

while( my $result = $in->next_result ) {

	while( my $hit = $result->next_hit ) {

		while( my $hsp = $hit->next_hsp ) {

  			my $strain = $hit->description;

  			$strain =~ m/Influenza A virus[a-zA-Z0-9\(\/\-\.\_\'\s]+\(H[0-9]+N[0-9]+\)/;

  			$strain = $&;

            $strain =~ s/Influenza A virus \(//;

            if ($strain) {

				my $year = parseStrainName($strain);

				my $deltaDate = 2009 - $year;
				my $deltaNucleotide = $hsp->hsp_length - $hsp->num_identical;

				recordData($strain, $deltaDate, $deltaNucleotide);

				print OUTFILE 	$strain, "\t",
			  					$hit->accession, "\t",
	  							$hsp->hsp_length, "\t",
								$hsp->num_identical, "\t",
								$year, "\t",
								$deltaDate, "\t",
	                            $deltaNucleotide, "\t",
	                            $hsp->percent_identity, "\n";
			
			}
		}
	}
}



printMetaDataReport();

selectLineageStrains();

graph();


################################################################################


sub selectLineageStrains {

my $yRangeMin = 0;
my $yRangeMax = 0;
my $i = 0;


	foreach (@dataX) {

		my $tempY = $dataY[$i];
		
		if ( $_ > 5) { 
			$yRangeMin = ( ( ( $_ - 5 ) / $maxX) * $maxY );
		} else { 
			$yRangeMin = 0; 
		}
		
		if ( $_ < ($maxY - 5) ) { 
			$yRangeMax = ( ( ( $_ + 5 ) / $maxX) * $maxY );
		} else { 
			$yRangeMin = $maxY; 
		}
		
		
		if (($dataY[$i] > $yRangeMin) && ($dataY[$i] < $yRangeMax)) {
		
			recordLineageData( $dataStrain[$i], $_, $dataY[$i]);
		
		}

		$i = $i + 1;
		
	}
}

sub recordLineageData {

	# Input: Strain, delta Year (X), delta Nucleotide (Y)
	
	push(@lineageStrain, $_[0]);
	push(@lineageX, $_[1]);
	push(@lineageY, $_[2]);

}


sub recordData {

	# Input: Strain, delta Year (X), delta Nucleotide (Y)
	
	push(@dataStrain, $_[0]);
	push(@dataX, $_[1]);
	push(@dataY, $_[2]);

	if ( $_[1] > $maxX) { $maxX = $_[1]; }
	if ( $_[2] > $maxY) { $maxY = $_[2]; }

}

sub graph {

 my @data = ( [@lineageX], [@lineageY], [@lineageX]);
  
  my $graph = GD::Graph::points->new(800, 640);
  
  $graph->set( 
      x_label           => 'Year Difference',
      y_label           => 'Nucleotide Difference',
      title             => 'North American H1N1 Age versus Nucleotide Difference',
      y_max_value       => $maxY,
      y_tick_number     => $maxY,
      y_label_skip      => 2 
  ) or die $graph->error;
  
  my $gd = $graph->plot(\@data) or die $graph->error;
  
  open(IMG, '>file.gif') or die $!;
  binmode IMG;
  print IMG $gd->gif;
}


sub printMetaDataReport {

	print METAFILE "\nHost / Location / Year:\n";
	while( my ($k, $v) = each %host_location_year ) {
	        print METAFILE "\t$k:\t$v\n";
	    }
	    

	print METAFILE "\nYear and Host:\n";
	while( my ($k, $v) = each %year_host ) {
	        print METAFILE "\t$k:\t$v\n";
	    }
	    

	print METAFILE "\nYear and Location:\n";
	while( my ($k, $v) = each %year_location ) {
	        print METAFILE "\t$k:\t$v\n";
	    }
	    

	print METAFILE "\nHost and Locations:\n";
	while( my ($k, $v) = each %host_location ) {
	        print METAFILE "\t$k:\t$v\n";
	    }
	    

	print METAFILE "\nHosts:\n";
	while( my ($k, $v) = each %hosts ) {
	        print METAFILE "\t$k:\t$v\n";
	    }
	    
	print METAFILE "\nYears:\n";
	while( my ($k, $v) = each %years ) {
	        print METAFILE "\t$k:\t$v\n";
	    }
	    
	    
	print METAFILE "\nLocations:\n";
	while( my ($k, $v) = each %locations ) {
	        print METAFILE "\t$k:\t$v\n";
	    }

}


sub parseStrainName {

    my $strain = shift;

	my @characteristic = split /\//, $strain;
    
    my $year = '';
    my $type = '';
   	my $host = '';
    my $location = '';
	my $serotype = '';
    
    #count characteristics to determine if host is human or not
    
    my $count = @characteristic;
    
    if ($count == 4) {
    
    	#serotype, host, location, year

       	#$type = $characteristic[0];
    	
    	$year = extractYear($characteristic[3]);
    	
    	updateHash(extractSerotype($characteristic[3]), "human", $characteristic[1], $year);
    
    } elsif ($count == 5) {
    
       	#$type = $characteristic[0];
    
    	$year = extractYear($characteristic[4]);
    	
    	updateHash(extractSerotype($characteristic[4]), $characteristic[1], $characteristic[2], $year);
 
    } else {
    
    	print "Error with strain: $strain\n";
    }
    
    return $year;
    
}

sub extractYear {

	my $year = $_[0];

	if ( m/[^0-9.]/ ) {

  				#do nothing

  	} else {
				
		$year =~ /[0-9\s]+\(/;

		$year = $&;

		$year =~ /[0-9]+/;

		$year = $&;
	  	
	  	if ($year < 10) {
		
			$year = $year + 2000;

		} else {

			if ($year < 100) {

				$year = $year + 1900;
		
			}
		}

	return $year;
	}
}

sub extractSerotype {

	my @strain = split /\(/, $_[0];
	
	my $serotype = '';

	$serotype = $strain[1];
	
	$serotype =~ /[HN0-9]+/;
	
	return $&;
	
}

sub updateHash {

	# Order: serotype, host, location, year
	
	# Convert location to country
	
	
	my $country = convertLocationToCountry( $_[2] );
	
	
	my $count = $serotypes{ $_[0] };
	$count = $count + 1;
	$serotypes{ $_[0] } = $count;

	$count = $hosts{ $_[1] };
	$count = $count + 1;
	$hosts{ $_[1] } = $count;

	$count = $locations{ $country };
	$count = $count + 1;
	$locations{ $country } = $count;

	$count = $years{ $_[3] };
	$count = $count + 1;
	$years{ $_[3] } = $count;
	
	#Combination
	
	$decade = substr($_[3], 0, 3) . "0";
	
	$count = $year_host{ $decade . "-" . $_[1] };
	$count = $count + 1;
	$year_host{  $decade . "-" . $_[1] } = $count;
	
	$count = $year_location{ $decade . "-" . $country };
	$count = $count + 1;
	$year_location{ $decade . "-" .$country } = $count;
	
	$count = $host_location{ $_[1] . "-" . $country };
	$count = $count + 1;
	$host_location{ $_[1] . "-" . $country } = $count;
	
	$count = $host_location_year{ $_[1] . "-" . $country . "-" . $decade};
	$count = $count + 1;
	$host_location_year{ $_[1] . "-" . $country . "-" . $decade } = $count;
	
	
}

sub convertLocationToCountry {

	# test to see if a region is present
	
	my $location = $_[0];
	
	if ( exists $regions{ $location } ){
	 
		# Get country ID from location hash
	
		my $countrykey = $regions{$location};
		
		# get Country name from country hash using ID
		
		return $countries{$countrykey };
    
    } elsif ( exists $regioncodes{ $location } ){
	 
		# Get country ID from location hash
	
		my $countrykey = $regioncodes{$location};
		
		# get Country name from country hash using ID
		
		return $countries{$countrykey };
      
	} elsif ( exists $regions{ $location }  ) {
     
		return $location;
         
    } else {
    
    	print "$location not found in region or contry.\n";
    	
    }
}

sub readCountryFile {

# Fields: "CountryId","Country","FIPS104","ISO2","ISO3","ISON","Internet","Capital","MapReference","NationalitySingular","NationalityPlural","Currency","CurrencyCode","Population","Title","Comment"

open (LIST1, "Countries.txt") || die "File not found\n";

while (my $line = <LIST1>) {
   
   chomp $line;
   
   # my @fields = split(/\,/,$line);
   
   my ($countryID, $country) = split(/\,/,$line);
   
   $countries{ $countryID } = $country;
} 

close LIST1;

}

sub readRegionFile {

# Fields: RegionID, CountryID, Region, Code, ADM1Code

open (LIST1, "Regions.txt") || die "File not found\n";

while (my $line = <LIST1>) {
   
   chomp $line;
   
   my ($regionID, $countryID, $region, $code) = split(/\,/,$line);
   
   $regions{$region} = $countryID;
   
   $regioncodes{$code} = $countryID;
} 

close LIST1;

}
