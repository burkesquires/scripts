#"/usr/local/bin/python3

import os.path

def parse_full_path(full_path):
    import os.path
    if os.path.exists(full_path):
        (file_path, file_name_with_ext) = os.path.split(full_path)
        (file_name, extension) = os.path.splitext(file_name_with_ext)
        return file_path, file_name, extension
    else:
        return "", "", ""


def main(args):
    from Bio.Blast import NCBIXML
    result_handle = open(args.blast_file)
    blast_record = NCBIXML.read(result_handle)
    file_path, file_name, extension = parse_full_path(args.blast_file)
    output_file = "%s/%s.%s.%s" % (file_path, file_name, "parsed", ".txt")
    output = open(output_file, 'w')
    output.write("Accession\tHit Def.\tHit ID\tTitle\tLength\t")
    output.write("Expect Value\tAlignment Length\tBits\tGaps\tIdentities\tPositives\tScore\t")
    if args.extended_output:
        output.write("Match\tQuery\tQuery Start\tQuery End\tSubject\tSubject Start\tSubject End\n")
    else:
        output.write("\n")
    for alignment in blast_record.alignments:
        for hsp in alignment.hsps:
            output.write("%s\t" % alignment.accession)
            output.write("%s\t" % alignment.hit_def)
            output.write("%s\t" % alignment.hit_id)
            output.write("%s\t" % alignment.title)
            output.write("%i\t" % alignment.length)
            output.write("%f\t" % hsp.expect)
            output.write("%i\t" % hsp.align_length)
            output.write("%f\t" % hsp.bits)
            output.write("%i\t" % hsp.gaps)
            output.write("%i\t" % hsp.identities)
            output.write("%f\t" % hsp.positives)
            output.write("%f\t" % hsp.score)
            if args.extended_output:
                output.write("%s\t" % hsp.match)
                output.write("%s\t" % hsp.query)
                output.write("%i\t" % hsp.query_start)
                output.write("%i\t" % hsp.query_end)
                output.write("%s\t" % hsp.sbjct)
                output.write("%i\t" % hsp.sbjct_start)
                output.write("%i\t" % hsp.sbjct_end)
            output.write("\n")

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    files = parser.add_argument_group('files')
    files.add_argument("-i", "--blast_file", required=True, help="The BLAST XML file to be parsed")
    files.add_argument("-o", '--output_file', type=argparse.FileType('wt'), help="The parsed output file")

    optional = parser.add_argument_group('optional')
    optional.add_argument("-x", dest="extended_output", help="The level of output: standard or extended?", action='store_true')

    args = parser.parse_args()

    main(args)
