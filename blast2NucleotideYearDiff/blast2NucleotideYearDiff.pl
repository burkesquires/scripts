#!/usr/bin/perl
#
#	Created by R. Burke Squires on 21-Sep-2011
#	Copyright (C) 2011 UT Southwestern  All Rights Reserved

=head1 [progam_name]

 description: This program takes a BLAST result file as input and outputs a data file and graph showing the nucleotide and year of isolation differences between the query source and each hit.

=cut

#use feature ':5.14';
use strict;

#use warnings;
#use Data::Dumper;
use Getopt::Long;

# use Template;
# use PMG::PMGBase;
# use File::Temp qw/ tempfile tempdir /;
# use File::Slurp;
# use File::Copy;
# use File::Path;
# use File::Spec;
# use File::Basename qw(basename dirname);
# use List::Util qw(reduce max min);
# use List::MoreUtils qw(uniq indexes each_arrayref natatime);

# my $PMGbase = PMG::PMGBase->new();

use GD::Graph::linespoints;
require 'save.pl';

my $prog  = $0;
my $usage = <<EOQ;
Usage for $0:

  >$prog [-test -help -verbose]

EOQ

my $date = get_date();

my $help;
my $debug;
my $input;

my $bsub;
my $log;
my $stdout;
my $stdin;
my $segment;

my @accessions;
my @yearOfIsolation;
my @normalizedScore;
my @length = ( 2341, 2341, 2233, 1775, 1565, 1413, 1027, 890 );

my $ok = GetOptions(
    'debug:i'   => \$debug,
    'in'        => \$input,
    'help'      => \$help,
    'stdout'    => \$stdout,
    'stdin'     => \$stdin,
    'segment:i' => \$segment

);

if ( $help || !$ok ) {
    print $usage;
    exit;
}

sub get_date {
    my ( $day, $mon, $year ) = (localtime)[ 3 .. 5 ];
    return my $date = sprintf "%04d-%02d-%02d", $year + 1900, $mon + 1, $day;
}

sub parse_csv_args {

    my $csv_str = shift;
    return [ split ',', $csv_str ];
}

sub parseBLAST {
    use Bio::Search::Result::BlastResult;
    use Bio::SearchIO;

    my ( $queryYear, $hitYear ) = 0;

    my $blastresults = Bio::SearchIO->new( -file => @_, -format => 'blast' );

    while ( my $result = $blastresults->next_result ) {

        print "Query: " . $result->query_name . "\n";

        my $queryAccession;

        while ( my $hit = $result->next_hit ) {

            my @query = split( /\|/, $result->query_name );

            if ( @query = 1 ) {
                $queryAccession = $result->query_name;
            }
            else {
                $queryAccession = $query[2];
            }

            while ( my $hsp = $hit->next_hsp ) {
                my $hitAccession = $hit->name;

                #print "Year: " . $hitYear . "\n";
                my $hspLength            = $hsp->length('total');
                my $numIdenticalResidues = $hsp->num_identical;
                my $bitScore             = $hit->bits();

                #my $rawScore = $hit->raw_score();
                my $seqDiff = $hspLength - $numIdenticalResidues;
                my $normalizedScore =
                  $seqDiff * ( $hspLength / $length[$segment] );

                #my $yearDiff = $queryYear - $hitYear;
                push( @accessions,      $hitAccession );
                push( @yearOfIsolation, $hitYear );
                push( @normalizedScore, $hitAccession );

            }
        }
    }
}

parseBLAST( $ARGV[0] );

my $count      = 0;
my $array_size = @accessions;

for ( $count = 0 ; $count < $array_size ; $count++ ) {
    print
      "$accessions[$count]\t$yearOfIsolation[$count]\t$normalizedScore[$count]";
}

print STDERR "Processing graph\n";

my @data = ( @yearOfIsolation, @normalizedScore );

my $my_graph = new GD::Graph::linespoints();

$my_graph->set(
    x_label       => 'X Label',
    y_label       => 'Y label',
    title         => 'A Lines and Points Graph',
    y_max_value   => 80,
    y_tick_number => 6,
    y_label_skip  => 2,
    y_long_ticks  => 1,
    x_tick_length => 2,
    markers       => [ 1, 5 ],

    skip_undef => 1,

    transparent => 0,

) or warn $my_graph->error;

$my_graph->set_legend( 'data set 1', 'data set 2', 'data set 3' );
$my_graph->plot( \@data );
save_chart( $my_graph, 'sample41' );
