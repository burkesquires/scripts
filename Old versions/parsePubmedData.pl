#!/usr/bin/perl
use strict;

use XML::Parser;
use XML::SimpleObject;

my %strainHash;
my $file    = './pubmed_result.xml';
my $outfile = $file . '.tab';

my $parser = XML::Parser->new( ErrorContext => 2, Style => "Tree" );
my $xmlobj = XML::SimpleObject->new( $parser->parsefile($file) );

extractAccessionAndStrainName();

open outFile, '>' . $outfile or die $!;

print outFile "Pandemic H1N1 2009 Related Articles from Pubmed.\n";
print outFile "Pubmed ID\tJournal\tArticle Title\tAuthors";

foreach
  my $article ( $xmlobj->child('PubmedArticleSet')->child('PubmedArticle') )
{

	#1) PubMed ID
	#2) Title
	#3) Authors
	#4) Abstract if a really interesting paper
	#5) H1N1 virus strains discussed
	#6) Protein or segment names in paper
	#7) Country or location where viruses were isolated

	print outFile $article->child('MedlineCitation')->child('PMID')->value
	  . "\t";

	print outFile $article->child('MedlineCitation')->child('Article')
	  ->child('Journal')->child('Title')->value . "\t";

	print outFile $article->child('MedlineCitation')->child('Article')
	  ->child('ArticleTitle')->value . "\t";

	# Get the author names,

	if ( $article->child('MedlineCitation')->child('Article')
		->child('AuthorList') )
	{

		if ( $article->child('MedlineCitation')->child('Article')
			->child('AuthorList')->child('Author')->children )
		{

			foreach
			  my $author ( $article->child('MedlineCitation')->child('Article')
				->child('AuthorList')->child('Author') )
			{

				foreach my $child ( $author->children ) {

					if ( $child->name ne 'ForeName' ) {

						print outFile $child->value . ", ";

					}

				}

			}
		}

	}

	# get accession information

	if ( $article->child('MedlineCitation')->child('Article')
		->child('DataBankList') )
	{
		if ( $article->child('MedlineCitation')->child('Article')
			->child('DataBankList')->child('DataBank')
			->child('AccessionNumberList') )
		{

			print outFile "\t";

			my $accessionlist =
			  ( $article->child('MedlineCitation')->child('Article')
				  ->child('DataBankList')->child('DataBank')
				  ->child('AccessionNumberList') );

			foreach my $child ( $accessionlist->children ) {

				my $accession = $child->value;
				
				print outFile $strainHash{$accession} . ", ";

				#         }

			}
		}
	}

	print outFile "\n";
}

close outFile or die $!;

sub extractAccessionAndStrainName() {

	# open the flu xml file

	my $file2 = './Influenza_virus.xml';

	my $parser2 = XML::Parser->new( ErrorContext => 2, Style => "Tree" );
	my $xmlobj2 = XML::SimpleObject->new( $parser2->parsefile($file2) );

	foreach my $sequence ( $xmlobj2->child('sourcelist')->child('source') ) {

	my $strainName;

	foreach my $category ( $sequence->children ) {

	# run through and grab accession and strain name

		if ($category->attribute("organism")) {

			$strainName = $category->child('parameter')->attribute("strain")->value;

		} elsif ($category->attribute("sequence_info")) {
		
			my $accession = $category->child('sequence')->child('genbank')->child('parameter')->attribute("latest_accession")->value;

			# Add to hash with the accesison and strina name
				
			$strainHash{$accession} = $strainName;
			
			print $accession . ": " . $strainName;
			
			}
		}
	}
}
