#!/usr/bin/perl -w
use strict;
use Bio::AlignIO;

# August 26, 2009
# This script with compute a quantitative measure of the similarity of sequence within 
# a multiple sequence alignment.


# read in alignment file
my $str = Bio::AlignIO->new(-file => '/Users/burkesquires/Desktop/flu-pandemic-h1n1-2009-np-cluster-1.aln');
my $aln = $str->next_aln();

my $seqlength = $aln->length;
my $numseqs = $aln->no_sequences; #This is the number of sequecnes in the alignment

#create an array of arrays to hold resides
my @array;

# loop though alignment
foreach my $seq ($aln->each_seq) {

	push @array, [split(//,$seq->seq)];

}

# find columns of differing amino acids

my @scores;
my $totalScore = 0;

#loop though the columns
for (my $column = 0; $column < $seqlength; $column++) {

	my $currentResidue = '';
	my $tempScore = 0;

	#loop through each sequence
	for (my $sequence = 0; $sequence < $numseqs; $sequence++) {
	
		#we now ahve access to a column of residues;
		# we want to chack to changes in the residues
		
		if ($currentResidue eq '') { 
				
			$currentResidue = $array[$sequence][$column];
			
		} else {
			
			my $newResidue = $array[$sequence][$column];
			
			if ($newResidue eq $currentResidue) {
			
				#do nothing
			
			} else {
			
				# quantitate differences

				$tempScore++;
				
				$currentResidue = $newResidue;
			
			}
		}
	}
	
	push @scores, $tempScore;

	$totalScore = $totalScore + $tempScore;
	
}


# print report
print $totalScore . "\n";

print join( ',', @scores );

my $columnCount = @scores;
print "\n" . $columnCount . " (" . $seqlength . ")";
print "\n" . $numseqs;