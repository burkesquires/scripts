#!/usr/bin/perl
use SOAP::Lite; # +trace => qw(debug);
use Getopt::Long qw(:config no_ignore_case bundling);
use strict;
my $WSDL = 'http://lsgw.uc.teragrid.org/webservices/wsdl/JobService.wsdl';
my $soap = SOAP::Lite->service($WSDL);

# Make a simple test call
my $result = $soap->test();
print "Test Result: $result\n";