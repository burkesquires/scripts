#!/usr/local/bin/python3
#
#	Created by R. Burke Squires on 21-Sep-2011
#	Copyright (C) 2011 R. Burke Squires  All Rights Reserved

#=head1 [progam_name]
# description: This program takes a BLAST result file as input and outputs a data file and graph showing the nucleotide and year of isolation differences between the query source and each hit.
#=cut



def parse_full_path(full_path):
    import os.path
    if os.path.exists(full_path):
        (file_path, file_name_with_ext) = os.path.split(full_path)
        (file_name, extension) = os.path.splitext(file_name_with_ext)
        return file_path, file_name, extension
    else:
        return "", "", ""

def main(args):
    from Bio.Blast import NCBIXML
    from Bio.Seq import Seq
    from Bio.SeqRecord import SeqRecord
    from Bio import SeqIO
    from Bio.Alphabet import generic_protein
    from parse_flu_strain_name_data import parse_ncbi_flu_data

    segment_lengths = [ 2341, 2341, 2233, 1775, 1565, 1413, 1027, 890 ]

    result_handle = open(args.blast_file)
    blast_record = NCBIXML.read(result_handle)
    file_path, file_name, extension = parse_full_path(args.blast_file)
    output_file = "%s/%s.%s.%s" % (file_path, file_name, extension, "fasta")
    sequences = []
    for alignment in blast_record.alignments:
        query_year = 2009
        hit_accession, strain_name, isolation_date, segment, host_type, subtype, country, hit_isolation_year = parse_ncbi_flu_data(alignment.hit_def)
        for hsp in alignment.hsps:
            hit_accession, hit_strain_name, hit_isolation_date, hit_segment, hit_host_type, hit_subtype, hit_country, hit_isolation_year = parse_ncbi_flu_data(alignment.hit_def)
            hsp_length              = hsp.align_length
            num_identical_residues  = hsp.identities
            bit_score               = hsp.bits

            seq_diff = hsp_length - num_identical_residues
            segment = int(hit_segment.split(" ")[0])
            normalized_score = float(seq_diff) * ( hsp_length / segment_lengths[segment] )

            yearDiff = query_year - int(hit_isolation_year)

            print("%s\t%i\t%0.2f" % ( hit_accession, yearDiff, normalized_score))
            #accessions.append(hitAccession )
            #yearOfIsolation.append( hitYear )
            #normalized_score.append(hitAccession )

    SeqIO.write(sequences, output_file, "fasta")

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    files = parser.add_argument_group('files')
    files.add_argument("-in", "--blast_file", required=True, help="The BLAST XML file to be parsed")
    files.add_argument("-out", '--output_file', type=argparse.FileType('wt'), help="The parsed output file (default: input file name.fasta)")

    args = parser.parse_args()

    main(args)
