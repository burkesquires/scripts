#!/usr/bin/perl -w

# burke squires; richard.squires@utsouthwestern.edu
# Sept 2010

use strict;
use Spreadsheet::ParseExcel;

my @proteinList = ('PB2, PB1, PB1-F2, PA, HA, NP, NA, M1, M2, NS1, NEP');
my $inputEpitopeExcel =
  '/Users/burkesquires/Desktop/SourceFiles/IEDB epitope positive V2.xls';
my $inputPolymorphismExcel =
'/Users/burkesquires/Desktop/SourceFiles/Poly-Epitope-Compare-Human-H1N1 V2.xls';
my $inputSequenceFeatureExcel =
  '/Users/burkesquires/Desktop/SourceFiles/IEDB epitope positive V2.xls';

my %polyData       = ();
my %bcellData      = ();
my %tcellData      = ();
my %mhcbindingData = ();
my %seqfeatureData = ();

# loop through each data file for each protein

foreach (@proteinList) {

    my $protein = $_;

    # extract polymorphism data for protein

    # tabs are protein abbreviations
    # Position	Polymorphism Score	No of Epitopes at Position

    %polyData = importPolyData( $inputPolymorphismExcel, $protein, 0, 1 );

    # collect B-cell epitope data

    # convert epitope to counts

    # collect T-cell epitope data

    # convert epitope to counts

    # collect MHC binding epitope data

    # convert epitope to counts

    # collect SFVT data

    # convert epitope to counts

    # save a single data file for each protein

    # loop through each amino acid position

}

sub importPolyData {
	croak importPolyData if @_ != 4; # All three args must be supplied

	my ($file, $protein, $idColumn, $dataColumn) = @_;
	
    my $parser     = Spreadsheet::ParseExcel->new();
    my $workbook   = $parser->parse( $file );
    
    my %hashTemp = ();

    if ( !defined $workbook ) {
        die $parser->error(), ".\n";
    }

    my $worksheet = $workbook->worksheet($protein);

    my ( $row_min, $row_max ) = $worksheet->row_range();

    for my $row ( 2 .. $row_max ) {

        my $cell = $worksheet->get_cell( $row, 0 );
        my $position = $cell->value();

        $cell = $worksheet->get_cell( $row, 1 );

        $hashTemp {$position} = $cell->value();
    }

    return %hashTemp;

}

sub epitopePositionCounter {

    my @passed_params = @_;
    if ( !( $passed_params[0] ) ) { print "Not passed parameters" }

    my $file = $passed_params[0];

    open( inputData, $file ) || die "File not found\n";

    my $outputfile = $file . '.out';

    open outFile, ">$outputfile"
      or die "unable to open $outputfile $!";

    my %positions;
    my $previousID            = 0;
    my $currentID             = 0;
    my $currentStartPosition  = 0;
    my $previousStartPosition = 0;
    my $currentEndPosition    = 0;
    my $previousEndPosition   = 0;
    my $i                     = 0;

    foreach my $line (<inputData>) {

        chomp($line);

        my @fields = split( /\t/, $line );

        $currentID            = $fields[0];
        $currentStartPosition = $fields[1];
        $currentEndPosition   = $fields[2];

        if (   ( $currentID != $previousID )
            && ( $currentStartPosition != $previousStartPosition )
            && ( $currentEndPosition != $previousEndPosition ) )
        {

            for ( $i = $currentStartPosition ;
                $i <= $currentEndPosition ; $i++ )
            {

                if ( exists $positions{$i} ) {

                    $positions{$i} = $positions{$i} + 1;

                }
                else {

                    $positions{$i} = 1;

                }

            }

            $previousID            = $currentID;
            $previousStartPosition = $currentStartPosition;
            $previousEndPosition   = $currentEndPosition;

        }

    }

    print outFile "Epitope Position Counter\n\n";

    print outFile "Position\tNo of Epitopes at Position\n\n";

    foreach my $key ( sort { $a <=> $b } keys %positions ) {
        print outFile "key: " . $key . " value: " . $positions{$key} . "\n";
    }

    close(inputData);
    close(outFile);
}
