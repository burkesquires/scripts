#!/usr/bin/perl

#use warnings;

print "Hello, World...\n";


############# Parameters

#my $HLA_Locus = "A";
#my $Ref_HLA_Locus = "A";
#my $sfvtRef_HLA_Locus = "A";
my $HLA_Locus = "DRB1";
my $Ref_HLA_Locus = "DRB1";
my $sfvtRef_HLA_Locus = "DRB1";

#my $HLA_Locus = "DQB1";
#my $Ref_HLA_Locus = "DQB1";
my $Species = "Hsa";
my $genesys = "HLA";
#my $Locus = "HLA-DQB1";
my $Locus = "HLA-".$HLA_Locus;
my $Seqfeatlabel = $Species."_".$Locus;
my $Seqfeatname = $Seqfeatlabel."_SF";
my $Typename = "VT";

my $alignrefallele = "010101";
my $ref4digallele = "0101";
my $sfvtref4digallele = "0101";

#my $alignrefallele = "050101";
#my $ref4digallele = "0501";
my $fullalignrefallele = $Ref_HLA_Locus."*".$alignrefallele;
my $fullref4digallele = $Ref_HLA_Locus."*".$ref4digallele;
my $metafullrefallele = quotemeta $fullalignrefallele;
my $metafullref4digallele =	quotemeta $fullref4digallele;


my $alleledigits = 4;
my $reftype = 1;

my $vardefdelim = "_";
my $motifdelim = "_";
#my $seqposfrmtdelim = " .. ";
my $seqposfrmtdelim = "..";
my $seqposdelim = ", ";
my $allelegrpdelim = ", ";
my $featnamedelim = "; ";
my $feattypedelim = "; ";
my $featnodelim = "; ";

my $tempmetastr;

$vardefmetadelim = quotemeta $vardefdelim;
$motifmetadelim = quotemeta $motifdelim;
$seqposfrmtmetadelim = quotemeta $seqposfrmtdelim;
$seqposmetadelim = quotemeta $seqposdelim;
$allelegrpmetadelim = quotemeta $allelegrpdelim;
$featnamemetadelim = quotemeta $featnamedelim;
$feattypemetadelim = quotemeta $feattypedelim;
$featnometadelim = quotemeta $featnodelim;
#my $alignref4digallele = 0501;
#my $alleleseqfeat = "Allele";
#my $alleleseqfeat = $HLA_Locus." Allele";
#my $alleleseqfeat = $Seqfeatlabel."_Allele";
#my $Typename = "Type ";

#my $Ver = "25Apr2008";
#my $alignVer = "2-23-0";
my $alignVer = "2-24-0";
####### In matprotlen and others below consider the ref allele's del positions as 1 position
####### In matprotlen and others below consider the ref allele's del positions as 1 position
my $matprotlen = 237;
my $signalseqst = 1;
my $signalseqed = 29;
my $matprotseqst = 30;
my $fullprotlen = 266;
###### align matprotseqst considers each del position as 1 position
my $alignmatprotseqst = 30; 
$alignseqstartpos = -29;
$alignsignallen = 29;
#my $matprotlen = 191;
#my $matprotlen = 341;
my $alignprotlen = 266;
#my $alignprotlen = $matprotlen + $alignsignallen;
#my %translalignpos;
#my %translvardefctpos;

my @delposits = ();
#my @delposits = (294);
#my @delposits = (226);
my $possplitthresh = 4;

my $custom1 = "IndivPos";
my $custom2 = "Mat_alleles";
my $custom3 = "MatprotSeq";

my $insertion = "ins";
my $deletion = "del";
my $insdel = "indel";

#my $Sqftname = $HLA_Locus." mature protein position";

#my @Sqftname = ("Allele", "mature protein", "mature protein position");
#my @Sqftname = ($Seqfeatlabel."_Allele", $Seqfeatlabel."_Mature protein", $Seqfeatlabel."_Mature protein variant position", $Seqfeatlabel."_Mature protein indel position");
#my @Sqftname = ($Seqfeatlabel."_allele", $Seqfeatlabel."_mature protein", $Seqfeatlabel."_polymorphic position", $Seqfeatlabel."_full-length protein indel position", $Seqfeatlabel."_full-length protein", $Seqfeatlabel."_signal peptide");
#my $Sqftname = "mature protein position";
#my @Featuretypes = ("Structural", "Functional", "Structural_Functional Combination", "Structural - variant location", "Structural - complete protein", "Structural - Domain","Structural - Secondary structure motif");
#my @Featuretypes = ("Structural", "Functional", "Structural_Functional Combination", "Evolutionary - Single polymorphic amino acids", "Structural - Complete protein", "Structural - domain","Structural - Secondary structure motif");
#my @Featuretypes = ("Structural", "Functional", "Sequence Alteration", "Structural_Functional Combination", "Functional_Sequence Alteration Combination", "Structural_Sequence Alteration Combination", "Structural - Complete protein", "Structural - Cleaved peptide region", "Structural - Domain", "Structural - Secondary structure motif", "Sequence Alteration - Single polymorphic amino acids", "Sequence Alteration - Insertions and Deletions");
my @Sqftname = ($Seqfeatlabel."_allele", $Seqfeatlabel."_mature protein", $Seqfeatlabel."_variant position", $Seqfeatlabel."_indel position", $Seqfeatlabel."_full-length protein", $Seqfeatlabel."_signal peptide");
my @Featuretypes = ("Structural", "Functional", "Sequence Alteration", "Structural_Functional Combination", "Functional_Sequence Alteration Combination", "Structural_Sequence Alteration Combination", "Structural - Complete protein", "Structural - Cleaved peptide region", "Structural - Domain", "Structural - Secondary structure motif", "Sequence Alteration - Single amino acid variation", "Sequence Alteration - Insertions and Deletions");

#my @Majortypes = ("Structural", "Functional", "Evolutionary");
my @Majortypes = ("Structural", "Functional", "Sequence Alteration");
my %seqposfeattypes;
my %seqposfeatnames;
########## Using comp. col nos.
my $seqlineallelecol = 0;
my $seqlinemotifseqcol = 1;
#my $seqlinematseqcol = 1;
my $seqlinevardefseqcol = 2;
my $alignsetidentfrcol = 0;
my $alignsetidentfr = "Prot";
#my @alignsymbs = ("A-Z", "-", ".", "*");
my %alignsymborder = ("-" => 2, "." => 3, "*" => 4);

my $alphabet;

@alphabets = 'A'..'Z';



my $nullcode = "N";


####### Using the starting no. not comp. ref. nos.

#my @customPosits = (1..$matprotlen);
my @customPosits = (1..$alignprotlen);
#my @customPosits = (1..$alignprotlen);

my $Alleles_CustSqft;
my $Motif_CustSqftType;
my $Alleles_CustUnkSqft;
my $Alleles_CustSqftType;
my $Alleles_Posamino;
my $Alleles_PosTypes;
my $Vardef_CustSqftType;


my @inpfiles;
my @outfiles;

######## Input files
$inpfiles[0] = $HLA_Locus."_prot_tabdelim_".$alignVer.".txt";
#$inpfiles[0] = "C_prot_tabdelim_".$alignVer.".txt";
#$inpfiles[0] = $HLA_Locus."_prot_tabdelim_test_".$alignVer.".txt";
#$inpfiles[0] = "Genrtd_".$HLA_Locus."_Alleles_SeqFeat_Varnts_".$Ver." Matrix";


######## Output files

#$outfiles[0] = "Genrtd_".$custom."_".$HLA_Locus."_Motif_Variants";
#$outfiles[1] = "Genrtd_".$custom."_".$HLA_Locus."_Allele_Variants";
#$outfiles[2] = "Genrtd_".$custom."_".$HLA_Locus."_Unknown and Non Variants";
#$outfiles[3] = "Genrtd_".$HLA_Locus."_".$custom."_Seqfeatures";

$outfiles[0] = "Genrtd_".$custom1."_".$custom2."_".$Locus."_Motif_Variants_".$alignVer;
$outfiles[1] = "Genrtd_".$custom1."_".$custom2."_".$Locus."_Allele_Variants_".$alignVer;
$outfiles[2] = "Genrtd_".$custom1."_".$custom2."_".$Locus."_Unknown and Non Variants_".$alignVer;
$outfiles[3] = "Genrtd_".$Locus."_".$custom1."_".$custom2."_Seqfeatures_".$alignVer;
$outfiles[4] = "Debug_Report";
$outfiles[5] = "Log_Report_".$Locus."_".$custom1."_".$custom2;
$outfiles[6] = "Genrtd_".$Locus."_Alleles_MatProt_vardef_delim seqs_".$alignVer;
#$outfiles[6] = "Genrtd_".$custom3."_".$HLA_Locus."_Allele_Variants_".$alignVer;

#my $tempfilename = "Genrtd_".$custom."_".$HLA_Locus."_Motif_Variants";
#my $tempfilename = "Genrtd_".$custom."_".$HLA_Locus."_Sqft_Variants";

#open SqftVarntsfile, ">$otufiles[0]";
#print SqftVarntsfile "Locus Name\tHLA Protein Feature Name\tHLA Protein Feature Type\tAllele Group(s)\tMotif\tVariant Type\tFeature Location - Amino Acid Positions\tVariant Type Definition\n";

#open AlleleVarntsfile, ">$outfiles[1]";
#print AlleleVarntsfile "HLA Allele Name";

#$tempfilename = "Genrtd_".$custom."_".$HLA_Locus."_Unknown and Non Variants";
#open Unk_NonVarntsfile, ">$outfiles[2]";
#print Unk_NonVarntsfile "Locus Name\tAmino Acid Position\tAmino Acid Residue\tAllele Group\n";




#my @customPosits = (1..192);

#my @customPosits = (1..341);

open debug, ">$outfiles[4]";
open logrept, ">$outfiles[5]";
#open debug, ">Debug_Report";


#foreach $alphabet (@alphabets) 
foreach $alphabet ('A'..'Z') 
	{
		#print debug "\n$alphabet\tBefore: \t$alignsymborder{$alphabet}";
		$alignsymborder{$alphabet} = 1;
		#print debug "\n$alphabet\tAfter: \t$alignsymborder{$alphabet}";
	}

#---------------------------------------------------------


sub Splitrow
{
	my @params = @_;
	my $i = 0;
	my $temprow = $params[0];
	
	my $tempdelim = $params[1];
	$tempdelim = quotemeta $tempdelim if (!($params[1] =~ /\t/i));
	#print debug "\nLine 163: tempdelim = $tempdelim";
	#my @splitrows = split /$params[1]/, $temprow;
	#my @Rowsplits = split /$params[1]/, $temprow;
	my @Rowsplits = split /$tempdelim/, $temprow;
	
	foreach $splits (@Rowsplits) 
		{
				$Rowsplits[$i] = Trim($Rowsplits[$i]); 
				$i++;
		}

	#my @Rowsplits = Trim(@splitrows);

	#$Rowsplits[0] = $splitrows[$params[2]-1];
	#my $i = 0;
	#$Rowsplits[1] = $splitrows[1];
	#if (0 != ($params[2]-1)) {
	#	$Rowsplits[1] = $splitrows[0];
	#	$Rowsplits[1] = $Rowsplits[1].$params[1].$splitrows[1] if (1 != ($params[2]-1));
	#}

	#foreach $i (2..@splitrows-1) {
	#	$Rowsplits[1] = $Rowsplits[1].$params[1].$splitrows[$i] if ($i != ($params[2]-1)) ;
	#}
	##print debug "The params are: \t @params\n" if ($temprow =~ /Amyotrophic/i); 
	##print debug "The params are: \t @params\n" if ($temprow =~ /Today/i); 
	##print debug "The rowsplits are: \t @Rowsplits\n" if ($temprow =~ /Amyotrophic/i); 
	#print debug "Line 222: The rowsplits for $temprow are: \tRowsplits = @Rowsplits\n"; 

#@splitrows;
@Rowsplits;
}

sub Trim
{
	my $term = $_[0];
	$_ = $term;
	s/^ *//;
	s/ *$//;
	#print debug "In the Seq imgt while and Seqsplits for loop, before mod Seqsplits = $Seqsplits\n";
	#print debug "In the Seq imgt while and Seqsplits for loop, currSeqsplits = $_ \n";
	$trimterm = $_;
	$trimterm;
}

#*********************************************************
#---------------------------------------------------------


#Func Motif Check start*****************************

sub Motifchk
{
	my $motif = $_[0];
	#print debug "\nInside Motifchk:\tMotif = $motif";
	my $motifflag = 0;
	if ($motif =~ /^[A-Z]*$/i) 
		{
				$motifflag = 1;
		}
	#print debug "\nInside Motifchk:\tMotifflag = $motifflag";
	$motifflag;
}

sub MotifPoschk
{
	my @motif = @_;
	#print debug "\nInside MotifPoschk:\tMotif[0] = $motif[0]\tMotif[1] = $motif[1]";
	my $motifflag = 0;
	if ($motif =~ /^[A-Z]$/i) 
		{
				$motifflag = 1;
		}
	#print debug "\nInside Motifchk:\tMotifflag = $motifflag";
	$motifflag;
}

#End Func Motif check*******************************

#Func Get Positions*******************************

sub GetPosits
{

my $Posit = $_[0];
my $delim1 = $_[1];
my $delim2 = $_[2];
my $Pos;
my $tempdelim1 = quotemeta $delim1;
my $tempdelim2 = quotemeta $delim2;

#my @Posits = Splitrow ($Posit, ", ");
my @Posits = Splitrow ($Posit, $delim2);
my $posct = 0;

my @finPosits;

foreach $Pos (@Posits) 
	{	
		
		#if ($Pos =~ /-/) 
		if ($Pos =~ /$tempdelim1/i) 
			{
				#$temp = $Pos;
				#my $temp =~ s/ *- */../g;
				#my @tempposits = Splitrow ($Pos, "-");
				my @tempposits = Splitrow ($Pos, $delim1);
				#my $temp = join "..", @tempposits;
				#my $temp = join ", ", $tempposits[0]..$tempposits[1];
#				my $temp = join $delim2, $tempposits[0]..$tempposits[1];
				
				push @finPosits, ($tempposits[0]..$tempposits[1]);
				print debug "\nLine 184: In getposits \tTemposits = $tempposits[0]\ttempposits[1] = $tempposits[1]\tfinposits in the Getposits function: = ",(@finPosits);
#				print debug "\nLine 185: In getposits befor pop\tPosits = ",(@Posits);
#				$Posits[$posct] = $temp;
#				pop @Posits;
#				print debug "\nLine 184: In getposits After pop\tPosits = ",(@Posits);
#				push @Posits, $tempposits[0]..$tempposits[1];
				
#				print debug "\nLine 189: In getposits after push\tPosits in the Getposits function:\t@Posits";
			}
		else
			{
				push @finPosits, $Pos;
			}
		$posct++;
	}
#@Posits;
@finPosits;
}

#End Get Positions*******************************

#Func Format seqpos*******************************
				
sub Formatseqpos
	{
		#my @params = @_;
		my ($delim1, $frmtthresh, $delim2, $givendelref, @tempseqposits) = @_;
		@givendelposits = @$givendelref;
		#my (@tempseqposits, $delim1, $frmtthresh, @givendelposits, $delim2) = @_;
		my $tempdelim1 = quotemeta $delim1;
		my $tempdelim2 = quotemeta $delim2;

		print debug "\nLine 179: In formatseqpos beginning\tdelim1 = $delim1\tdelim2 = $delim2\tfrmtthresh = $frmtthresh\ttempseqposits = @tempseqposits\tgivendelposits = @givendelposits";
		#my $Posit = $_[0];
		my $insertion = "ins";
		my $deletion = "del";
		my $insdel = "insdel";
		
		my $Pos;
		#my $delim1 = ", ";
 		
		#my $delim1 = $params[1];
		#my $delim2 = $params[4];
		#my @givendelposits = @{$params[3]};
		#my $frmtthresh = $params[2];
		#my @	tempseqposits =  @{$params[0]};
		
		#my @Posits = Splitrow (@tempseqposits, $delim1);
		
		my $tempseqpos = join $delim1, @tempseqposits;
		my $temptempseqpos = $tempseqpos;

		$tempseqpos = $delim1.($tempseqpos).",";	
		$tempseqpos =~ s/$tempdelim1[0-9]+($deletion),/,/gi;
		$tempseqpos =~ s/$tempdelim1[0-9]+($insertion),/,/gi;
		$tempseqpos =~ s/$tempdelim1[0-9]+($insdel),/,/gi;
		
		$tempseqpos =~ s/^($tempdelim1)//i;
		$tempseqpos =~ s/(,)$//i;
		
		my $tempfrmtseqpos = join ", ", ($tempseqposits[0]..$tempseqposits[-1]);
		$tempfrmtseqpos = ", ".($tempfrmtseqpos).", ";
		$tempfrmtseqpos =~ s/, (0), /, /i;
		$tempfrmtseqpos =~ s/^(, )//i;
		$tempfrmtseqpos =~ s/(, )$//i;
		$tempfrmtseqpos =~ s/, /$delim1/gi;
		print debug "\nLine 184: In formatseqpos before formatted tempfrmtseqpos = \t$tempfrmtseqpos\ttempseqpos = $tempseqpos";

		
		
		if (($tempseqpos eq $tempfrmtseqpos) && (@tempseqposits > $frmtthresh))
			{
				$tempfrmtseqpos = $tempseqposits[0].($delim2).($tempseqposits[-1]);
			}
		else
			{
				$tempfrmtseqpos = $temptempseqpos;
			}
		
		#$tempfrmtseqpos;
		
		$tempseqpos = $delim1.$tempseqpos.",";
		my $tempdelposit;
		my $delpositflag = 0;
		print debug "\nLine 550: Outside the tempdelposit loop for tempseqpos = $tempseqpos";

		################### Check for any del positions and include them in the tempseqpos
		#my $tempseqposit;
		#foreach $tempseqposit (@tempseqposits) 
		#	{
		#	}
#
#		foreach $tempdelposit (@givendelposits) 
#			{
#				#if ($tempseqpos =~ /$tempdelim1$tempdelposit$deletion,/i) 
#				if ($tempseqpos =~ /$tempdelim1$tempdelposit,/i) 
#					{
#						$tempseqpos =~ s/($delim1($tempdelposit),)/$delim1\2$delim1\2$deletion,/i;
#						#$tempfrmtseqpos =~ s/($tempdelim1($tempdelposit),)/$delim1$2$delim1$2$deletion,/i;
#						$delpositflag = 1;
#						#print debug "\nLine 346: Inside the formatseqpos, tempdelposit loop for tempdelposit = $tempdelposit\ttempseqpos = $tempseqpos\tMatch = $1\t$2\$3";
#					}		
#			}		
#
#
#
#		$tempseqpos =~ s/^($tempdelim1)//i;
#		$tempseqpos =~ s/(,)$//i;
		
		$tempseqpos = $temptempseqpos;

		################### End of Check for any del positions and include them in the tempseqpos
		print debug "\nLine 244: In formatseqpos ending\ttempfrmtseqpos = $tempfrmtseqpos\ttempseqpos = $tempseqpos\tdelpositflag = $delpositflag";
		($tempfrmtseqpos, $tempseqpos, $delpositflag);
	}
				
#End Func Format seqpos*******************************


#Func Getseqpos*******************************
#my $tempseqpositsref;
sub Getseqpos
	{
		my ($frmtseqpos, $delim2, $delim1, @givendelposits) = @_;
		#my @params = @_;
		
		#my $Posit = $_[0];
		my $insertion = "ins";
		my $deletion = "del";
		my $insdel = "insdel";
		
		my $tempdelim1 = quotemeta $delim1;
		my $tempdelim2 = quotemeta $delim2;
		
		my $Pos;
		#my $delim1 = ", ";
		#my $delim2 = $params[1];
		#my $delim1 = $params[3];
		#my @givendelposits = @{$params[2]};
		#my $frmtthresh = $params[2];
		#my $frmtseqpos = $params[0];
		#$frmtseqpos =~ /^([0-9]+) ?$delim2 ?([0-9]+)$/i;
		#my @tempseqposits =  (($1)..($2));
		
		my @tempseqposits =  GetPosits($frmtseqpos, $delim2, $delim1);
		
		#my $tempseqpos = join $delim1, @tempseqposits;
		my $tempseqpos = join ", ", @tempseqposits;
		
		$tempseqpos = ", ".($tempseqpos).", ";
		$tempseqpos =~ s/, (0), /, /i;
		$tempseqpos =~ s/^(, )//i;
		$tempseqpos =~ s/(, )$//i;
		$tempseqpos =~ s/, /$delim1/gi;
		
		print debug "\nLine 275: In Getseqpos beginning\ttempseqposits = @tempseqposits\tgivendelposits = @givendelposits\ttempseqpos = $tempseqpos";

		$tempseqpos = $delim1.$tempseqpos.",";
		my $tempdelposit;
		my $delpositflag = 0;
		#print debug "\nLine 550: Outside the tempdelposit loop for tempseqpos = $tempseqpos";

		################### Check for any del positions and include them in the tempseqpos
		#my $tempseqposit;
		#foreach $tempseqposit (@tempseqposits) 
		#	{
		foreach $tempdelposit (@givendelposits) 
			{
				if (($tempseqpos =~ /($tempdelim1($tempdelposit),)$/i) || ($tempseqpos =~ /^($tempdelim1($tempdelposit),)$/i) )
					{
						next;
					}
				elsif ($tempseqpos =~ /$tempdelim1$tempdelposit,/i) 
					{
						#$tempseqpos =~ s/($delim1($tempdelposit),)/$delim1\2$delim1\2$deletion,/i;
						$tempseqpos =~ s/($tempdelim1($tempdelposit),)/$delim1$2$delim1$2$deletion,/i;
						$delpositflag = 1;
						#print debug "\nLine 313: Inside the tempdelposit loop for tempdelposit = $tempdelposit\ttempseqpos = $tempseqpos\tMatch = $1\t$2\$3";
					}		
			}		
		#	}
		
		$tempseqpos =~ s/^($tempdelim1)//i;
		$tempseqpos =~ s/(,)$//i;
		################### End of Check for any del positions and include them in the tempseqpos
		@tempseqposits = Splitrow ($tempseqpos,$delim1);
		print debug "\nLine 302: In getseqpos ending\ttempseqpos = $tempseqpos\ttempseqposits = @tempseqposits\tdelpositflag = $delpositflag";
		#$tempseqpositsref = \@tempseqposits;
		#($tempseqpos, $tempseqpositsref, $delpositflag);
		($tempseqpos, $delpositflag, @tempseqposits);
	}
				
#End Func Getseqpos*******************************


#my @files;
#@files = @ARGV;
#open SeqFeat, "<$files[0]";
#open phen, "<$files[1]";
#open matches, ">matchedSeqFeat";
my %SeqFeats_Pos;
my @SeqFeat;
my %CustSeqFTalels;
my %allAlleles;
my @dig4Alleles;
my $all4digAlleles;

my %fin4digAlleleSeqs;
my %alelecode;
my $fin4digAllelemotifSeqs;
my $fin4digAllelevardefSeqs;

my %fin4digAllelemotifSeqs;
my %fin4digfullprotvardefs;

my %fin4digmatmotifSeqs;
my %fin4digmatprotvardefs;

my %fin4digleadmotifseqs;
my %fin4digleadvardefseqs;

my $matseqpos;
my $fullseqpos;
my $leaderseqpos;

#my %nullalleles;
my @refseqnumb;
#open AlleleSeqFeats, "<$inpfiles[0]";
open AllelealignSeqs, "<$inpfiles[0]";

#my $tempfilename = "Genrtd_".$custom."_".$HLA_Locus."_Motif_Variants";
#my $tempfilename = "Genrtd_".$custom."_".$HLA_Locus."_Sqft_Variants";
$tempdatetime = scalar localtime();

#print logrept "\nLog Report for $Locus $custom1_$custom2 variants generation from Alignment data of Version: $alignVer as of ", $tempdatetime, "\n";


my $alignseqstflag = 0;
my $alignsetflag = 0;
my $allelect = 0;
######################	Gathering allele alignment sequences and concatenating them to form a full sequence for an allele

while (<AllelealignSeqs>) 
	{
		chomp;
		my $alignseqline = $_;
		my @alignseqlineterms = Splitrow ($alignseqline, "\t");

		if ($alignseqlineterms[$alignsetidentfrcol] =~ /^$alignsetidentfr/i) 
			{
				$alignseqstflag = 1;
				$alignsetflag++;
				#print logrept "\n\n############## The alleles that are inconsistently represented in the aligned sets of sequences are: ";
				next;
			}
		
		if ($alignseqstflag == 0) 
			{
				#print logrept "\n$alignseqline";
				next;
			}		
		

		my $tempallele = shift(@alignseqlineterms);
		my $tempseq = join "", @alignseqlineterms;

		
		$tempallele =~ /^($HLA_Locus\*[0-9]+([A-Z]*))$/i;
		#$tempallele =~ s/([A-Z])$//i;

		$alelecode{$tempallele} =  $2;
		
		if ($alelecode{$tempallele} =~ /^($nullcode)$/i) 
			{
				#print logrept "\n$tempallele";
				next;
			}
		
		if ((!($tempallele =~ /^($metafullref4digallele)/i)) && (!($tempallele =~ /^($HLA_Locus\*)/i)))
			{
				#print logrept "\n$tempallele";
				next;
			}

		if (($alignsetflag > 1) && !(defined($allAlleles{$tempallele})))
			{
				#print logrept "\n$tempallele";
			}
		
		$allAlleles{$tempallele} = ($allAlleles{$tempallele}).$tempseq;
		
		
		#$tempallele =~ /($HLA_Locus\*[0-9]{$alleledigits})/i;
		$tempallele =~ /^(.*\*[0-9]{$alleledigits})/i;
		
		my $temp4digallele = $1;
		
#		if ($tempallele =~ /^($metafullref4digallele)/i) 
#			{
#				$temp4digallele = $1		
#			}
#		
		
		#$all4digAlleles->{$temp4digallele} = \%allAlleles;
		if (!(keys %{$all4digAlleles->{$temp4digallele}} >= 1))
			{
				$dig4Alleles[$allelect] = $temp4digallele;
				$allelect++;		
			}
		

		$all4digAlleles->{$temp4digallele}{$tempallele} = $allAlleles{$tempallele};
		
	}


######################	End of Gathering allele alignment sequences and concatenating them to form a full sequence for an allele


#print logrept "\n\n############## The final 4 digit allele sequences that were derived from multiple 6 digit alleles: ";


######################   To get the final 4 digit allele sequence

my $dig4allele;
my $fullallele;
my $reffullallele;
my $currresdsymb;

my @refalleleresds;
#my @prevalleleresds;
#my @curr4digalleleresds;

my $currpos = 0;

my $vardefcurrpos = $currpos;
#my $vardefpossymb = $vardefcurrpos;
my $insdelflag = 0;
my $insdelct = 0;
my $refalleleflag = 0;
my $refvardefct = 0;

#$reffullallele = ($HLA_Locus).("*").($alignrefallele);
$reffullallele = $fullalignrefallele;

@refalleleresds = split("",$allAlleles{$reffullallele});


#foreach $currpos (($signalseqst-1)..($signalseqed-1)) 
#	{
#		my $signalresd = shift(@refalleleresds);
#	}
				
$currpos = 0;

my $refallelelen = scalar @refalleleresds;
print debug "\nThe reference allele is $alignrefallele\tThe reference reffullallele is $reffullallele\tThe residues are: @refalleleresds";

#foreach $temppos () 
#	{
		
#	}

#$alignrefallele

#foreach $dig4allele (keys %$all4digAlleles)
foreach $dig4allele (@dig4Alleles)
	{
		my @prevallelesresds;
		my @curralleleresds;
		my @prevallelesvardefresds;
		my $vardefcurrpos = $currpos;
		my $vardefpossymb = $vardefcurrpos;
		#my $motifpossymb; 
		my $seqcombdflag = 0;
		my $tempfullseqpos;

		foreach  $fullallele (sort keys %{$all4digAlleles->{$dig4allele}})
			{
				
				if ($alelecode{$fullallele} =~ /^($nullcode)$/i) 
					{
						#print logrept "\nThe null allele:\t$fullallele";
						next;
					}		
				
				#if (keys %{$all4digAlleles->{$dig4allele}} <=1) 
				#	{
						#$fin4digAlleleSeqs{$dig4allele} = $all4digAlleles->{$dig4allele}{$fullallele};
				#		$fin4digAlleleSeqs{$dig4allele} = $allAlleles{$fullallele};
				#		next;
				#	}
				
				#print debug "\n$fullallele\t$allAlleles{$fullallele}";
				
				@curralleleresds = split("",$allAlleles{$fullallele});
				
				#foreach $currpos (($signalseqst-1)..($signalseqed-1)) 
				#	{
				#		my $signalresd = shift(@curralleleresds); 
				#	}
				$currpos = 0;
				my $startposflag = 1;
				#$vardefcurrpos = $startposflag;
				$vardefcurrpos = $alignseqstartpos;
				$insdelflag = 0;
				my $vardefct = -1;
				
				$prevtempfullseqpos = "";
				

				#if (@curralleleresds < $refallelelen) 
				#if (@curralleleresds < $matprotlen) 
				if (@curralleleresds < $alignprotlen) 
					{
						#print logrept "\nThe allele with incomplete sequence:\t$fullallele";
					}

				foreach $currpos (0..@curralleleresds-1) 
					{
														
						if ($curralleleresds[$currpos] eq "-") 
							{
								$curralleleresds[$currpos] = $refalleleresds[$currpos];
							}
						
						my $tempcurrresd = $curralleleresds[$currpos];
						my $tempprevresd = $prevallelesresds[$currpos];
						my $temprefresd = $refalleleresds[$currpos];
						$vardefpossymb = "";
						#$motifpossymb = "":
						$seqcombdflag = 0;
						#print debug "\nLine 668: Current allele = $fullallele\tCurrpos = $currpos\tRefresdiue = $temprefresd\tCurrallele residue = $tempcurrresd\talignsymbol = \t$alignsymborder{$tempprevresd}\tThe tempprevresd is:\t$prevallelesresds[$currpos]";
						#my $tempcurrresd = "\\".$currresd;
						
						#if ($tempcurrresd =~ /[A-Z]/i)
						#	{
						#		$currresdsymb = "A-Z";
						#	}
						#else
						#	{
						#		$currresdsymb = $tempcurrresd;
						#	}
						#$refalleleflag = 1 if ($fullallele =~ /^($HLA_Locus\*($alignrefallele))$/i);


						#if ((!(defined $alignsymborder{$tempprevresd})) || ($alignsymborder{$tempcurrresd} < $alignsymborder{$tempprevresd}) || (!(defined $prevallelesresds[$currpos])))  
						if ((!(defined $prevallelesresds[$currpos])) || ($tempprevresd eq "") || ($alignsymborder{$tempcurrresd} < $alignsymborder{$tempprevresd}))  
							{
								#$seqcombdflag = 1 if ($fullallele =~ /\*($alignrefallele)/i);
								$seqcombdflag = 1;
								#print debug "\nLine 685: Inside the alignsymborder if block, fullseqpos = $fullseqpos\ttempfullseqpos = $tempfullseqpos\tprevtempfullseqpos = $prevtempfullseqpos";
								$prevallelesresds[$currpos] = $tempcurrresd;
							}
								
						#if (($temprefresd eq ".") && $alignsymborder{$tempcurrresd} == 1)
						if (($temprefresd eq ".") && ($tempcurrresd =~ /[A-Z]/i))
							{
								#$vardefpossymb = $vardefcurrpos.$insertion if ($insdelflag == 0);
								if ($insdelflag == 0)
									{
										#$translalignpos{$currpos} = $vardefcurrpos;
										#$translvardefctpos{$vardefct} = $vardefcurrpos;
										$vardefpossymb = $vardefcurrpos.$insertion;
										#$vardefpossymb = $vardefcurrpos.$insertion;
										$vardefct++;
								#		$motifpossymb = "";
									}
								
								#$vardefcurrpos++;
								$insdelct++;
								#$vardefpossymb = $insertion.$vardefcurrpos.".".$insdelct;
								#$vardefpossymb = $vardefcurrpos.".".$insdelct.$insertion;
								$vardefpossymb = $vardefpossymb.$tempcurrresd;
								#$vardefpossymb = $vardefcurrpos.$insertion.$tempcurrresd;
								#$motifpossymb = $motifpossymb.$tempcurrresd;
								#$refseqnumb[$currpos] = $vardefcurrpos.$deletion;
								$refseqnumb[$vardefct] = "$vardefcurrpos"."$deletion";
								$prevtempfullseqpos = $prevtempfullseqpos.($seqposdelim).$refseqnumb[$vardefct] if ($insdelflag == 0);
								#$refvardefct++;
								#$vardefpossymb = $vardefcurrpos.".".$insdelct.$insertion.$tempcurrresd;
								#$refseqnumb[$currpos] = $vardefcurrpos.".".$insdelct;
								$insdelflag = 1;
								#$vardefcurrpos--;
							}
						#elsif (($temprefresd eq ".") && $alignsymborder{$tempcurrresd} == 3)
						elsif (($temprefresd eq ".") && ($tempcurrresd eq "."))
							{
								#$vardefpossymb = $vardefcurrpos.$deletion if ($insdelflag == 0);
								if ($insdelflag == 0)
									{
										$vardefpossymb = $vardefcurrpos.$deletion;
										$vardefct++;
								#		$motifpossymb = "";
									}
								#$vardefcurrpos++;
								$insdelct++;
								#$vardefpossymb = $vardefcurrpos.$deletion.$tempcurrresd;
								$vardefpossymb =  $vardefpossymb;
								#$motifpossymb = $motifpossymb.$tempcurrresd;
								#$refseqnumb[$currpos] = $vardefcurrpos.$deletion;
								#$refseqnumb[$refvardefct] = $vardefcurrpos.$deletion;
								$refseqnumb[$vardefct] = "$vardefcurrpos"."$deletion";
								$prevtempfullseqpos = $prevtempfullseqpos.($seqposdelim).$refseqnumb[$vardefct] if ($insdelflag == 0);
								#$refvardefct++;
								#$vardefpossymb = $vardefcurrpos.".".$insdelct.$deletion;
								#$refseqnumb[$currpos] = $vardefcurrpos.".".$insdelct;
								$insdelflag = 1;
								#$vardefcurrpos--;
							}
						#elsif (($temprefresd eq ".") && $alignsymborder{$tempcurrresd} == 4)
						elsif (($temprefresd eq ".") && ($tempcurrresd eq "*"))
							{
								#$vardefpossymb = $vardefcurrpos.$insdel if ($insdelflag == 0);
								if ($insdelflag == 0)
									{
										$vardefpossymb = $vardefcurrpos.$insdel;
										$vardefct++;
								#		$motifpossymb = $vardefcurrpos.$insdel;
									}
								#$vardefcurrpos++;
								$insdelct++;
								#$vardefpossymb = $vardefcurrpos.".".$insdelct;
								$vardefpossymb = $vardefpossymb.$tempcurrresd;
								#$motifpossymb = $motifpossymb.$tempcurrresd;
								#$refseqnumb[$currpos] = $vardefcurrpos.$deletion;
								$refseqnumb[$vardefct] = "$vardefcurrpos"."$deletion";
								$prevtempfullseqpos = $prevtempfullseqpos.($seqposdelim).$refseqnumb[$vardefct] if ($insdelflag == 0);
								#$vardefpossymb = $vardefpossymb.$tempcurrresd;
								#$refseqnumb[$currpos] = $vardefcurrpos.".".$insdelct;
								$insdelflag = 1;
								#$vardefcurrpos--;
							}
						elsif (($temprefresd ne ".") && ($tempcurrresd ne ".")) 
							{
								#$vardefcurrpos = $vardefcurrpos + $insdelflag;
								$insdelflag = 0;
								$vardefct++;
								$vardefcurrpos++;
								$vardefcurrpos = $vardefcurrpos - $startposflag;
								if ($vardefcurrpos == 0) 
									{
										$vardefcurrpos++;
									}
								$insdelct = 0;
								#$vardefpossymb = $vardefcurrpos;
								$vardefpossymb = $vardefcurrpos.$tempcurrresd;
								#$motifpossymb = $vardefcurrpos.$tempcurrresd;
								#$refseqnumb[$currpos] = $vardefcurrpos;
								$refseqnumb[$vardefct] = $vardefcurrpos;
								$prevtempfullseqpos = $prevtempfullseqpos.($seqposdelim).$refseqnumb[$vardefct];
								$startposflag = 0;
							}
						elsif (($temprefresd ne ".") && ($tempcurrresd eq ".")) 
						#elsif ($tempcurrresd eq ".")
							{
								#$vardefpossymb = $vardefcurrpos.$deletion if ($insdelflag == 0);
								#$vardefcurrpos = $vardefcurrpos + $insdelflag;
								$insdelflag = 0;
								$vardefct++;
								$vardefcurrpos++;
								$vardefcurrpos = $vardefcurrpos - $startposflag;
								if ($vardefcurrpos == 0) 
									{
										$vardefcurrpos++;
									}
								$insdelct = 0;
								#$vardefpossymb = $vardefcurrpos.".".$insdelct.$deletion;
								$vardefpossymb = $vardefcurrpos.$deletion;
								#$motifpossymb = $vardefcurrpos.$tempcurrresd;
								#$refseqnumb[$currpos] = $vardefcurrpos;
								$refseqnumb[$vardefct] = $vardefcurrpos;
								$prevtempfullseqpos = $prevtempfullseqpos.($seqposdelim).$refseqnumb[$vardefct];
								#$vardefcurrpos--;
								$startposflag = 0;
							}
								
						
								
						$prevallelesvardefresds[$currpos] = $vardefpossymb if ($seqcombdflag == 1);
						
						#print debug "\nLine 839: In the change of residue section:\tcurrpos = $currpos\tprevallelesvardef = $prevallelesvardefresds[$currpos]\tvardefct = $vardefct\trefseqnumb = $refseqnumb[$vardefct]\tvardefcurrpos = $vardefcurrpos\ttempcurrresd = $tempcurrresd\ttempprevresd = $tempprevresd";
																		
								#$prevallelesmotifresds[$currpos] = $motifpossymb;

								
								#$prevallelesvardefresds[$currpos] = $vardefpossymb.$tempcurrresd;
								#if ($fullallele =~ /^($HLA_Locus\*($alignrefallele))$/i)								
								#	{
								#		$refalleleflag = 1;
								#	}						
								
								#if (($refalleleflag == 1) && ($dig4allele =~ /^($HLA_Locus\*($ref4digallele))$/i) && (!($fullallele =~ /^($HLA_Locus\*($alignrefallele))$/i)))
								#	{
								#		print logrept "\n The 4 digit reference allele sequence has been derived from multiple synonymous allele sequences";	
								#	}
						
						#$refalleleflag = 0;
			
						#########   Check for several log report conditions
						
						if ((defined $prevallelesresds[$currpos]) && ($alignsymborder{$tempcurrresd} <= 3) && ($alignsymborder{$tempprevresd} <=3) && ($tempcurrresd ne $tempprevresd))
							{
								#print logrept "\nThe allele with inconsistent sequence residue information from its synonymous alleles\t$fullallele";
							}
					}				
				$tempfullseqpos = $prevtempfullseqpos if ($seqcombdflag == 1);
			}
		
		#$fullseqpos =~ s/^($seqposmetadelim)//;
		$tempfullseqpos =~ s/^($seqposmetadelim)//;
		$prevtempfullseqpos =~ s/^($seqposmetadelim)//;
		#$fullseqpos = $tempfullseqpos if ($fullallele =~ /\*($alignrefallele)/i);
		$fullseqpos = join $seqposdelim, @refseqnumb;
		print debug "\nLine 841: Outside the foreach full allele loop if block, fullseqpos = $fullseqpos\ttempfullseqpos = $tempfullseqpos\tprevtempfullseqpos = $prevtempfullseqpos";
		#my $tempfinseq = join "",@prevallelesresds;
		#if ($dig4allele =~ /\*($ref4digallele)$/i)
		if ($dig4allele =~ /$Ref_HLA_Locus\*($ref4digallele)$/i)
			{
				my @tempposits = Splitrow($fullseqpos, $seqposdelim);
				print debug "\nLine 844: dig4allele = $dig4allele\ttempposits for fullseqpos = $fullseqpos are: @tempposits";
				my $temppositct;
				#foreach $temppositct ($signalseqst..$fullprotlen) 
				#foreach $temppositct ($signalseqst..@tempposits) 
				foreach $temppositct (1..@tempposits) 
					{
						my $tempposit = $tempposits[$temppositct-1];
						if ($temppositct >= $matprotseqst) 
							{
								$matseqpos = $matseqpos.$seqposdelim.$tempposit;
							}
						else
							{
								$leaderseqpos = $leaderseqpos.$seqposdelim.$tempposit;
							}

					}
				$matseqpos =~ s/^($seqposmetadelim)//;
				$leaderseqpos =~ s/^($seqposmetadelim)//;
				
			}
		print debug "\nLine 898: dig4allele = $dig4allele\tleaderseqpos = $leaderseqpos\tmatseqpos = $matseqpos\tfullseqpos = $fullseqpos";
		print debug "\nLine 899: dig4allele = $dig4allele\ttempposits for fullseqpos = $fullseqpos are: @tempposits";
		
		$fin4digAlleleSeqs{$dig4allele} = join "",@prevallelesresds;
		#$fin4digfullmotifSeqs{$dig4allele} = 
		$insdelflag = 0;
		$changeflag = 0;
		$currresdct = 0;
		$currvardefresd = "";
		my $currmotifresd = "";
		#my $temp
		foreach $currvardefresd (@prevallelesvardefresds) 
			{
				$currmotifresd = $prevallelesresds[$currresdct];
				my $refmotifresd = $refalleleresds[$currresdct];

				if ($refmotifresd =~ /^([A-Z\*])$/i)
					{
						$insdelflag = 0;
						#print debug "\nInside the concetenation of vardef and motif seqs - Inside insdelflag = 0 for $currvardefresd:\tmatched = $1";
					}
				
				if ($insdelflag == 1) 
					{
						if ($currresdct >= $alignsignallen) 
							{
#								$fin4digmatprotvardefs{$dig4allele} = $fin4digmatprotvardefs{$dig4allele}.$currvardefresd;
#								$fin4digmatmotifSeqs{$dig4allele} = $fin4digmatmotifSeqs{$dig4allele}.$currmotifresd;

								$fin4digAllelevardefSeqs->{$matseqpos}{$dig4allele} = $fin4digAllelevardefSeqs->{$matseqpos}{$dig4allele}.$currvardefresd;
								$fin4digAllelemotifSeqs->{$matseqpos}{$dig4allele} = $fin4digAllelemotifSeqs->{$matseqpos}{$dig4allele}.$currmotifresd;
							}
						else
							{
#								$fin4digleadvardefseqs{$dig4allele} = $fin4digleadvardefseqs{$dig4allele}.$currvardefresd;
#								$fin4digleadmotifseqs{$dig4allele} = $fin4digleadmotifseqs{$dig4allele}.$currmotifresd; 

								$fin4digAllelevardefSeqs->{$leaderseqpos}{$dig4allele} = $fin4digAllelevardefSeqs->{$leaderseqpos}{$dig4allele}.$currvardefresd;
								$fin4digAllelemotifSeqs->{$leaderseqpos}{$dig4allele} = $fin4digAllelemotifSeqs->{$leaderseqpos}{$dig4allele}.$currmotifresd; 
							}
						
#						$fin4digfullprotvardefs{$dig4allele} = $fin4digfullprotvardefs{$dig4allele}.$currvardefresd;
#						$fin4digfullmotifSeqs{$dig4allele} = $fin4digfullmotifSeqs{$dig4allele}.$currmotifresd; 

						$fin4digAllelevardefSeqs->{$fullseqpos}{$dig4allele} = $fin4digAllelevardefSeqs->{$fullseqpos}{$dig4allele}.$currvardefresd;
						$fin4digAllelemotifSeqs->{$fullseqpos}{$dig4allele} = $fin4digAllelemotifSeqs->{$fullseqpos}{$dig4allele}.$currmotifresd; 
					}
				else
					{
						if ($currresdct >= $alignsignallen) 
							{
#								$fin4digmatprotvardefs{$dig4allele} = $fin4digmatprotvardefs{$dig4allele}.$vardefdelim.$currvardefresd;
#								$fin4digmatmotifSeqs{$dig4allele} = $fin4digmatmotifSeqs{$dig4allele}.$vardefdelim.$currmotifresd;
								#my $tempaminoseq = quotemeta $tempaminoseq;
								#$currvardefresd =~ /^(-?[0-9]+)((.*)$tempaminoseq)$/i;
								#$matseqpos = $matseqpos.($seqposdelim).($1).($3);
								$fin4digAllelevardefSeqs->{$matseqpos}{$dig4allele} = $fin4digAllelevardefSeqs->{$matseqpos}{$dig4allele}.$vardefdelim.$currvardefresd;
								$fin4digAllelemotifSeqs->{$matseqpos}{$dig4allele} = $fin4digAllelemotifSeqs->{$matseqpos}{$dig4allele}.$vardefdelim.$currmotifresd;
							}
						else
							{
#								$fin4digleadvardefseqs{$dig4allele} = $fin4digleadvardefseqs{$dig4allele}.$vardefdelim.$currvardefresd;
#								$fin4digleadmotifseqs{$dig4allele} = $fin4digleadmotifseqs{$dig4allele}.$vardefdelim.$currmotifresd;

								$fin4digAllelevardefSeqs->{$leaderseqpos}{$dig4allele} = $fin4digAllelevardefSeqs->{$leaderseqpos}{$dig4allele}.$vardefdelim.$currvardefresd;
								$fin4digAllelemotifSeqs->{$leaderseqpos}{$dig4allele} = $fin4digAllelemotifSeqs->{$leaderseqpos}{$dig4allele}.$vardefdelim.$currmotifresd;
							}
						
#						$fin4digfullprotvardefs{$dig4allele} = $fin4digfullprotvardefs{$dig4allele}.$vardefdelim.$currvardefresd;
#						$fin4digfullmotifSeqs{$dig4allele} = $fin4digfullmotifSeqs{$dig4allele}.$vardefdelim.$currmotifresd;
#
						$fin4digAllelevardefSeqs->{$fullseqpos}{$dig4allele} = $fin4digAllelevardefSeqs->{$fullseqpos}{$dig4allele}.$vardefdelim.$currvardefresd;
						$fin4digAllelemotifSeqs->{$fullseqpos}{$dig4allele} = $fin4digAllelemotifSeqs->{$fullseqpos}{$dig4allele}.$vardefdelim.$currmotifresd;
					}
				
				
				if (($currvardefresd =~ /[0-9]+($insertion)/i) || ($currvardefresd =~ /[0-9]+($deletion)/i) || ($currvardefresd =~ /[0-9]+($insdel)/i))
				#if (($currmotifresd =~ /[0-9]+\./i) || ($currmotifresd =~ /[0-9]+($deletion)/i) || ($currmotifresd =~ /[0-9]+($insdel)/i))
					{
						$insdelflag = 1;
						#print debug "\nInside the concetenation of vardef and motif seqs - Inside insdelflag = 1 for $currvardefresd:\tmatched = $1";
					}													
				#elsif ($currvardefresd =~ /^[0-9]+([^($insertion)^($deletion)^($insdel)]*)$/i)
				
				$currresdct++;
			}
		
			my $currseqpos;
			foreach  $currseqpos ($fullseqpos, $leaderseqpos, $matseqpos)
				{
					$fin4digAllelevardefSeqs->{$currseqpos}{$dig4allele} =~ s/^($vardefmetadelim)//;
					$fin4digAllelemotifSeqs->{$currseqpos}{$dig4allele} =~ s/^($vardefmetadelim)//; 
				}
			
#			$fin4digmatprotvardefs{$dig4allele} =~ s/^($vardefmetadelim)//;
#			$fin4digmatmotifSeqs{$dig4allele} =~ s/^($vardefmetadelim)//;
#			
#			$fin4digleadvardefseqs{$dig4allele} =~ s/^($vardefmetadelim)//;
#			$fin4digleadmotifseqs{$dig4allele} =~ s/^($vardefmetadelim)//;
#			
#			$fin4digfullprotvardefs{$dig4allele} =~ s/^($vardefmetadelim)//;
#			$fin4digfullmotifSeqs{$dig4allele} =~ s/^($vardefmetadelim)//;
				
		#$fin4digmatprotvardefs{$dig4allele} = join "_", @prevallelesvardefresds;

		@prevalleleresds = ();
		@curralleleresds = ();
	
	}

######################   End of To get the final 4 digit allele sequence
my $tempseqpos;
my @tempfeattypeindeces = (6, 7, 6);
my @tempfeatnameindeces = (4, 5, 1);
my $tempct = 0;
foreach  $tempseqpos ($fullseqpos, $leaderseqpos, $matseqpos) 
	{
		$seqposfeattypes{$tempseqpos} = $Featuretypes[$tempfeattypeindeces[$tempct]];
		$seqposfeatnames{$tempseqpos} = $Sqftname[$tempfeatnameindeces[$tempct]];
		$tempct++;
	}


#open SqftVarntsfile, ">$tempfilename";
open SqftVarntsfile, ">$outfiles[0]";
print SqftVarntsfile "Locus Name\tHLA Protein Feature Name\tHLA Protein Feature Type\tAllele Group(s)\tMotif\tVariant Type\tFeature Location - Amino Acid Positions\tVariant Type Definition\n";

#$tempfilename = "Genrtd_".$custom."_".$HLA_Locus."_Allele_Variants";
#open AlleleVarntsfile, ">$tempfilename";
open AlleleVarntsfile, ">$outfiles[1]";
print AlleleVarntsfile "HLA Allele Name";

#open MatprotAlleleVarntsfile, ">$outfiles[6]";
#print MatprotAlleleVarntsfile "HLA Allele Name";

#$tempfilename = "Genrtd_".$custom."_".$HLA_Locus."_Unknown and Non Variants";
#open Unk_NonVarntsfile, ">$tempfilename";
open Unk_NonVarntsfile, ">$outfiles[2]";
print Unk_NonVarntsfile "Locus Name\tAmino Acid Position\tAmino Acid Residue\tAllele Group\n";

open IndivSeqfeat, ">$outfiles[3]";
print IndivSeqfeat "HLA Protein Feature Name\tHLA Protein Feature Type\tFeature Location - Amino Acid Positions\tComments\tReferences\n";
#print IndivSeqfeat "\nAllele\tStructural - complete protein\t\t\t";
print IndivSeqfeat "\n$Sqftname[0]\t$Featuretypes[6]\t\t\t";
#print IndivSeqfeat "\n$Sqftname[1]\t$Featuretypes[6]\t\t\t";

my $i = 0;

my @SeqFeathdrs;
#my $SeqFeathdr = <AlleleSeqFeats>;
#$SeqFeathdrs[$i] = <AlleleSeqFeats>;
#$i++;
#$SeqFeathdrs[$i] = <AlleleSeqFeats>;

#print SqftVarntsfile "$SeqFeathdr\n";

my $AlleleVarntshdr;

my $refcharct = 0;
#my $temprefallele = $HLA_Locus."*".$ref4digallele;
#my $temprefallele = $Ref_HLA_Locus."*".$ref4digallele;
my $temprefallele = $sfvtRef_HLA_Locus."*".$sfvtref4digallele;
#my $tempref4digallele = $sfvtref4digallele;

#my $refallelefullseq = $fin4digfullmotifSeqs{$temprefallele};
#my $refallelefullvardefseq = $fin4digfullprotvardefs{$temprefallele};

my $refallelefullseq = $fin4digAllelemotifSeqs->{$fullseqpos}{$temprefallele};
my $refallelefullvardefseq = $fin4digAllelevardefSeqs->{$fullseqpos}{$temprefallele};

#my $matseqpos;
#my $fullseqpos;
#my $leaderseqpos;
#$refseqnumb[$currpos];
#my @refallelevardefaminos = split("_",$refallelematvardefseq);
#my @refalleleseqaminos = split ("_", $refallelematseq);
my @refallelevardefaminos = split("_",$refallelefullvardefseq);
my @refalleleseqaminos = split ("_", $refallelefullseq);

$currvardefresd = "";

foreach $currvardefresd (@refallelevardefaminos)
	{
		#$currvardefresd =~ /([0-9]+($insertion)?($deletion)?($insdel)?)(.*)/i;
		#$refalleleaminos[$refcharct] = $5;
		#$refalleleaminos[$1] = $5;
		my $tempaminoseq = $refalleleseqaminos[$refcharct];
		#$tempaminoseq =~ s/\*/\\\*/gi;
		$tempaminoseq = quotemeta $tempaminoseq;
		#$currvardefresd =~ /^([0-9]+)((.*)$tempaminoseq)$/i;
		$currvardefresd =~ /^(-?[0-9]+)((.*)$tempaminoseq)$/i;
		#$currvardefresd =~ /([0-9]+)(.*)/i;
		$refalleleaminos[$refcharct] = $2;
		
#		if ($1 > 0) 
#			{
#				$matseqpos = $matseqpos.($seqposdelim).($1).($3);
#				$fin4digmatmotifSeqs{$temprefallele}
#				$fin4digmatprotvardefs{$temprefallele};
#			}
#		else
#			{
#				$leaderseqpos = $leaderseqpos.($seqposdelim).($1).($3);
#			}
		#$matseqpos = $matseqpos.($seqposdelim).($1).($3);
#		$fullseqpos = $fullseqpos.($seqposdelim).($1).($3);
		
		#print debug "\nLine 724: currvardefresd = $currvardefresd\trefcharct = $refcharct\ttempaminoseq = $tempaminoseq\trefalleleseqaminos = $refalleleseqaminos[$refcharct]\trefseqnumber = $refseqnumb[$refcharct]\tmatseqpos = $matseqpos\t1 = $1\t2 = $2\t3 = $3";
		$refcharct++;
	}
#my $refallelematseq = $fin4digmatmotifSeqs{$temprefallele};
#my $refallelematvardefseq = $fin4digmatprotvardefs{$temprefallele};

my $refallelematseq = $fin4digAllelemotifSeqs->{$matseqpos}{$temprefallele};
my $refallelematvardefseq = $fin4digAllelevardefSeqs->{$matseqpos}{$temprefallele};

#my $align
#$tempmetastr = quotemeta $seqposdelim;

#$matseqpos =~ s/^($seqposmetadelim)//i;
my $tempdelref = \@delposits;
#my @temparray = Formatseqpos ($seqposdelim, $possplitthresh, $seqposfrmtdelim, @refseqnumb, @delposits);
my @temparray = Formatseqpos ($seqposdelim, $possplitthresh, $seqposfrmtdelim, $tempdelref, @refseqnumb);
my $frmtmatseqpos = $temparray[0];
print debug "\nLine 1080: refseqnumb = @refseqnumb\ttemparray = @temparray\tmatseqpos = $matseqpos\tleaderseqpos = $leaderseqpos";

$refcharct = 1;
#foreach $refcharct (1..@refalleleaminos)
#$Alleles_Posamino->{$ref4digallele} = \@refalleleseqaminos;
$Alleles_Posamino->{$temprefallele} = \@refalleleseqaminos;

my @seqpos = ($fullseqpos, $leaderseqpos, $matseqpos, (1..@refalleleseqaminos));
my $seqposct = 1;
#foreach $refcharct ($fullseqpos, $leaderseqpos, $matseqpos, (1..@refalleleseqaminos)) 
foreach $refcharct (@seqpos) 
	{
		my $vardefseq;
		my $amino;
		
		#if ($refcharct =~ /$matseqpos/i)
		if ($seqposct < 4)
			{
#				$vardefseq = $refallelematvardefseq;
#				$amino = $fin4digAlleleSeqs{$temprefallele};
				
				$vardefseq = $fin4digAllelevardefSeqs->{$refcharct}{$temprefallele};
				$amino = $fin4digAllelemotifSeqs->{$refcharct}{$temprefallele};
				#print debug "\nLine 1135: refcharct = $refcharct\tvardefseq = $vardefseq\tamino = $amino";
			}
		else
			{
				$vardefseq = $refallelevardefaminos[$refcharct-1];
				$amino = $refalleleseqaminos[$refcharct-1];
				#print debug "\nLine 1141: refcharct = $refcharct\tvardefseq = $vardefseq\tamino = $amino";
			}

		
		#if ((!($amino =~ /^(\*)$/i)) && (!($amino =~ /^( *)$/i))) 
		#if ($amino =~ /^([A-Z\.]+)$/i) 
		if ($amino =~ /^([_A-Z\.]+)$/i) 
			{
#				$Alleles_CustSqftType->{$refcharct}{$fullref4digallele}{$amino} = $reftype;
				$Alleles_CustSqftType->{$refcharct}{$temprefallele}{$amino} = $reftype;
				$Alleles_PosTypes->{$refcharct}{$amino} = $reftype;
				$Motif_CustSqftType->{$refcharct}->{$reftype} = $amino;
				#$Motif_CustSqftType->{$refcharct}->{$reftype} = $motifseq;
				$Vardef_CustSqftType->{$refcharct}->{$reftype} = $vardefseq;
				#$Alleles_CustSqft->{$refcharct}{$amino} = $Alleles_CustSqft->{$refcharct}{$amino}.", ".$HLA_Locus."*".$ref4digallele;
#				$Alleles_CustSqft->{$refcharct}{$amino} = $Alleles_CustSqft->{$refcharct}{$amino}.$allelegrpdelim.$Ref_HLA_Locus."*".$ref4digallele;
				if ($HLA_Locus =~ /^ *($sfvtRef_HLA_Locus) *$/i)
					{
						$Alleles_CustSqft->{$refcharct}{$amino} = $Alleles_CustSqft->{$refcharct}{$amino}.$allelegrpdelim.$sfvtRef_HLA_Locus."*".$sfvtref4digallele;
					}
				else
					{
						$Alleles_CustSqft->{$refcharct}{$amino} = $Alleles_CustSqft->{$refcharct}{$amino}.$allelegrpdelim.$sfvtRef_HLA_Locus."*".$sfvtref4digallele;
#						$Alleles_CustSqft->{$refcharct}{$amino} = "";
					}
			}
		else
			{
				#$Alleles_CustUnkSqft->{$refcharct} = $Alleles_CustUnkSqft->{$refcharct}.", ".$HLA_Locus."*".$ref4digallele;
#				$Alleles_CustUnkSqft->{$refcharct} = $Alleles_CustUnkSqft->{$refcharct}.$allelegrpdelim.$Ref_HLA_Locus."*".$ref4digallele;
				$Alleles_CustUnkSqft->{$refcharct} = $Alleles_CustUnkSqft->{$refcharct}.$allelegrpdelim.$sfvtRef_HLA_Locus."*".$sfvtref4digallele if ($sfvtHLA_Locus =~ /^ *($sfvtRef_HLA_Locus) *$/i);
				#$Alleles_CustSqftType->{$Allele}{$charct}{Unknown} = "Unknown Variant Type";
#				$Alleles_CustSqftType->{$refcharct}{$fullref4digallele}{Unknown} = "Unknown Variant Type";
				$Alleles_CustSqftType->{$refcharct}{$temprefallele}{Unknown} = "Unknown Variant Type";
			}
		$seqposct++;
	}

open MatProt_vardefseqs, ">$outfiles[6]";
print MatProt_vardefseqs "HLA Locus\tDelimited Mature protein Sequence\tVariant Definition Sequence\n";

#while (<AlleleSeqFeats>) 
foreach $dig4allele (sort keys %fin4digAlleleSeqs)
	{
		
		#print SqftVarntsfile "\n$Locus\t$Sqftname[0]\t$Featuretypes[6]\t$dig4allele\t\t$dig4allele\t\t" if (!($dig4allele=~ /($Ref_HLA_Locus\*($ref4digallele))$/i));
#		print SqftVarntsfile "\n$Locus\t$Sqftname[0]\t$Featuretypes[6]\t$dig4allele\t\t$dig4allele\t\t";
		#if ($dig4allele =~ /^($temprefallele)$/i) 
		#chomp;
		#my $Seqfeatline = $_;
		#my $Seqfeatline = $dig4allele."\t".$fin4digfullmotifSeqs{$dig4allele}."\t".$fin4digmatprotvardefs{$dig4allele};
		#my $Seqfeatline = $dig4allele."\t".$fin4digfullmotifSeqs{$dig4allele}."\t".$fin4digfullprotvardefs{$dig4allele};
		my $Seqfeatline = $dig4allele."\t".$fin4digAllelemotifSeqs->{$fullseqpos}{$dig4allele}."\t".$fin4digAllelevardefSeqs->{$fullseqpos}{$dig4allele};
		#my $Seqfeatline = $dig4allele."\t".$fin4digmatprotvardefs{$dig4allele};
		#my $Seqfeatline = $dig4allele."\t".$fin4digAlleleSeqs{$dig4allele};
		#print debug "\nLine 1182: Before splitrow Featlineterms = @Featlineterms";
		my @Featlineterms = Splitrow($Seqfeatline, "\t");
		#my @Featlineterms = Splitrow($Seqfeatline, "\t");
		#print debug "\nLine 1185: After splitrow Featlineterms = @Featlineterms";
		if ($dig4allele =~ /($sfvtRef_HLA_Locus\*($sfvtref4digallele))$/i) 
			{
				##print debug "\nEncountered reference allele - $dig4allele\tAllele = $Allele";
				print MatProt_vardefseqs "\n$sfvtRef_HLA_Locus*$sfvtref4digallele\t$Featlineterms[$seqlinemotifseqcol]\t$Featlineterms[$seqlinevardefseqcol]"if ($HLA_Locus =~ /^ *($sfvtRef_HLA_Locus) *$/i);
				print SqftVarntsfile "\n$Locus\t$Sqftname[0]\t$Featuretypes[6]\t$dig4allele\t\t$dig4allele\t\t" if ($HLA_Locus =~ /^ *($sfvtRef_HLA_Locus) *$/i);
				next;
			}
		if (($dig4allele =~ /($Ref_HLA_Locus\*($ref4digallele))$/i) && (!($HLA_Locus =~ /^ *($Ref_HLA_Locus) *$/i)))
			{
				##print debug "\nEncountered reference allele - $dig4allele\tAllele = $Allele";
#				print MatProt_vardefseqs "\n$Ref_HLA_Locus*$ref4digallele\t$Featlineterms[$seqlinemotifseqcol]\t$Featlineterms[$seqlinevardefseqcol]"if ($HLA_Locus =~ /^ *($Ref_HLA_Locus) *$/i);
#				print SqftVarntsfile "\n$Locus\t$Ref_HLA_Locus*$ref4digallele\t$Featuretypes[6]\t$dig4allele\t\t$dig4allele\t\t" if ($HLA_Locus =~ /^ *($Ref_HLA_Locus) *$/i);
				next;
			}
		print SqftVarntsfile "\n$Locus\t$Sqftname[0]\t$Featuretypes[6]\t$dig4allele\t\t$dig4allele\t\t";
		#if ($Featlineterms[0] ne "") 
		if ($Featlineterms[$seqlineallelecol] ne "") 
			{
				#my $Allele = $Featlineterms[0];
				my $Allele = $Featlineterms[$seqlineallelecol];
				my $tempallele = $Allele;
				$tempallele =~ s/.*$HLA_Locus\*//i;
#				$Allele =~ s/$HLA_Locus\*//;
#				my $tempallele = $HLA_Locus."*".($Allele);
				#my $AlleleMatProt = $Featlineterms[1];
				my $AlleleMotif = $Featlineterms[$seqlinemotifseqcol];
				my $Allelevardef = 	$Featlineterms[$seqlinevardefseqcol];
				#my $Alleles_PosTypes;
				#print debug "\nCurrallele = $Allele\tAlleleMatprot = $AlleleMotif\tAllelevardef = $Allelevardef";
#				print MatProt_vardefseqs "\n$HLA_Locus*$Allele\t$AlleleMotif\t$Allelevardef";
				print MatProt_vardefseqs "\n",$HLA_Locus,"*",$tempallele,"\t$AlleleMotif\t$Allelevardef";
				#my @seqaminos = split("",$AlleleMotif);
				my @seqaminos = split("_",$AlleleMotif);
				#my @seqaminos = split /$vardefdelim/, $AlleleMotif;
				#my @vardefseqaminos = split /$vardefdelim/, $Allelevardef;
				my @vardefseqaminos = split("_",$Allelevardef);
				 
				
				$currvardefresd = "";
				#$allelevardefcharct = 1;

				#foreach $currvardefresd (@vardefseqaminos) 
				#	{
						#$currvardefresd =~ /([0-9]+($insertion)?($deletion)?($insdel)?)(.*)/i;
						#$refalleleaminos[$refcharct] = $5;
						#$refalleleaminos[$1] = $5;
				#		$currvardefresd =~ /([0-9]+)(.*)/i;
				#		$seqaminos[$allelevardefcharct] = $2;
				#		
				#		$allelevardefcharct++;
				#	}
				
				$Alleles_Posamino->{$Allele} = \@seqaminos;
				#print debug "\nInside the featureline if block: \tThe address for Allele - DRB1*$Allele positions = \t",$Alleles_Posamino->{$Allele};
				my @tempalleleaminos = @{$Alleles_Posamino->{$Allele}};
				#print debug "\nAllele - $Allele\t",$Alleles_Posamino->{$Allele},"\tPositions: ",$Alleles_Posamino->{$Allele}[7],":: ",$Alleles_Posamino->{$Allele}[7],"::: ",${$Alleles_Posamino->{$Allele}}[7],":::: ",$Alleles_Posamino->{$Allele}[7]," ::::: ",@tempalleleaminos," all aminos- ",(@tempalleleaminos);
				#my @seqposaminos = (@seqaminos,$fin4digfullmotifSeqs{$fullseqpos}{$tempallele},$fin4digfullmotifSeqs{$leaderseqpos}{$tempallele},$fin4digfullmotifSeqs{$matseqpos}{$tempallele});
				my @seqpos2 = ((1..@seqaminos), $fullseqpos, $leaderseqpos, $matseqpos);
#				my @seqposaminos = (@seqaminos, $fin4digAllelemotifSeqs->{$fullseqpos}{$tempallele}, $fin4digAllelemotifSeqs->{$leaderseqpos}{$tempallele}, $fin4digAllelemotifSeqs->{$matseqpos}{$tempallele});
				my @seqposaminos = (@seqaminos, $fin4digAllelemotifSeqs->{$fullseqpos}{$Allele}, $fin4digAllelemotifSeqs->{$leaderseqpos}{$Allele}, $fin4digAllelemotifSeqs->{$matseqpos}{$Allele});
				#print debug "\nLine 1234: Seqposaminos = @seqposaminos";
					
				my $charct = 0;
				my $Typect = 0;
				$seqposct = 1;
				#my $alleleamino = $AlleleMotif;
				#$alleleamino =~ s/\*/0/gi;
				#$alleleamino =~ s/\./1/gi;
				#foreach $amino (@seqaminos,$fin4digAlleleSeqs{$tempallele})
				foreach $amino (@seqposaminos)
				#foreach $amino (@seqaminos)
					{
						my $vardefseq;
						
						if ($seqposct > @seqaminos)
							{
								$charct = $seqpos2[$seqposct-1];
#								$vardefseq = $fin4digAllelevardefSeqs->{$charct}{$tempallele};
								$vardefseq = $fin4digAllelevardefSeqs->{$charct}{$Allele};
							}
#						if ($amino eq $fin4digAlleleSeqs{$tempallele}) 
#							{
#								$charct = $matseqpos;
#								$vardefseq = $Allelevardef;
#							}
						else
							{
								$charct++;
								$vardefseq = $vardefseqaminos[$charct-1];								
							}

						if ($amino =~ /^([_A-Z\.]+)$/i)
						#if ($amino =~ /^([A-Z]+)$/i)
						#if ($amino =~ /[A-Z]/i)
							{
								#$tempcharct = $charct-1;
								$Typect = keys %{$Motif_CustSqftType->{$charct}};
								#print debug "\nThe Allele-$Allele\tAmino - $amino\tTypect - $Typect";
								#print debug "\n Before exist for amino alleles are - \t",$Alleles_CustSqft->{$charct}{$amino};

								if (!(exists $Alleles_CustSqft->{$charct}{$amino})) 
									{
										#$Alleles_CustSqftType->{$Allele}{$charct}{$amino} = $Typect+1;
										$Alleles_CustSqftType->{$charct}{$Allele}{$amino} = $Typect+1;
										$Alleles_PosTypes->{$charct}{$amino} = $Typect+1;
										$Vardef_CustSqftType->{$charct}->{$Alleles_PosTypes->{$charct}{$amino}} = $vardefseq; 
										$Motif_CustSqftType->{$charct}->{$Alleles_PosTypes->{$charct}{$amino}} = $amino;
										#print debug "\n In exist for amino, the \ttype is - $Alleles_CustSqftType->{$charct}{$Allele}{$amino}";
									}
								else 
									{
										$Alleles_CustSqftType->{$charct}{$Allele}{$amino} = $Alleles_PosTypes->{$charct}{$amino};
									}
								
								#$Alleles_CustSqft->{$charct}{$amino} = $Alleles_CustSqft->{$charct}{$amino}.", ".$HLA_Locus."*".$Allele;
#								$Alleles_CustSqft->{$charct}{$amino} = $Alleles_CustSqft->{$charct}{$amino}.$allelegrpdelim.$HLA_Locus."*".$Allele;
								$Alleles_CustSqft->{$charct}{$amino} = $Alleles_CustSqft->{$charct}{$amino}.$allelegrpdelim.$HLA_Locus."*".$tempallele;
								#print debug "\nOutside Exist the Alleles are: \t$Alleles_CustSqft->{$charct}{$amino}";

								#$Motif_CustSqftType->{$charct}{$Alleles_CustSqftType->{$charct}{$Allele}{$amino}} = $amino;
								#print debug "\nOutside Exist the charct- $charct Type- ",$Alleles_CustSqftType->{$charct}{$Allele}{$amino}," Allele- $Allele amino- $amino Motif is: \t",$Motif_CustSqftType->{$charct}{$Alleles_CustSqftType->{$charct}{$Allele}{$amino}};
							}
						else
						{
							#$Alleles_CustUnkSqft->{$charct} = $Alleles_CustUnkSqft->{$charct}.", ".$HLA_Locus."*".$Allele;
#							$Alleles_CustUnkSqft->{$charct} = $Alleles_CustUnkSqft->{$charct}.$allelegrpdelim.$HLA_Locus."*".$Allele;
							$Alleles_CustUnkSqft->{$charct} = $Alleles_CustUnkSqft->{$charct}.$allelegrpdelim.$HLA_Locus."*".$tempallele;
							#$Alleles_CustSqftType->{$Allele}{$charct}{Unknown} = "Unknown Variant Type";
							$Alleles_CustSqftType->{$charct}{$Allele}{Unknown} = "Unknown Variant Type";
						} 
					#$Alleles_Posamino->{$Allele}{$charct} = $amino;	
						#$charct++;
						$seqposct++;
					}
			}
	}

$charct = @customPosits;
my $pos = 1;

#$AlleleVarntshdr = $AlleleVarntshdr."\t$Sqftname[4]-Motif\t$Sqftname[4]-Variant Type\t$Sqftname[4]-Variant Type Definition\t$Sqftname[1]-Motif\t$Sqftname[1]-Variant Type\t$Sqftname[1]-Variant Type Definition";
$AlleleVarntshdr = $AlleleVarntshdr."\t$Sqftname[4]-Motif\t$Sqftname[4]-Variant Type\t$Sqftname[4]-Variant Type Definition\t$Sqftname[5]-Motif\t$Sqftname[5]-Variant Type\t$Sqftname[5]-Variant Type Definition\t$Sqftname[1]-Motif\t$Sqftname[1]-Variant Type\t$Sqftname[1]-Variant Type Definition";

#foreach $pos (@customPosits) 
#foreach $pos ($matseqpos,@customPosits)
$seqposct = 1;
foreach $pos (@seqpos)
	{
		my $refseqnumber;
		my $seqfeattype;
		my $seqfeatname;
		my $frmtpos;
		
#		if ($pos =~ /$matseqpos/i)
		if ($seqposct < 4)
			{
				my @seqposits = Splitrow ($pos, $seqposdelim);
				
				#my @temparray = Formatseqpos ($seqposdelim, $possplitthresh, $seqposfrmtdelim, @seqposits, @delposits);
				my @temparray = Formatseqpos ($seqposdelim, $possplitthresh, $seqposfrmtdelim, $tempdelref, @seqposits);
				$frmtpos = $temparray[0];
				$refseqnumber = "";
				$seqfeattype = $seqposfeattypes{$pos};
				$seqfeatname = $seqposfeatnames{$pos};
			}
		elsif ($refseqnumb[$pos-1] =~ /^[0-9]+$deletion$/i) 
			{
				$frmtpos = $refseqnumb[$pos-1];
				$refseqnumber = " ".($refseqnumb[$pos-1]);
				$seqfeattype = $Featuretypes[11];
				$seqfeatname = $Sqftname[3];
			}
		else
			{
				$frmtpos = $refseqnumb[$pos-1];
				$refseqnumber = " ".($refseqnumb[$pos-1]);
				$seqfeattype = $Featuretypes[10];
				$seqfeatname = $Sqftname[2];
			}
			
		#print debug "\nLine 1354: pos = $pos\tseqposct = $seqposct\tfrmtpos = $frmtpos\trefseqnumber = $refseqnumber";
		
		if (keys %{$Alleles_CustSqft->{$pos}} == 1) 
			{
				#print debug "\n In print Non Variant seqfeat Pos $pos \tkeys = ",keys %{$Alleles_CustSqft->{$pos}};
				my $tempresidue = $Motif_CustSqftType->{$pos}->{1};
				my $tempmotifseq = $tempresidue;
				$tempmotifseq =~ s/$motifdelim//gi;
				my $tempvardefresidue = $Vardef_CustSqftType->{$pos}->{1};
				#my $tempresidue = $Motif_CustSqftType->{$pos}->{1};
				#print debug "\n In print Non Variant seqfeat Pos $pos \ttempresidue = $tempresidue";
				#$tempmetastr = quotemeta $allelegrpdelim;
				$Alleles_CustSqft->{$pos}{$tempresidue} =~ s/^$allelegrpmetadelim//;
				print debug "\nLine 1365: pos = $pos\tseqposct = $seqposct\tfrmtpos = $frmtpos\trefseqnumber = $refseqnumber\ttempresidue = $tempresidue\ttempvardefresidue = $tempvardefresidue";
				print Unk_NonVarntsfile "\n$Locus\t",$frmtpos,"\t$tempmotifseq\t$Alleles_CustSqft->{$pos}{$tempresidue}";
				if ($seqposct < 4)
					{
						print SqftVarntsfile "\nHLA-$HLA_Locus\t$seqfeatname",$refseqnumber,"\t$seqfeattype\t$Alleles_CustSqft->{$pos}{$tempresidue}\t$tempmotifseq\tType ",$Alleles_PosTypes->{$pos}{$tempresidue},"\t",$frmtpos,"\t$tempvardefresidue";
						print IndivSeqfeat "\n$seqfeatname",$refseqnumber,"\t$seqfeattype\t",$frmtpos,"\t\t";
					}
				
				#print Unk_NonVarntsfile "\nHLA-$HLA_Locus\t$pos\t$tempresidue\t$Alleles_CustSqft->{$pos}{$tempresidue}";
				#if ($tempresidue eq ".") 
				#	{
				#		print Unk_NonVarntsfile "\nHLA-$HLA_Locus\t$refseqnumb[$pos-1]\t$deletion\t$Alleles_CustSqft->{$pos}{$tempresidue}";		
				#	}
				#else
				#	{
						#print Unk_NonVarntsfile "\nHLA-$HLA_Locus\t$refseqnumb[$pos-1]\t$tempresidue\t$Alleles_CustSqft->{$pos}{$tempresidue}";
				#	}
				#print Unk_NonVarntsfile "\nHLA-$HLA_Locus\t$pos\t$tempresidue\t$Alleles_CustSqft->{$pos}{$tempresidue}";
				#print debug "\n In print Non Variant seqfeat Pos $pos \talleles are = \t",$Alleles_CustSqft->{$pos}{$tempresidue};
			}
		if (keys %{$Alleles_CustSqft->{$pos}} > 1)
			{
				#print IndivSeqfeat "\nmature protein position $pos\tStructural - variant location\t$pos\t\t";
				print IndivSeqfeat "\n$seqfeatname",$refseqnumber,"\t$seqfeattype\t",$frmtpos,"\t\t";
				#if (!($pos =~ /$matseqpos/i))
				if ($seqposct >= 4)
					
					{
						#print IndivSeqfeat "\n$seqfeatname",$refseqnumber,"\t$seqfeattype\t",$frmtpos,"\t\t";
						$AlleleVarntshdr = $AlleleVarntshdr."\t$seqfeatname".$refseqnumber."-Motif\t$seqfeatname".$refseqnumber."-Variant Type\t$seqfeatname".$refseqnumber."-Variant Type Definition";
					}
				#$refseqnumb[$pos-1]
				foreach $aminoct (1..keys %{$Alleles_CustSqft->{$pos}}) 
					{
						#print debug "\n In print Seqfeat Pos $pos Aminoct $aminoct\tkeys = ",keys %{$Alleles_CustSqft->{$pos}};
						my $tempresidue = $Motif_CustSqftType->{$pos}->{$aminoct};
						my $tempmotifseq = $tempresidue;
						$tempmotifseq =~ s/$motifdelim//gi;
						my $refresidue = $Motif_CustSqftType->{$pos}->{$reftype};
						my $tempvardefresidue = $Vardef_CustSqftType->{$pos}->{$aminoct};
						#print debug "\nLine 1396: pos = $pos\tseqposct = $seqposct\tfrmtpos = $frmtpos\trefseqnumber = $refseqnumber\ttempresidue = $tempresidue\ttempvardefresidue = $tempvardefresidue";
						#print debug "\n In print Seqfeat Pos $pos Aminoct $aminoct\ttempresidue = $tempresidue";
						#$Alleles_CustSqft->{$pos}{$tempresidue} =~ s/^, //;
						#$tempmetastr = quotemeta $allelegrpdelim;
						$Alleles_CustSqft->{$pos}{$tempresidue} =~ s/^$allelegrpmetadelim//;
				
						#print SqftVarntsfile "\nHLA-$HLA_Locus\t$Sqftname $pos\tStructural - variant location\t$Alleles_CustSqft->{$pos}{$tempresidue}\t$tempresidue\tType ",$aminoct,"\t$pos\t$pos$tempresidue";
						
						#if ($tempresidue =~ /\./i)
						if ($refresidue =~ /\./i) 
						#if ($tempresidue eq ".") 
							{
								#print SqftVarntsfile "\nHLA-$HLA_Locus\t$Sqftname[2] $refseqnumb[$pos-1]\tStructural - variant location\t$Alleles_CustSqft->{$pos}{$tempresidue}\t$deletion\t$Seqfeatname",$Alleles_PosTypes->{$pos}{$tempresidue},"\t$refseqnumb[$pos-1]\t$refseqnumb[$pos-1]$deletion";
								print SqftVarntsfile "\nHLA-$HLA_Locus\t$seqfeatname",$refseqnumber,"\t$seqfeattype\t$Alleles_CustSqft->{$pos}{$tempresidue}\t$tempmotifseq\tType ",$Alleles_PosTypes->{$pos}{$tempresidue},"\t",$frmtpos,"\t$tempvardefresidue";
								#print SqftVarntsfile "\nHLA-$HLA_Locus\t$Sqftname[2] $refseqnumb[$pos-1]\tStructural - variant location\t$Alleles_CustSqft->{$pos}{$tempresidue}\t$deletion\tType ",$Alleles_PosTypes->{$pos}{$tempresidue},"\t$refseqnumb[$pos-1]\t$refseqnumb[$pos-1]$deletion";
							}
						elsif ($tempresidue =~ /\./i)
						#elsif (($refresidue =~ /\./i) && (!($tempresidue =~ /\./i))) 
						#elsif (($refresidue eq ".") && ($tempresidue ne ".")) 
							{
								#print SqftVarntsfile "\nHLA-$HLA_Locus\t$Sqftname[2] $refseqnumb[$pos-1]\tStructural - variant location\t$Alleles_CustSqft->{$pos}{$tempresidue}\t$tempresidue\t$Seqfeatname",$Alleles_PosTypes->{$pos}{$tempresidue},"\t$refseqnumb[$pos-1]\t$refseqnumb[$pos-1]$insertion$tempresidue";
								print SqftVarntsfile "\nHLA-$HLA_Locus\t$seqfeatname",$refseqnumber,"\t$seqfeattype\t$Alleles_CustSqft->{$pos}{$tempresidue}\t$tempmotifseq\tType ",$Alleles_PosTypes->{$pos}{$tempresidue},"\t",$frmtpos,"\t$tempvardefresidue";
								#print SqftVarntsfile "\nHLA-$HLA_Locus\t$Sqftname[2] $refseqnumb[$pos-1]\tStructural - variant location\t$Alleles_CustSqft->{$pos}{$tempresidue}\t$tempresidue\tType ",$Alleles_PosTypes->{$pos}{$tempresidue},"\t$refseqnumb[$pos-1]\t$refseqnumb[$pos-1]$insertion$tempresidue";
							}
						else
							{
								#print SqftVarntsfile "\nHLA-$HLA_Locus\t$Sqftname[2] $refseqnumb[$pos-1]\tStructural - variant location\t$Alleles_CustSqft->{$pos}{$tempresidue}\t$tempresidue\t$Seqfeatname",$Alleles_PosTypes->{$pos}{$tempresidue},"\t$refseqnumb[$pos-1]\t$refseqnumb[$pos-1]$tempresidue";
								print SqftVarntsfile "\nHLA-$HLA_Locus\t$seqfeatname",$refseqnumber,"\t$seqfeattype\t$Alleles_CustSqft->{$pos}{$tempresidue}\t$tempmotifseq\tType ",$Alleles_PosTypes->{$pos}{$tempresidue},"\t",$frmtpos,"\t$tempvardefresidue";
								#print SqftVarntsfile "\nHLA-$HLA_Locus\t$Sqftname[2] $refseqnumb[$pos-1]\tStructural - variant location\t$Alleles_CustSqft->{$pos}{$tempresidue}\t$tempresidue\tType ",$Alleles_PosTypes->{$pos}{$tempresidue},"\t$refseqnumb[$pos-1]\t$refseqnumb[$pos-1]$tempresidue";
							}
						#print debug "\n In print Seqfeat Pos $pos \talleles are = \t",$Alleles_CustSqft->{$pos}{$tempresidue};
					}
				#$AlleleVarntshdr = $AlleleVarntshdr."\t$Sqftname $pos-Motif\t$Sqftname $pos-Variant Type\t$Sqftname $pos-Variant Type Definition";
					
			}
		
		if (exists $Alleles_CustUnkSqft->{$pos})
			{
				$Alleles_CustUnkSqft->{$pos} =~ s/^, //;
				#$tempmetastr = quotemeta $allelegrpdelim;
				$Alleles_CustUnkSqft->{$pos} =~ s/^$allelegrpmetadelim//;
				
				if (keys %{$Alleles_CustSqft->{$pos}} > 1) 
					{	
						#print SqftVarntsfile "\nHLA-$HLA_Locus\t$Sqftname $pos\tStructural - variant location\t$Alleles_CustUnkSqft->{$pos}\t*\tUnknown Motif Type\t$pos\tUnknown Variant Type";
						print SqftVarntsfile "\n$Locus\t$seqfeatname",$refseqnumber,"\t$seqfeattype\t$Alleles_CustUnkSqft->{$pos}\t*\tUnknown Motif Type\t",$frmtpos,"\tUnknown Variant Type";
					}
				elsif ((keys %{$Alleles_CustSqft->{$pos}} == 1) && ($seqposct < 4))
					{
						print SqftVarntsfile "\n$Locus\t$seqfeatname",$refseqnumber,"\t$seqfeattype\t$Alleles_CustUnkSqft->{$pos}\t*\tUnknown Motif Type\t",$frmtpos,"\tUnknown Variant Type";
					}
				#print Unk_NonVarntsfile "\nHLA-$HLA_Locus\t$pos\t*\t",$Alleles_CustUnkSqft->{$pos};
				print Unk_NonVarntsfile "\n$Locus\t",$frmtpos,"\t*\t",$Alleles_CustUnkSqft->{$pos};
			}
		$seqposct++;
	}

print AlleleVarntsfile "$AlleleVarntshdr\n";

#print debug "\n\nThe between Seqfeat and Allele print the AllelecustSqfttype - Type $Alleles_CustSqftType->{9}{0301}{E}";
my $temptypect = 0;


#foreach $Allele (sort keys %{$Alleles_CustSqftType->{1}})
foreach $dig4allele (@dig4Alleles)
#foreach $Allele (sort keys %{$Alleles_CustSqftType->{$fullseqpos}}) 
	{
		my $tempdig4allele;
		$tempdig4allele = $dig4allele;
		if ($dig4allele =~ /$Ref_HLA_Locus\*($ref4digallele)$/i) 
		#if ($Allele =~ /$Ref_HLA_Locus\*($ref4digallele)$/i) 
			{
				next if (!($HLA_Locus =~ /^ *($Ref_HLA_Locus) *$/i));
				$Allele = $dig4allele;
				#$tempdig4allele = $Allele;
				print AlleleVarntsfile "\n$Allele";
				
			}
		else
			{
				$Allele = $dig4allele;
#				$Allele =~ s/^(.*\*)//i;
				#$tempdig4allele = $HLA_Locus."*".$Allele;
#				print AlleleVarntsfile "\n$HLA_Locus*$Allele";
				print AlleleVarntsfile "\n$Allele";				
			}
		#print AlleleVarntsfile "\n$HLA_Locus*$Allele\t$fin4digAlleleSeqs{$tempdig4allele}\t$tempdig4allele\t$fin4digmatprotvardefs{$tempdig4allele}";
		#print AlleleVarntsfile "\n$HLA_Locus*$Allele\$fin4digAlleleSeqs{$tempdig4allele}\t$tempdig4allele\t$fin4digmatprotvardefs{$tempdig4allele}";
		$temptypect++;
		#print SqftVarntsfile "\nHLA-$HLA_Locus\t$Sqftname[0]\tStructural - complete protein\t$HLA_Locus*$Allele\t\t$Allele\t\t";
		#print SqftVarntsfile "\n$Locus\t$Sqftname[0]\t$Featuretypes[6]\t$tempdig4allele\t\t$tempdig4allele\t\t";
		#print SqftVarntsfile "\n$Locus\t$Sqftname[1]\t$Featuretypes[4]\t$HLA_Locus*$Allele\t$fin4digAlleleSeqs{$tempdig4allele}\t$tempdig4allele\t1-",$matprotlen,"\t$fin4digmatprotvardefs{$tempdig4allele}";
		
		#print SqftVarntsfile "\nHLA-$HLA_Locus\tAllele\tStructural - complete protein\t$HLA_Locus*$Allele\t\t$Allele\t\t";
		#print SqftVarntsfile "\nHLA-$HLA_Locus\t$Sqftname[1]\tStructural - complete protein\t$HLA_Locus*$Allele\t$fin4digAlleleSeqs{$tempdig4allele}\t$tempdig4allele\t1-",$matprotlen,"\t$fin4digmatprotvardefs{$tempdig4allele}";

		#foreach $pos ($matseqpos,@customPosits)
		$seqposct = 1;
		foreach $pos (@seqpos)
			{
				if ((keys %{$Alleles_CustSqft->{$pos}} > 1) || ((keys %{$Alleles_CustSqft->{$pos}} == 1) && ($seqposct < 4)))
					{
						my $tempresidue;
						my $refresidue;

#						if ($pos =~ /$matseqpos/i)
						if ($seqposct < 4)
							{
								$tempresidue = $fin4digAllelemotifSeqs->{$pos}{$tempdig4allele};
								$refresidue = $fin4digAllelemotifSeqs->{$pos}{$fullref4digallele};
							}
						else
							{
								$tempresidue = $Alleles_Posamino->{$Allele}[$pos-1];
								$refresidue = $Alleles_Posamino->{$fullref4digallele}[$pos-1];							
							}
						
						my $tempmotifseq = $tempresidue;
						$tempmotifseq =~ s/$motifdelim//gi;

						
						my $temptype =	$Alleles_CustSqftType->{$pos}{$Allele}{$tempresidue};
						my $tempvardefresidue = $Vardef_CustSqftType->{$pos}->{$temptype};
						#my $vardefresidue =  
						#print debug "\nLine 1063: In allele print Tempresidue = ",$tempresidue,"\t\t Alternatively the Alleles posamino at Allele- $Allele and at Pos- $pos = ", ${$Alleles_Posamino->{$Allele}}[$pos];
						if ($tempresidue =~ /\*/i)
						#if ($tempresidue =~ /[\*\?]/) 
							{
								#print AlleleVarntsfile "\t$tempresidue\tUnknown Motif Type\tUnknown Variant Type";
								print AlleleVarntsfile "\t$tempmotifseq\tUnknown Motif Type\tUnknown Variant Type";
							}
						elsif ($tempresidue =~ /\./i)
						#elsif ($tempresidue eq ".")
							{
								#print AlleleVarntsfile "\t$tempresidue\tType ",$Alleles_CustSqftType->{$pos}{$Allele}{$tempresidue},"\t$pos$tempresidue";
								#print AlleleVarntsfile "\t$deletion\tType ",$Alleles_CustSqftType->{$pos}{$Allele}{$tempresidue},"\t$refseqnumb[$pos-1]$deletion";
								print AlleleVarntsfile "\t$tempmotifseq\tType ",$temptype,"\t$tempvardefresidue";
								
								#print debug "\nInside Allele Print Pos- $pos Allelel- $Allele Tempresidue- $tempresidue \t$Alleles_CustSqftType->{$pos}{$Allele}{$tempresidue} OR $Alleles_PosTypes->{$pos}{$tempresidue}";
							}
						elsif (($refresidue =~ /\./i) && (!($tempresidue =~ /\./i)))
						#elsif (($refresidue eq ".") && ($tempresidue ne "."))
							{
								#print AlleleVarntsfile "\t$tempresidue\tType ",$Alleles_CustSqftType->{$pos}{$Allele}{$tempresidue},"\t$pos$tempresidue";
								print AlleleVarntsfile "\t$tempmotifseq\tType ",$temptype,"\t$tempvardefresidue";
								#print AlleleVarntsfile "\t$tempresidue\tType ",$Alleles_CustSqftType->{$pos}{$Allele}{$tempresidue},"\t$refseqnumb[$pos-1]$insertion$tempresidue";
								#print debug "\nInside Allele Print Pos- $pos Allelel- $Allele Tempresidue- $tempresidue \t$Alleles_CustSqftType->{$pos}{$Allele}{$tempresidue} OR $Alleles_PosTypes->{$pos}{$tempresidue}";
							}
						else
							{
								#print AlleleVarntsfile "\t$tempresidue\tType ",$Alleles_CustSqftType->{$pos}{$Allele}{$tempresidue},"\t$pos$tempresidue";
								print AlleleVarntsfile "\t$tempmotifseq\tType ",$temptype,"\t$tempvardefresidue";
								#print AlleleVarntsfile "\t$tempresidue\tType ",$Alleles_CustSqftType->{$pos}{$Allele}{$tempresidue},"\t$refseqnumb[$pos-1]$tempresidue";
								#print debug "\nInside Allele Print Pos- $pos Allelel- $Allele Tempresidue- $tempresidue \t$Alleles_CustSqftType->{$pos}{$Allele}{$tempresidue} OR $Alleles_PosTypes->{$pos}{$tempresidue}";
							}
					}
				$seqposct++;
			}
	}

	close IndivSeqfeat;
	close AlleleVarntsfile;
	close Unk_NonVarntsfile;
	close SqftVarntsfile;
	close AlleleSeqFeats;

#	close debug;
