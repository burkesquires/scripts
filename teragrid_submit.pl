#!/usr/bin/perl
use strict;
use SOAP::Lite +trace => qw(debug);
use Getopt::Long qw(:config no_ignore_case bundling);
use strict;

my $WSDL = 'http://lsgw.uc.teragrid.org/webservices/wsdl/JobService.wsdl';
my $soap = SOAP::Lite->service($WSDL);

my ($sequence, $outfile, $outformat, %params);
GetOptions(
"sequence|s=s" => \$sequence
);

# Open the file, read the sequences in the file
open(FH, $sequence) or die "No sequence given. Please specify -s ";
local($/) = undef;
my $seq = ;
close(FH);

# Set me as the user for now
my $params_val= "DF278EA2-D95C-11DB-892C-90C245496706";

#Please replace YOUR_LOGIN_NAME with the name you use to log into LSGW
$params{$params_val} = "burkesquires";

my $job = $soap->runIPRScan($seq,
SOAP::Data->name('params')->type(map=>\%params));
print "Run Job Result($job)";