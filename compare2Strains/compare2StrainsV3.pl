#!/usr/bin/perl -w
#
#	Created by Burke Squires on 28-Mar-2011
#	Copyright (C) 2011 UT Southwestern  All Rights Reserved

use strict;
use Bio::DB::GenBank;
use IO::CaptureOutput qw/capture/;

my ($stdout, $stderr);


my @strain1Segment = ("B/Florida/4/2006", "CY033882", "CY033883", "CY033881", "CY033876", "CY033879", "CY033878", "CY033877", "CY033880");
my @strain2Segment = ("B/Florida/7/2004", "CY033858", "CY033859", "CY033857", "CY033852", "CY033855", "CY033854", "CY033853", "CY033856");

#my @strain1Protein = ("B/Florida/4/2006", "CY033882", "CY033883", "CY033881", "CY033876", "CY033879", "CY033878", "CY033878", "CY033877", "CY033877", "CY033880", "CY033880");
#my @strain2Protein = ("B/Florida/7/2004", "CY033858", "CY033859", "CY033857", "CY033852", "CY033855", "CY033854", "CY033854", "CY033853", "CY033853", "CY033856", "CY033856");


my $db_obj = Bio::DB::GenBank->new;
my $path = "/Users/burkesquires/data/";


####################################################################################################
#======================================== Protein Analaysis ========================================
####################################################################################################


my $length = @strain1Segment;

my $strainOne = $strain1Segment[0];
my $strainTwo = $strain2Segment[0];

my $underscore = "_";

$strainOne =~ s/\//$underscore/ge;
$strainTwo =~ s/\//$underscore/ge;


my $analysisFile = $path . $strainOne . "-" . $strainTwo . "-NT-";

for (my $i = 1; $i < $length; $i++) {

	my $seq_obj1 = $db_obj->get_Seq_by_id($strain1Segment[$i]);
	my $seq_obj2 = $db_obj->get_Seq_by_id($strain2Segment[$i]);

	open (fasta1, ">" . $path . $strain1Segment[$i] . ".fasta"); 
	print fasta1 ">" . $strain1Segment[$i] . "\n";
	print fasta1 $seq_obj1->seq;
	
	open (fasta2, ">"  . $path . $strain2Segment[$i] . ".fasta"); 
	print fasta2 ">" . $strain2Segment[$i] . "\n";
	print fasta2 $seq_obj2->seq;
 

	my $command = '/usr/local/bin/needle -asequence ' . $path . $strain1Segment[$i];
	$command = $command . '.fasta -bsequence '  . $path .  $strain2Segment[$i];
	$command = $command . '.fasta -gapopen 10 -gapextend 0.5 -outfile ' . $analysisFile . $i . '.needle';

	#print $command . "\n";

	system $command;
}

printf ( "Segment %25s %25s %25s %25s\n","Identity", "Similarity", "Gaps", "Score");

for (my $i = 1; $i < $length; $i++) {

	open(ALIGNMENT, $analysisFile . $i . ".needle") or die("Error");

	my @list = <ALIGNMENT>; close ALIGNMENT;

	my $identity 	= "# Identity:";
	my $similarity 	= "# Similarity:";
	my $gaps 		= "# Gaps:";
	my $score		= "# Score:";

	my @match1 = grep /$identity/,@list;
	my @match2 = grep /$similarity/,@list;
	my @match3 = grep /$gaps/,@list;
	my @match4 = grep /$score/,@list;

	$match1[0] =~ s/$identity//g;
	$match2[0] =~ s/$similarity//g;
	$match3[0] =~ s/$gaps//g;
	$match4[0] =~ s/$score//g;

	chomp($match1[0]);
	chomp($match2[0]);
	chomp($match3[0]);
	chomp($match4[0]);
	
	
	printf ( "Seg.%3s: %25s %25s %25s %25s\n",$i, $match1[0], $match2[0], $match3[0], $match4[0]);

}

####################################################################################################
#======================================== Protein Analaysis ========================================
####################################################################################################

#$length = @strain1Protein;

#$strainOne = $strain1Segment[0];
#$strainTwo = $strain2Segment[0];

#$underscore = "_";

#$strainOne =~ s/\//$underscore/ge;
#$strainTwo =~ s/\//$underscore/ge;

my @proteins1 = "";
my @proteins2 = "";
my $geneID1 = "";
my $geneID2 = "";

for (my $i = 1; $i < $length; $i++) {

	my $seq_obj1 = $db_obj->get_Seq_by_id($strain1Segment[$i]);

	for my $feat_object ($seq_obj1->get_SeqFeatures) {          

	  	#print "primary tag: ", $feat_object->primary_tag, "\n";          

		if ($feat_object->primary_tag eq "CDS") {

	   					for my $tag ($feat_object->get_all_tags) {              

				
				if ($tag eq 'gene') {
	
					for my $value ($feat_object->get_tag_values($tag)) {                
							
						#print "    value: ", $value, "\n";             

						$geneID1 = $value;

						push @proteins1, $strain1Segment[$i] . "-" . $geneID1;
							
					}

				}

				if ($tag eq 'translation') {
				
					for my $value ($feat_object->get_tag_values($tag)) {                
		
						#print "    value: ", $value, "\n";             
					      
						open (fasta1, ">" . $path . $strain1Segment[$i] . "-" . $geneID1 . ".fasta"); 
						print fasta1 ">" . $geneID1 . "\n";
						print fasta1 $value;
		
					}
				}
			}          
		}       
	}

	my $seq_obj2 = $db_obj->get_Seq_by_id($strain2Segment[$i]);

	for my $feat_object ($seq_obj2->get_SeqFeatures) {          
	
		  	#print "primary tag: ", $feat_object->primary_tag, "\n";          
	
			if ($feat_object->primary_tag eq "CDS") {
	
		   		for my $tag ($feat_object->get_all_tags) {              
		
					if ($tag eq 'gene') {
		
						for my $value ($feat_object->get_tag_values($tag)) {                
								
							#print "    value: ", $value, "\n";             
	
							$geneID2 = $value;
	
							push @proteins2, $strain2Segment[$i] . "-" . $geneID2;
														
						}
			}
	
					if ($tag eq 'translation') {
					
						for my $value ($feat_object->get_tag_values($tag)) {                
			
							#print "    value: ", $value, "\n";             
						      
							open (fasta1, ">" . $path . $strain2Segment[$i] . "-" . $geneID2 . ".fasta"); 
							print fasta1 ">" . $geneID2 . "\n";
							print fasta1 $value;
			
						}
					}
				}          
			}       
		}
}

$analysisFile = $path . $strainOne . "-" . $strainTwo . "-AA-";

$length = @proteins1;

# loop through all proteins to align
print "Analaysis of proteins files;";

for (my $i = 1; $i < $length; $i++) {

	my $command = '/usr/local/bin/needle -asequence ' . $path . $proteins1[$i];
	$command = $command . '.fasta -bsequence '  . $path .  $proteins2[$i];
	$command = $command . '.fasta -gapopen 10 -gapextend 0.5 -outfile ' . $analysisFile . $i . '.needle';

	#print $command . "\n";

	system $command;
}


printf ( "\n\nProtein %25s %25s %25s %25s\n","Identity", "Similarity", "Gaps", "Score");


for (my $i = 1; $i < $length; $i++) {

	open(ALIGNMENT, $analysisFile . $i . ".needle") or die("Error");

	my @list = <ALIGNMENT>; close ALIGNMENT;

	my $identity 	= "# Identity:";
	my $similarity 	= "# Similarity:";
	my $gaps 		= "# Gaps:";
	my $score		= "# Score:";

	my @match1 = grep /$identity/,@list;
	my @match2 = grep /$similarity/,@list;
	my @match3 = grep /$gaps/,@list;
	my @match4 = grep /$score/,@list;

	$match1[0] =~ s/$identity//g;
	$match2[0] =~ s/$similarity//g;
	$match3[0] =~ s/$gaps//g;
	$match4[0] =~ s/$score//g;

	chomp($match1[0]);
	chomp($match2[0]);
	chomp($match3[0]);
	chomp($match4[0]);
	
	
	print $proteins1[$i] . ": " . $match1[0] . "\t" . $match2[0] . "\t" . $match3[0] . "\t" . $match4[0] . "\n";
	printf ( "%35s: %25s %25s %25s %25s\n",$proteins1[$i], $match1[0], $match2[0], $match3[0], $match4[0]);

}

close (fasta1);
close (fasta2);






