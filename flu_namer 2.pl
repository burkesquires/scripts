#!/usr/bin/perl

open(fileNames, "influenza_NA_conversion.txt") || die "File not found\n";
open(fileTree, "influenza_NA_tree.ph") || die "File not found\n";
open(fileTreeOut, ">influenza_NA_tree_corrected.ph");

#Load the names hashs
while (<fileNames>) {

	chomp;
	
	($oldName, $newName) = split(/\t/, $_);
	
	$proteinNames{$oldName} = $newName;
	
}

#run through the tree file
#read tree file line


while (<fileTree>) {
	
	chomp;
	
	$treeString = $_;
	
	#Extract node name
	if (index($treeString, "|") > 0) {
	
		#find name in hash - using substitutions
		($oldName) = split(/:/, $treeString);
		
		$newName = "";
		
		$newName = $proteinNames{$oldName};
		
		#replace anme in tree file
		if ($newName ne "") {
			
			$oldName =~ s/\|/::/g;
		
			$treeString =~ s/\|/::/g;
			
			$treeString =~ s/$oldName/$newName/;
	
		} 
		
	}
	
	print fileTreeOut $treeString;
	
}

close(fileNames);
close(fileTree);
close(fileTreeOut);