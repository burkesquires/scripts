#!/usr/bin/perl -w

# burke squires; richard.squires@utsouthwestern.edu
# Sept 2010

use strict;

use Spreadsheet::XLSX;

my $excel = Spreadsheet::XLSX->new(
    '/Users/burkesquires/Desktop/SourceFiles/IEDB epitope positive V2.xlsx',
    $converter );

foreach my $sheet ( @{ $excel->{Worksheet} } ) {

    printf( "Sheet: %s\n", $sheet->{Name} );

    $sheet->{MaxRow} ||= $sheet->{MinRow};

    foreach my $row ( $sheet->{MinRow} .. $sheet->{MaxRow} ) {

        $sheet->{MaxCol} ||= $sheet->{MinCol};

        foreach my $col ( $sheet->{MinCol} .. $sheet->{MaxCol} ) {

            my $cell = $sheet->{Cells}[$row][$col];

            if ($cell) {
                printf( "( %s , %s ) => %s\n", $row, $col, $cell->{Val} );
            }

        }

    }

}
