#!usr/bin/perl -w

use File::Basename;

# Burke Squires
# December 21, 2006
#
# Output as per Northrop Grumman instructions
# File one:
# Protein gi <tab> start <tab> end <tab> Isoelectric point
#
# File two:
# Protein gi <tab> ph <tab> bound <tab> charge
#
# Adapted from
# FASTApar version 0.1   k.james at bangor.ac.uk
# My 2nd Perl script
#
# Parses FASTA files into tab-delimited files.
# The output file is of the format:
#   SEQNAME tab SEQDESCRIPTION tab SEQUENCE


my @header;

push @suffixlist, ".fasta";
push @suffixname, ".fa";
push @suffixname, ".txt";

($sequenceFile) = @ARGV;

( my $basename, my $path, my $suffix ) =
  fileparse( $sequenceFile, @suffixlist );

$usage =
"perl fasta_tab.pl Sequence_File\nOutput: Analysis file with GI identifier <tab> start <tab> stop <tab> isoelectric point\n";
$usage = $usage . "Error file of failed sequences as FASTA\n";

unless ($sequenceFile)
{
    die "\nUsage: $usage\n";
}

open( SOURCE, "$sequenceFile" )
  || die "Couldn't find the source file $sequenceFile!\n";

my $errorfile = ">$path$basename.iep.error";

open( ERROR, $errorfile )
  or die "Couldn't create the error file.\n";

my $analysisfile1 = ">$path$basename.iep";

open( ANALYSIS1, $analysisfile1 )
  or die "Couldn't create the analysis file 1.\n";

my $analysisfile2 = ">$path$basename.iepextra";

open( ANALYSIS2, $analysisfile2 )
  or die "Couldn't create the analysis file 2.\n";

LINE: while ( $line = <SOURCE> ) {
    chomp($line);

  SWITCH: {

        if ( !$reading_seq && $line =~ />/ ) {

            # found the start of a sequence, so get the header
            # and remove the > character

            $reading_seq = 1;
            $acc_line    = $line;
            @header      = $acc_line . "\n";    #split (/\s+/, $acc_line);
            last SWITCH;

        }

        if ( $reading_seq && $line !~ /^>/ ) {

            # already reading a sequence and no new header on
            # this line, so remove whitespace and add the line
            # to the currently read sequence

            $line =~ s/\s+//g;
            push @sequence, $line;              #."\n";
            last SWITCH;

        }

        if ( $reading_seq && $line =~ /^>/ ) {

            # already reading a sequence but there is a header
            # on this line, so stop reading, make an entry in
            # the output list, clear the sequence list and redo
            # that line

            $reading_seq = 0;

            @entry = ( ( join( " ", $header[0] ) ), ( join( "", @sequence ) ) );

            push @output, join( "\t", @entry );

            analysizeData( $header[0], @sequence );

            
#            my $result = analysizeData( $header[0], @sequence );
#
#            if ( $result =~ /Error:|Died:/ ) {
#
#                my $errortemp = $line . "\n" . join( "\n", @sequence );
#
#                print ERROR $errortemp . "\n";
#
#            }
#            else {
#
#                my $analysistemp = $result;
#
#                print ANALYSIS1 $analysistemp . "\n";
#
#            }

            undef @sequence;
            redo LINE;

        }

    }
}

# add the last sequence to the output list when
# we have run out of lines in the source file as
# this is the only one whose end is not delimited
# by the start of a new sequence

@entry = ( ( join( " ", $header[0] ) ), ( join( "", @sequence ) ) );

push @output, join( "\t", @entry );

analysizeData( $header[0], @sequence );

print( "Found " . @output . " FASTA files in $sequenceFile\n" );

close(ERROR);

close(ANALYSIS1);

################################################################################
# functions

sub analysizeData {

    # isoelectric point calcualtions

    my $output;

    ( my $input, my @sequence ) = @_;

    my @identifier = split( /\|/, $input );

    @args = (
        "/sw/bin/iep -sequence",
        "asis::" . join( "", @sequence ),
        "-stdout -auto 2>&1"
    );

    my $commandline = join " ", @args;

    open( OUTPUT, "$commandline |" ) || warn "cannot execute iep: $!";

    $i = 1;

    while (<OUTPUT>) {

        my $temp = parseOutput( $identifier[1], $i, $_ );

        if ( $temp ne "" ) { $output = $output . $temp }

        $i++;

    }

    close OUTPUT;


}

##############################

sub parseOutput {

    my @output;

    ( my $identifier, my $linecounter, my $data ) = @_;

    #IEP of asis from 1 to 230
    #13383298 # 2 Isoelectric Point = 8.2250
    ## Protein gi <tab> start <tab> end <tab> Isoelectric point

    chomp($data);

    if ( ( $linecounter == 1 ) and ( $data =~ /Error:|Died:/ ) )
    {    # error codes present

        my @error = split( /:/, $' );

        push @output, $identifier;
        push @output, $&;
        push @output, $error[2];

        my $temp = join( "\t", @output );

		print $temp;
		
		
		my $errortemp = $line . "\n" . join( "\n", @sequence );

		print ERROR $errortemp . "\n";


    }
    else { # true data; no errors

        if ( $linecounter == 1 ) {

            my @temp = split( / /, $data );

            push @output, $identifier;
            push @output, $temp[4];
            push @output, $temp[6];

            $temp = join( "\t", @output );

            print ANALYSIS1 $temp;
            

        }
        elsif ( $linecounter == 2 ) {

            @temp = split( / /, $data );

            print ANALYSIS1 "\t$temp[3]\n";
            
            
        }
        elsif ( ( $linecounter > 4 ) and ( $data ne "" ) ) {

            # File two:
            # Protein gi <tab> ph <tab> bound <tab> charge

            my @temp = split( /\s+/, $data );

            push @output, $identifier;
            push @output, $temp[1];
            push @output, $temp[2];
            push @output, $temp[3];

            $temp = join( "\t", @output );

			print ANALYSIS2 $temp . "\n";
			

        }
    }

}
