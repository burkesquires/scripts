#!usr/bin/perl -w

# Burke Squires
# December 21, 2006
#
# Output as per NG instructions
# File one:
# Protein gi <tab> start <tab> end <tab> Isoelectric point
#
# File two:
# Protein gi <tab> ph <tab> bound <tab> charge
#
# If possible, we would like to have protein gi instead of protein accession.
# Make sure you use RefSeq protein sequences if the RefSeq version is available.
# Or use the sequences retrieved from our database.
# This will guarantee you use the correct version of input sequences.

my @header;

( $sequenceFile, $analysisFile, $errorFile ) = @ARGV;

$usage = "perl fasta_tab.pl Sequence_File Output_File Error_File";

unless ( $sequenceFile && $analysisFile )

{
    die "\nUsage: $usage\n";
}

open( SOURCE, "$sequenceFile" )
  || die "Couldn't find the source file $sequenceSource!\n";

LINE: while ( $line = <SOURCE> ) {
    chomp($line);

  SWITCH: {

        if ( !$reading_seq && $line =~ />/ ) {

            # found the start of a sequence, so get the header
            # and remove the > character

            $reading_seq = 1;
            $acc_line    = $line;
            @header      = $acc_line . "\n";    #split (/\s+/, $acc_line);
            last SWITCH;

        }

        if ( $reading_seq && $line !~ /^>/ ) {

            # already reading a sequence and no new header on
            # this line, so remove whitespace and add the line
            # to the currently read sequence

            $line =~ s/\s+//g;
            push @sequence, $line;              #."\n";
            last SWITCH;

        }

        if ( $reading_seq && $line =~ /^>/ ) {

            # already reading a sequence but there is a header
            # on this line, so stop reading, make an entry in
            # the output list, clear the sequence list and redo
            # that line

            $reading_seq = 0;

            @entry = ( ( join( " ", $header[0] ) ), ( join( "", @sequence ) ) );

            push @output, join( "\t", @entry );

            my $result = analysizeData( $header[0], @sequence );

            if ( $result =~ /Error:|Died:/ ) {
            
                push @error, $result;
            
            }
            else {
            
                push @analysis, $result;
            
            }

            undef @sequence;
            redo LINE;

        }

    }
}

# add the last sequence to the output list when
# we have run out of lines in the source file as
# this is the only one whose end is not delimited
# by the start of a new sequence

@entry = ( ( join( " ", $header[0] ) ), ( join( "", @sequence ) ) );

push @output, join( "\t", @entry );

push @analysis, analysizeData( $header[0], @sequence );

print( "Found " . @output . " FASTA files in $sequenceSource\n" );

# Error output

open( ERROR, ">$errorFile" )
  or die "Couldn't create the error file $errorFile!\n";

select(ERROR);
foreach $entry (@error) {
    print $entry . "\n";
}

close(ERROR);

# Analysis output

open( ANAYLYSIS, ">$analysisFile" )
  or die "Couldn't create the analysis file $analysisFile!\n";

select(ANAYLYSIS);
foreach $entry (@analysis) {
    print $entry . "\n";
}

close(ANAYLYSIS);

################################################################################
# functions

sub analysizeData {

    # isoelectric point calcualtions

    my $output;

    ( my $input, my @sequence ) = @_;

    my @identifier = split( /\|/, $input );

    @args = (
        "/sw/bin/iep -sequence",
        "asis::" . join( "", @sequence ),
        "-stdout -auto 2>&1"
    );

    my $commandline = join " ", @args;

    open( OUTPUT, "$commandline |" ) || warn "cannot execute iep: $!";

    $i = 1;

    while (<OUTPUT>) {

        my $temp = parseOutput( $identifier[1], $i, $_ );

        if ( $temp ne "" ) { $output = $output . $temp }

        $i++;

    }

    close OUTPUT;

    return $output;

}

##############################

sub parseOutput {

    my @output;

    ( my $identifier, my $line, my $data ) = @_;

    #IEP of asis from 1 to 230
    #13383298 # 2 Isoelectric Point = 8.2250
    ## Protein gi <tab> start <tab> end <tab> Isoelectric point

    chomp($data);

    if ( ( $line == 1 ) and ( $data =~ /Error:|Died:/ ) )
    {    # error codes present

        my @error = split( /:/, $' );

        push @output, $identifier;
        push @output, $&;
        push @output, $error[2];

        my $temp = join( "\t", @output );

        return $temp;

    }
    else {

        if ( $line == 1 ) {

            my @temp = split( / /, $data );

            push @output, $identifier;
            push @output, $temp[4];
            push @output, $temp[6];

            my $temp = join( "\t", @output );

            return $temp;

        }
        elsif ( $line == 2 ) {

            @temp = split( / /, $data );

            return "\t$temp[3]";
        }
    }

    # process the rest of the file
}
