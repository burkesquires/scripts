#!usr/bin/perl -w
# Burke Squires
# December 21, 2006
#
# Output as per NG instructions
# File one: 
# Protein gi <tab> start <tab> end <tab> Isoelectric point
# 
# File two: 
# Protein gi <tab> ph <tab> bound <tab> charge
# 
# If possible, we would like to have protein gi instead of protein accession. 
# Make sure you use RefSeq protein sequences if the RefSeq version is available. 
# Or use the sequences retrieved from our database. 
# This will guarantee you use the correct version of input sequences. 

my @header;

( $sequenceSource, $analysisDestination ) = @ARGV;

$usage = "perl fasta_tab.pl sequence_source_file analysis_destination_file program_name";
unless ( $sequenceSource && $analysisDestination )
{ die "\nUsage: $usage\n" };

open( SOURCE, "$sequenceSource" ) or die 
"Couldn't find the source file $sequenceSource!\n";

LINE:	while( $line = <SOURCE> ){
		chomp( $line );

SWITCH: {

if ( !$reading_seq && $line =~ />/ ) {
		# found the start of a sequence, so get the header
		# and remove the > character

		$reading_seq = 1;
		$acc_line=$line;
		@header = $acc_line."\n"; #split (/\s+/, $acc_line);
		last SWITCH;
			
		};

if ( $reading_seq && $line !~ /^>/ ) {
		# already reading a sequence and no new header on
		# this line, so remove whitespace and add the line
		# to the currently read sequence

		$line =~ s/\s+//g;
		push @sequence, $line;#."\n";
		last SWITCH;
		
		};

if ( $reading_seq && $line =~ /^>/ ) {
		# already reading a sequence but there is a header
		# on this line, so stop reading, make an entry in
		# the output list, clear the sequence list and redo
		# that line

		$reading_seq = 0;
		
		@entry = (
				( join ( " ", $header[0] ) ),
				( join ( "", @sequence ) )
				);
		
		push @output, join ( "\t", @entry );
		
		push @analysis, analysizeData($header[0], @sequence);

		undef @sequence;
		redo LINE;	
						
		};
		
	}
}

	# add the last sequence to the output list when
	# we have run out of lines in the source file as
	# this is the only one whose end is not delimited
	# by the start of a new sequence


	@entry = (
			( join ( " ", $header[0] ) ),
			( join ( "", @sequence ) )
			);

	push @output, join ( "\t", @entry );

	push @analysis, analysizeData($header[0], @sequence);

print ("Found " . @output . " FASTA files in $sequenceSource\n");

open ( DESTINATION, ">$analysisDestination" ) or die
"Couldn't create the destination file $analysisDestination!\n";

select ( DESTINATION );
foreach $entry ( @analysis ) {
	print $entry . "\n";
}

close ( DESTINATION );


################################################################################
# functions

sub analysizeData{

	# isoelectric point calcualtions
	
	my $output;
	
	(my $input, my @sequence) = @_;
	
	my @identifier = split(/\|/, $input);
		
	@args = ("/sw/bin/iep -sequence", "asis::".join ( "", @sequence ) , "-stdout -auto");
    	
    my $commandline = join " ", @args; 
    	    
	open (OUTPUT, "$commandline |");
		
		$i=1;
		
		while (<OUTPUT>){
		
		$output = $output . parseOutput($identifier[1], $i, $_);
					
		$i++;
		
	}
		
	close OUTPUT;	

	return $output;

}

	##############################

sub parseOutput{

	my @output;
	
	($identifier, my $line, $data) = @_;

	#IEP of asis from 1 to 230
	#13383298 # 2 Isoelectric Point = 8.2250
	## Protein gi <tab> start <tab> end <tab> Isoelectric point

	chomp($data);

	if ($line == 1) {
	
		@temp = split(/ /, $data);
	
		push @output, $identifier;
		push @output, $temp[4];
		push @output, $temp[6];
		
		my $temp = join ( "\t", @output );
		
		return $temp;
		
	}
	elsif ($line == 2) {
		@temp = split(/ /, $data);
	
		return "\t$temp[3]";
	}
	# process the rest of the file
}
