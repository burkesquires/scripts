rule qualrecal:
    input: bam="{base}.sra"
    output: bam="{base}.fastq"
    shell: "fastq-dump {input} {output}"