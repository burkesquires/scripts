def main(args):
	from Bio import SeqIO
	SeqIO.convert(args.input_file, "fastq", args.fasta, "fasta")
	SeqIO.convert(args.input_file, "fastq", args.qual, "qual")
		
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(prog='split_fastq.py', usage="%(prog)s -in FASTQ_file", description='Split FASTQ file into FASTA and qual.')
	required_group = parser.add_argument_group('Required')
	required_group.add_argument('-in', '--input_file', type=file, required=True, help='The input FASTQ file.')
	required_group.add_argument('-fa', '--fasta', type=file, required=True, help='A FASTA file of all sequences.')
	required_group.add_argument('-q', '--qual', type=file, required=True, help='A qual file of all sequences.')
	args = parser.parse_args()
	main(args)