#!/usr/bin/perl

use strict;
use warnings;
# bioperl was installed at custom location. Change to your local setting, or comment out.
 use lib "/home/gsun/lib/perl5/";

use Bio::Biblio;
use Bio::Biblio::IO;
use Carp;
use English;
use Getopt::Long;
use Data::Dumper;
my $debug = 0;

## This script either downloads citations from pubmed, or count the number of publications
## of an anthor. When downloading citations, it covers 11 viral families. When counting
## number of publications of an author, it reads from files named "pubmed_<vfamily>.xml",
## find all authors, then tally the number of publications
## Usage: ./pubmed_count.pl -d <0|1>

GetOptions(
            "-d=s" => \ my $download,      # either 0 or 1
          );
  print "Usage: ./pubmed_count.pl -d <0|1>\n\n";
  my ($biblio, $xml, $io, $article);
  $biblio = Bio::Biblio->new(
                             -access => 'eutils' 
#                             -access => 'pubmed' 
                            );
  my $families = [
#        'Flaviviridae',
#        'Bunyaviridae',
#        'Paramyxoviridae',
#        'Arenaviridae',
#        'Togaviridae',
#        'Herpesviridae',
#        'Poxviridae',
#        'Baculoviridae',
#        'Filoviridae',
#        'Iridoviridae',
        'Asfarviridae',
#        'Rotavirus',
                 ];
  my $period = ' 2000:2010 [dp]';

  my $count = {};
foreach my $term (@{$families}) {

  my $filename = 'pubmed_'.$term.'.xml';
  if ( $download == '1' ) {
    sleep(20) if ($count); # if we've downloaded before, wait 20 seconds
    $biblio->find($term.$period);

    open my $FILE,'>', $filename
      or croak "$0: Can\'t open $filename: $OS_ERROR";
    print $FILE $biblio->get_all;
    close $FILE;
  }

    $io = Bio::Biblio::IO->new(
                              -data   => $xml,
                              -file => $filename,
                              -format => 'pubmedxml',
#                              -format => 'medlinexml'
                              );
#    $debug && print STDERR "$0: \$io is a: ". ref($io) ."\n";
#    $debug && print STDERR "$0: \$io =\n". Dumper($io) ."\n\n";

  $count->{$term} = 0;
  my $authorlist = {};
#  while (($xml = $biblio->get_next) && $count<2) {
#    $debug && print STDERR "$0: \$xml =\n". Dumper($xml) ."\n\n";

#    $article = $io->next_bibref();
  while (($article = $io->next_bibref()) && $count->{$term}<20000 ) {
#    $debug && print STDERR "$0: \$article is a: ". ref($article) ."\n";
#    $debug && print STDERR "$0: \$article =\n". Dumper($article) ."\n\n";
#    $debug && print STDERR "$0: \$authors =\n". Dumper($article->{_authors}) ."\n\n";

    foreach my $author (@{$article->{_authors}}) {
#      $debug && print STDERR "$0: \$author =\n". Dumper($author) ."\n\n";
      next if (! $author->{_lastname});
      my $s;
      $s = $author->{_forename} ? $author->{_forename} : '';
      $s = $author->{_lastname}.', '. $s;
#      exit if ($s eq ', ');
      $authorlist->{$s}->{count}++;
#      if ($authorlist->{$s}->{count} == 1) {
#        $authorlist->{$s}->{aff} = $article->{_affiliation} ? $article->{_affiliation} : '';
      my $date = $article->{_date} ? $article->{_date} : '';
      push @{$authorlist->{$s}->{title}}, "($date) ".$article->{_title};
      $date = $article->{_affiliation} ? $article->{_affiliation} : '';
      push @{$authorlist->{$s}->{aff}}, $date;
      if ($date =~ /(\w+[^ ]*[@][^ ]+[.]\w{2,6})/i) {
#        print STDERR $1 . "\n";
        $authorlist->{$s}->{email}->{lc $1}++;
      }
    }

    $count->{$term}++;
    print STDERR "\$count->{$term}=$count->{$term}\n";
  }

  my $ct = 0;
  print "\$count->{$term}=$count->{$term}\n";
  foreach my $key (sort {$authorlist->{$b}->{count} <=> $authorlist->{$a}->{count} } (keys(%{$authorlist})) ) {
     $debug && print "\n";
     print "$term:\t$authorlist->{$key}->{count} \t'$key' \t";
     for my $email (sort keys %{$authorlist->{$key}->{email}}) {
       $debug && print "$authorlist->{$key}->{email}->{$email}:";
       print "$email, ";
     }
     print "\n";
     for my $i (0 .. $authorlist->{$key}->{count}-1) {
       $debug && print "\t\t\t$authorlist->{$key}->{title}->[$i]\n";
       print "\t\t\t\t$authorlist->{$key}->{aff}->[$i]\n";
       !$debug && last;
     }
     $ct++;
     last if ($ct>15);
  }

  print "\n";
} # families
exit;
