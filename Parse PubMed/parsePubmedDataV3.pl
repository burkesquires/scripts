#!/usr/bin/perl
use strict;

use XML::Parser;
use XML::SimpleObject;

my %strainHash;
my $file    = './pubmed_result_flu.xml';
my $outfile = $file . '.tab';

my $parser = XML::Parser->new( ErrorContext => 2, Style => "Tree" );
my $xmlobj = XML::SimpleObject->new( $parser->parsefile($file) );

extractAccessionAndStrainName();

open outFile, '>' . $outfile or die $!;

print outFile "Pandemic H1N1 2009 Related Articles from Pubmed.\n";
print outFile "Pubmed ID\tJournal\tArticle Title\tAuthors\n";

foreach
  my $article ( $xmlobj->child('PubmedArticleSet')->child('PubmedArticle') )
{

    #1) PubMed ID
    #2) Title
    #3) Authors
    #4) Abstract if a really interesting paper
    #5) H1N1 virus strains discussed
    #6) Protein or segment names in paper
    #7) Country or location where viruses were isolated

    my $pubmedID = $article->child('MedlineCitation')->child('PMID')->value;
    
    print outFile $pubmedID . "\t";

    print outFile $article->child('MedlineCitation')->child('Article')
      ->child('Journal')->child('Title')->value . "\t";

    print outFile $article->child('MedlineCitation')->child('Article')
      ->child('ArticleTitle')->value . "\t";

    # Get the author names,

    if ( $article->child('MedlineCitation')->child('Article')
	->child('AuthorList') )
    {

	if ( $article->child('MedlineCitation')->child('Article')
	    ->child('AuthorList')->child('Author')->children )
	{

	    foreach
	      my $author ( $article->child('MedlineCitation')->child('Article')
		->child('AuthorList')->child('Author') )
	    {

		foreach my $child ( $author->children ) {

		    if ( $child->name ne 'ForeName' ) {

			print outFile $child->value . ", ";

		    }

		}

	    }
	}

    }

    # get accession information

    if ( $article->child('MedlineCitation')->child('Article')
	->child('DataBankList') )
    {
	if ( $article->child('MedlineCitation')->child('Article')
	    ->child('DataBankList')->child('DataBank')
	    ->child('AccessionNumberList') )
	{

	   

		print outFile "\t";
				
		my $accessionlist =
		( $article->child('MedlineCitation')->child('Article')
		      ->child('DataBankList')->child('DataBank')
		      ->child('AccessionNumberList') );

		foreach my $child ( $accessionlist->children ) {

		    my $accession = $child->value;

		    #@strainData = ($accession, $strainName, $subtype, $segment, $protein );
		    
		    if ($accession eq "EU263559") {
			print "/n";
		    }
		    
		    if (exists $strainHash{ $accession }) {

		    # how to get teh array back out
		    # question is how to de-reference the array
		    
		    my @strainData = @{$strainHash{$accession}};
		    
		    my $printText = "\n" . $pubmedID . "\t\t\t" . join("\t", @strainData);
		    
		    print outFile $printText if exists $strainHash{ $accession };
		    }
		}
	}
    }

    print outFile "\n";
}

close outFile or die $!;

sub extractAccessionAndStrainName() {

    # open the flu xml file

    open( my $strainFile, './all_flu.tab' );

    while (<$strainFile>) {

	chomp;

    # Segment	Protein Name	Source Sequence Accession	Subtype	Strain Name	
    # 4	HA Hemagglutinin,	AB013806	H3N2	A/Tokyo/1511/98	

	(my $segment, my $protein,  my $accession, my $subtype, my $strainName ) = split("\t");

	#my @strainData = ($accession, $strainName, $subtype, $segment, $protein );
	
	$strainHash{$accession} = [ $accession, $strainName, $subtype, $segment, $protein ];

    }

    close($strainFile);
}

exit;
