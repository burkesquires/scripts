#!/usr/bin/perl
use strict;

use XML::Parser;
use XML::SimpleObject;

my $file    = '/Users/squiresrb/Documents/Projects/Antimicrobial peptide database metaanalysis/pubmed_result.xml';
my $outfile = $file . '.tab';

#my $parser = XML::Parser->new( ErrorContext => 2, Style => "Tree" );
#my $xmlobj = XML::SimpleObject->new( $parser->parsefile($file) );

my $parser = new XML::Parser (ErrorContext => 2, Style => "Tree");
my $xmlobj = new XML::SimpleObject ($parser->parse($file));

open outFile, '>' . $outfile or die $!;

print outFile "Antimicrobial Database metadata summary.\n";
#print outFile "Pubmed ID\tJournal\tArticle Title\tAuthors";

foreach
  my $article ( $xmlobj->child('PubmedArticleSet')->child('PubmedArticle') )
{

    #1)ÊÊÊÊÊ PubMed ID
    #2)ÊÊÊÊÊ Title
    #3)ÊÊÊÊÊ Authors
    #4)ÊÊÊÊÊ Abstract if a really interesting paper
    #5)ÊÊÊÊÊ H1N1 virus strains discussed
    #6)ÊÊÊÊÊ Protein or segment names in paper
    #7)ÊÊÊÊÊ Country or location where viruses were isolated

    print outFile $article->child('MedlineCitation')->child('PMID')->value
      . "\t";

    print outFile $article->child('MedlineCitation')->child('Article')
      ->child('Journal')->child('Title')->value . "\t";

    print outFile $article->child('MedlineCitation')->child('Article')
      ->child('ArticleTitle')->value . "\t";

    # Get the author names,

    if ( $article->child('MedlineCitation')->child('Article')
        ->child('AuthorList') )
    {

        if ( $article->child('MedlineCitation')->child('Article')
            ->child('AuthorList')->child('Author')->children )
        {

            foreach
              my $author ( $article->child('MedlineCitation')->child('Article')
                ->child('AuthorList')->child('Author') )
            {

                foreach my $child ( $author->children ) {

                    if ( $child->name ne 'ForeName' ) {

                        print outFile $child->value . ", ";

                    }

                }

            }
        }

    }

    # get accession information

    if ( $article->child('MedlineCitation')->child('Article')
        ->child('DataBankList') )
    {
        if ( $article->child('MedlineCitation')->child('Article')
            ->child('DataBankList')->child('DataBank')->child('DataBankName')
            ->child('AccessionNumberList') )
        {

         # if ( $article->child('MedlineCitation')->child('Article')
         #     ->child('DataBankList')->child('DataBank')->child('DataBankName')
         #     ->child('AccessionNumberList')->children )
         # {
            print outFile "\t";

            my $accessionlist =
              ( $article->child('MedlineCitation')->child('Article')
                  ->child('DataBankList')->child('DataBank')
                  ->child('DataBankName')->child('AccessionNumberList')
                  ->children );

            foreach my $child ( $accessionlist->children ) {

                print outFile $child->value . ", ";

                #         }

            }
        }
    }

    print outFile "\n";
}

close outFile or die $!;
