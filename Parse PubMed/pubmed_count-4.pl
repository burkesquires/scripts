#!/usr/bin/perl

use strict;
use warnings;
# bioperl was installed at custom location. Change to your local setting, or comment out.
 use lib "/home/gsun/lib/perl5/";

use Bio::Biblio;
use Bio::Biblio::IO;
use Carp;
use English;
use Getopt::Long;
use Data::Dumper;
my $debug = 0;
my $separate_families = '0';

## This script counts the number of publications of an author and harvests email addresses.
## It either downloads citations from pubmed, or reads from existing xml files (named
## "pubmed_<vfamily>_<term>.xml").
## It covers 16 viral families, plus any additional terms. When counting number of
## publications of an author, it first removes duplicate publications according to PMID,
## finds all authors and gives them the corresponding email. Records for each family are
## saved in a hash. If families are not to be separated, it combines the authors in to
## one hash. The result is printed to 2 files, one for authors w/o e-mail, another
## a list of all emails.
## Usage: ./pubmed_count.pl -d <0|1> [-f <subdir>] [-p <pattn>] [-s <0|1>] [-a <0|1>]

my $download = '0';
my $pattn    = q{};
my $subdir   = 'xml';
my $separate_families   = '0';
my $print_all           = '0';
GetOptions(
            "-d=s" => \ $download,      # either 0 or 1
            "-p=s" => \ $pattn,         # the search term
            "-f=s" => \ $subdir,        # subdirectory for xml files
            "-s=s" => \ $separate_families, # Print out results separately for families
            "-a=s" => \ $print_all,         # Print out all authors
          );
  print STDERR "Usage: ./pubmed_count.pl -d <0|1> [-f <subdir>] [-p <pattn>] [-s <0|1>] [-a <0|1>]\n\n";
  my ($biblio, $xml, $io, $article);
  $biblio = Bio::Biblio->new(
                             -access => 'eutils'
                            );
  my $terms;
if ($pattn) {
  $terms = {$pattn => []};
#  $download = 1;
} else {
  my $families1 = {
        'Flaviviridae'    => [],
        'Bunyaviridae'    => [],
        'Paramyxoviridae' => [],
        'Arenaviridae'    => [],
        'Togaviridae'     => [],
        'Herpesviridae'   => [],
        'Poxviridae'      => [],
        'Baculoviridae'   => [],
        'Filoviridae'     => [],
        'Iridoviridae'    => [],
        'Asfarviridae'    => [],
        'Rotavirus'       => [],
                 };

  my $families = {
# This list is adapted from Burke Squires' e-mail on 1/22/2010. There are some minor changes, such as removal of (MOCV) and (KFVD)
'Arenaviridae' => [],
'Bunyaviridae' => [
	'Hantavirus',
	'Rift Valley Fever Virus',	# (RVFV)
	'LaCrosse Virus',	# (LACV)
	'California Encephalitis Virus',
	'Crimean-Congo Hemorrhagic Fever Virus',	# (CCHFV)
],
'Caliciviridae' => [],
'Coronaviridae' => [
	'SARS virus',
],
'Filoviridae' => [
	'Ebola virus',
	'Marburg virus',
],
'Flaviviridae' => [
	'Dengue',	# (DEN)
	'Hepatitis C',	# (HCV)
	'West Nile Virus',	# (WNV)
	'Japanese Encephalitis Virus',	# (JEV)
	'Yellow Fever Virus',	# (YFV)
	'Kyasanur Forest Virus',
	'Tickborne Encephalitis Virus',	# (TBEV)
],
'Herpesviridae' => [
	'Herpes Simplex 1',	# (HSV1)
	'Herpes Simplex 2',	# (HSV2)
	'Varicella Zoster',	# (VZV)
	'Cytomegalovirus',	# (CMV)
	'Epstein-Barr',	# (EBV)
	'Human Herpesvirus 6',	# (HHV6)
	'Human Herpesvirus',	# (HHV7)
	'Kaposi Sarcoma-Associated Human Herpes Virus',	# (KSHV)
],
'Paramyxoviridae' => [
	'Henipavirus',
	'Nipah virus',
	'Hendra virus',
],
'Picornaviridae' => [],
'Poxviridae' => [
	'Variola Major',	# (VARV)
	'Variola Minor',
	'Molluscum contagiosum',
	'Vaccinia',	# (VACV)
	'Cowpox',	# (CPXV)
	'Ectromelia',	# (ECTV)
	'Monkeypox',
],
'Rhabdoviridae' => [],
'Togaviridae' => [],
         };
  $terms = {%{$families1}, %{$families}};
  print "$0: \$terms ='\n". Dumper($terms) ."'\n";
}
  my $period = ' 2000:2010 [dp]';

# Check if we have the PubMed xml files. If not, download
if (! -e $subdir) {
  mkdir($subdir) or croak "$0: Can\'t creat $subdir: $OS_ERROR";
}
foreach my $term (sort keys %{$terms}) {

  foreach my $term1 (@{$terms->{$term}}) {
    my $filename1 = "$subdir/pubmed_".lc $term.'_'.lc $term1.'.xml';
    print "\$filename1=$filename1\n";
    if ( ($download eq '1') || !(-e $filename1) ) {
      print "Searching for '$term1', and saving to \$filename=$filename1\n";
      $biblio->find($term1.$period);

      open my $FILE,'>', $filename1
        or croak "$0: Can\'t open $filename1: $OS_ERROR";
      print {$FILE} $biblio->get_all or croak "Can't close $filename1: $OS_ERROR\n";
      close $FILE or croak "Can't close $filename1: $OS_ERROR\n";
      sleep(90) if ($term1 ne $term);    # wait 20 seconds
    } else {
      print "Existing xml for '$term1', \tin \$filename=$filename1\n";
    }
  } # $terms->{$term}

} # $terms

# count the publications for all search terms, separate for each family
  my $authorlists = {};
  my $count = {};
foreach my $term (keys %{$terms}) {
  $count->{$term} = 0;
  my $authorlist = {}; # hash of names, having count, arrays of title, aff, and array of email
  my $mergedpubs = {}; # list of unique pubs, based on pmid
  foreach my $term1 ($term, @{$terms->{$term}}) {
    my $filename = "$subdir/pubmed_".lc $term.'_'.lc $term1.'.xml';
    print "Using xml for '$term:$term1', \tin \$filename=$filename\n";
#    print "\$filename=$filename\n";

    $io = Bio::Biblio::IO->new(
                              -data   => $xml,
                              -file   => $filename,
                              -format => 'pubmedxml',
                              );

    while (($article = $io->next_bibref()) ) {
#      $debug && print STDERR "$0: \$article =\n". Dumper($article) ."\n\n";

      if (!exists($mergedpubs->{$article->{_pmid}})) {
        $mergedpubs->{$article->{_pmid}} = 1;
      } else {
        next; # skil if article has been seen
      }
      foreach my $author (@{$article->{_authors}}) {
        next if (! $author->{_lastname}); # in case there is no lastname
        my $s;
        $s = $author->{_forename} ? $author->{_forename} : '';
        $s = $author->{_lastname}.', '. $s;

        $authorlist->{$s}->{count}++;

        my $date = $article->{_date} ? $article->{_date} : '';
        push @{$authorlist->{$s}->{title}}, "($date) ".$article->{_title};

        my $aff = $article->{_affiliation} ? $article->{_affiliation} : '';
        push @{$authorlist->{$s}->{aff}}, $aff;
        if ($aff =~ /[@]/i) {
          # try to correct some of the formatting errors in email address
          $aff =~ s/ \@/\@/g;
          $aff =~ s/\@ /\@/g;
          $aff =~ s/-ag-de/.ag.de/g;
          $aff =~ s/unimi.i$/unimi.it/g;
          $aff =~ s/ ac.uk/ac.uk/g;
          $aff =~ s/uku.f$/uku.fi/g;
          $aff =~ s/mdanderson$/mdanderson.org/g;
          $aff =~ s/i itd.pan.wroc.pl/iitd.pan.wroc.pl/g;
          $aff =~ s/ up ac za/.up.ac.za/g;
          $aff =~ s/inia.e$/inia.es/g;
          $aff =~ s/gmail .com/gmail.com/g;
          $aff =~ s/ nih.gov/nih.gov/g;
          $aff =~ s/\@ss$/\@inia.es/g;
          $aff =~ s/\@tt$/\@unimi.it/g;
          $aff =~ s/\@virgsdu$/\@virginia.edu/g;
          $aff =~ s/a p-hop-paris.fr/ap-hop-paris.fr/g;
          $aff =~ s/mcmedsoc,org/mcmedsoc.org/g;
          $aff =~ s/ .com/.com/g;
          $aff =~ s/ rochester.edu/rochester.edu/g;
          $aff =~ s/ ac.jp/ac.jp/g;
          $aff =~ s/kfl apublichealth.ca/kflapublichealth.ca/g;
          $aff =~ s/uniroma1\@it/uniroma1.it/g;
          if ($aff =~ /(\w+[^ ();:,\&|]*[@][^ ]+[.]\w{2,6})/i) {
#            $debug && print STDERR $1 . "\n";
            $authorlist->{$s}->{email}->{lc $1}++;
          } else {
          print STDERR "Can't find email in \$aff=$aff\n";
          }
        }
      }

      $count->{$term}++;
    }

    print "\$count->{$term}=$count->{$term}\n";
  } # $term, @{$terms->{$term}}
  $authorlists->{$term} = $authorlist; # has title, aff, email
} # $terms

# print out the results
if ( 1 ) {

 if ($separate_families eq '0') {
  foreach my $term (keys %{$authorlists}) {

    print "Lump families together: \$term=$term\n";
    my $authorlist = {};
    $authorlist = $authorlists->{$term};

    foreach my $name (sort keys %{$authorlist}) {

       my $author = $authorlist->{$name};
       if (!exists($authorlists->{'All'}->{$name})) {
         $authorlists->{'All'}->{$name} = $author;
       } else {
         $authorlists->{'All'}->{$name}->{count} += $author->{count};
         push @{$authorlists->{'All'}->{$name}->{title}}, @{$author->{title}};
         push @{$authorlists->{'All'}->{$name}->{aff  }}, @{$author->{aff}};

        if (exists($authorlist->{$name}->{email})) {
         for my $email (keys %{$authorlist->{$name}->{email}}) {
           $authorlists->{'All'}->{$name}->{email}->{$email}++;
         }
        }
       }

    }
    delete($authorlists->{$term}); # free up mem
  } # $terms
 }

    print "\nStarting final count\n";
    my $mergedemails;
  foreach my $term (keys %{$authorlists}) {

    my $filename;
    $filename = 'pubmed_'.lc $term.'_author_noemail.txt';
    print "$0: writing to $filename\n";
    open my $fh_author_noemail, '>', $filename or croak "Can't open $filename\n";

    my $authorlist = {};
    $authorlist = $authorlists->{$term};
    $mergedemails = {};
    my $ct = 0;
    print "\$count->{$term}=$count->{$term}\n" if (exists($count->{$term}));
    foreach my $name (sort {$authorlist->{$b}->{count} <=> $authorlist->{$a}->{count} } (sort keys %{$authorlist}) ) {

       $ct++;
       last if ($authorlist->{$name}->{count}<2);

       if (exists($authorlist->{$name}->{email})) {
         for my $email (keys %{$authorlist->{$name}->{email}}) {
           $mergedemails->{$email}++;
         }
       }

       # Whether to print info for authors with e-mails
       next if (($print_all ne '1') && exists($authorlist->{$name}->{email}));

         print {$fh_author_noemail} "$term:\t#$ct\t$authorlist->{$name}->{count} \t'$name' \t"
               or croak "Can't close $fh_author_noemail\n";
         for my $email (sort {$authorlist->{$name}->{email}->{$b} <=> $authorlist->{$name}->{email}->{$a}} keys %{$authorlist->{$name}->{email}}) {
           if (!$debug) {
             print {$fh_author_noemail} "$authorlist->{$name}->{email}->{$email}:"
               or croak "Can't close $fh_author_noemail\n";
           }
           print {$fh_author_noemail} "$email, "
               or croak "Can't close $fh_author_noemail\n";
         }
         print {$fh_author_noemail} "\n" or croak "Can't close $fh_author_noemail\n";

         for my $i (0 .. $authorlist->{$name}->{count}-1) {
           if ($debug) {
             print {$fh_author_noemail} "\t\t\t$authorlist->{$name}->{title}->[$i]\n"
               or croak "Can't close $fh_author_noemail\n";
           }
           print {$fh_author_noemail} "\t\t\t\t$authorlist->{$name}->{aff}->[$i]\n"
               or croak "Can't close $fh_author_noemail\n";
           !$debug && last;
         }
    }
    print "\n" or croak "Can't close $filename\n";
    close $fh_author_noemail or croak "Can't close $filename\n";

    # print all the unique emails
    $filename = 'pubmed_'.lc $term.'_emails.txt';
    print "$0: writing to $filename\n\n";
    open my $fh_email, '>', $filename or croak "Can't open $filename\n";
    foreach my $email (sort{$mergedemails->{$b} <=>$mergedemails->{$a}} keys%{$mergedemails}) {
      if ( 0 ) {print {$fh_email} "$mergedemails->{$email}:" or croak "Can't write to $fh_email\n";}
      print {$fh_email} "$email\n"   or croak "Can't write to $fh_email\n";
    }
    close $fh_email or croak "Can't close $fh_email\n";

  } # $terms
}
