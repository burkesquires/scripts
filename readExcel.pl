#!/usr/bin/perl -w

# burke squires; richard.squires@utsouthwestern.edu
# Sept 2010

use strict;
use Spreadsheet::ParseExcel;

my proteins = {'PB2, PB1, PB1-F2, PA, HA, NP, NA, M1, M2, NS1, NEP'};

# loop through each data file for each protein

# save a single data file for each protein






sub readB-cellEpitopeFile {

    return $self->{_firstName};


}





my $parser   = Spreadsheet::ParseExcel->new();
my $workbook = $parser->parse(
    '/Users/burkesquires/Desktop/SourceFiles/IEDB epitope positive V2.xls');

if ( !defined $workbook ) {
    die $parser->error(), ".\n";
}

for my $worksheet ( $workbook->worksheets() ) {

    my ( $row_min, $row_max ) = $worksheet->row_range();
    my ( $col_min, $col_max ) = $worksheet->col_range();

    for my $row ( $row_min .. $row_max ) {
        for my $col ( $col_min .. $col_max ) {

            my $cell = $worksheet->get_cell( $row, $col );
            next unless $cell;

            print "Row, Col    = ($row, $col)\n";
            print "Value       = ", $cell->value(),       "\n";
            print "Unformatted = ", $cell->unformatted(), "\n";
            print "\n";
        }
    }
}
