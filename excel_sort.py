import xlwt
from xlrd import open_workbook

excel_file_path = '/Users/squiresrb/Desktop/excel-sort-test.xls'


target_column = 51

book = open_workbook(excel_file_path, formatting_info=True)
sheet = book.sheets()[0]
data = [sheet.row_values(i) for i in xrange(sheet.nrows)]
labels = data[0]
data = data[1:]
#data.sort(key=lambda x: (x[target_column]))
data.sort(key=lambda x: (x[target_column]), reverse=True)

wbk = xlwt.Workbook()
sheet = wbk.add_sheet(sheet.name)

for idx, label in enumerate(labels):
     sheet.write(0, idx, label)

for idx_r, row in enumerate(data):
    for idx_c, value in enumerate(row):
        sheet.write(idx_r+1, idx_c, value)

wbk.save('/Users/squiresrb/Desktop/result.xls')

