#!/usr/bin/perl -w

use warnings;
use strict;
use Bio::SeqIO;
use Getopt::Long;
use Pod::Usage;
use File::Basename;

if ($#ARGV == -1) {
    &usage;
}

my $input   = "";
my $min = 0;
my $max = 0;
my $result = GetOptions ("i=s" => \$input,    # string
							"min=i"   => \$min,      # numeric
                        "max=i"   => \$max)      # numeric
							or pod2usage(2);     # string
							
my @suffixes = (".fasta",".fa");
my($filename, $directories, $suffix) = fileparse($input, @suffixes);
my $output = $directories . $filename . "-" . $min . "-" . $max . ".fasta";

my $in  = Bio::SeqIO->new(-file => $input , '-format' => 'fasta');
my $out = Bio::SeqIO->new(-file => '>' . $output , '-format' => 'fasta');

while ( my $seq = $in->next_seq() ) {
	my $len = $seq->length;
	if($len >= $min && $len <= $max){
			$out->write_seq($seq);
		}
}


#-----------------------------------------------------------------------
sub usage {
    print "\nUsage: parseSeqByLength.pl\n\n";
    print "Parameters:\n\n";
    print "-i\tA fasta file\n\n";
    print "-min\tIn the new fasta file, include sequences greater than or equal to this number\n\n";
    print "-max\tIn the new fasta file, include sequences less than or equal to this number\n\n";
    print "This script takes a fasta file (-i) and two integers (-min, -max) and returns a new fasta file that contains only those records that have a length within the range of numbers provided. The output file name is automatically generated from the input name and the minimum and maximum values.\n\n";
    print "R. Burke Squires\n";
    print "December 7, 2012\n\n";
    exit;
}
#-----------------------------------------------------------------------