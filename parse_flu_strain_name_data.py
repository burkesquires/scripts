__author__ = 'squiresrb'

#['CY015022', 'A/chicken/Victoria/1985', '1985//', '5 (NP)', 'Avian', 'H7N7', 'Australia', '1985', '', '', '', '']

def count_char_digits_punc_in_string(str):
    import string
    count = lambda l1, l2: len(list(filter(lambda c: c in l2, l1)))
    num_chars = count(str, string.ascii_letters)
    num_digits = count(str, string.digits)
    num_punct = count(str, string.punctuation)
    return num_chars, num_digits, num_punct

def parse_flu_data( description ):
    strain_name = _parse_ncbi_flu_description(description)
    accession = _parse_accession(description)
    flu_type, host, location, isolate, isolation_date = parse_flu_strain_name( strain_name )
    subtype = _parse_subtype(description)
    return accession, flu_type, host, location, isolate, isolation_date, subtype

def parse_flu_strain_name( strain_name ):
    flu_type = _parse_flu_type(strain_name)
    host = _parse_host(strain_name)
    location = _parse_location(strain_name)
    isolate = _parse_isolate(strain_name)
    isolation_date = _parse_isolation_date(strain_name)
    return flu_type, host, location, isolate, isolation_date

def _parse_flu_type(strain_name):
    if strain_name[0] in ["A", "B", "C"]:
        return strain_name[0]
    else:
        return ""

def _parse_host(strain_name):
    count = strain_name.count("/")
    fields = strain_name.split("/")
    if count == 1:
        return "human"
    elif count == 2:
        return "human"
    elif count == 3: #A/Puerto Rico/8/34; 'A/chicken/Victoria/1985'
        return fields[1]
    elif count == 4:
        return fields[1]


def _parse_location ( strain_name ):
    count = strain_name.count("/")
    fields = strain_name.split("/")
    if count == 1:
        return "" #A/X-31
    elif count == 2:
        return fields[1] #?
    elif count == 3: #A/Puerto Rico/8/34
        return fields[2]
    elif count == 4:
        return fields[3]

def _parse_isolate ( strain_name ):
    count = strain_name.count("/")
    fields = strain_name.split("/")
    if count == 1:
        return "" #A/X-31
    elif count == 2:
        return "" #?
    elif count == 3: #A/Puerto Rico/8/34
        return fields[2]
    elif count == 4:
        return fields[3]

def _parse_isolation_date ( strain_name ):
    print( strain_name )
    count = strain_name.count("/")
    fields = strain_name.split("/")
    if count == 1:
        return "" #A/X-31
    elif count == 2:
        return fields[2]
    elif count == 3: #A/Puerto Rico/8/34
        return fields[3]
    elif count == 4:
        return fields[4]
    #import re
    #regex = re.compile( "\d+\/\d*\/\d*" )
    #temp = regex.findall( strain_name )
    #return int( temp[0] )

def _parse_ncbi_flu_description ( description ):
    temp = description.split( "|" )
    return temp[1]

def _parse_accession ( description ):
    import re
    regex = re.compile( "[A-Z]+\d\d\d+" )
    temp = regex.findall( description )
    tempAccession = temp[0].strip( ">" )
    return tempAccession

def _parse_subtype ( description ):
    import re
    #print description
    regex = re.compile( "[H]+\d+[N]+\d+" )
    #regex = re.compile( ">[A-Z]+\d+" )
    subtype = regex.findall( description )
    #tempAccession = subtype[0].strip( ">" )
    #print tempAccession
    return subtype[0]

