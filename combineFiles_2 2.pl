##########################################################
# This program was designed for combine a serious of cluster #
# files into a single file with cluster ID added     \   #
# Author Megan Kong 3/6/2006                             #
##########################################################

#!/usr/bin/perl -w


#read in two file names
unless (open (FILES, "config.txt")) {
	print "Can not open file \"config.txt\" ! \n";
	exit;
}

my @files=();


while (my $filename = <FILES>) {
	push @files, $filename;
}

close (FILES);

my $outputfile="clusters_2.txt";

unless(open(OUTPUT, ">$outputfile")){
	print "Can not open file \"outputfile\" ! \n";
	exit;
}



##process 20 pairs of files

for (my $x = 0; $x <= $#files; $x++){

## assign file name to variables

my $filename1 = $files[$x];

print $filename1,"\n";

my @data = ();

open(DATAFILE, $filename1);

unless(open(DATAFILE, $filename1)){
	die("Could not open file $filename1\n");
	exit;
}


while (my $rec = <DATAFILE>) {
	push  @data, $rec;
}
close(DATAFILE);


my @data_matrix = ();

my $total_record1;
for (my $m = 0; $m <= $#data; $m++){
    my $rec = $data[$m];
   #print "here is each line of record:";
   # print $rec;

    chomp ($rec);
    my @tmp = split (/\t/, $rec);
    $total_records1 = $total_records1 + 1;

    push @data_matrix, [@tmp];
}



#print $data_matrix[6][1],"\t", $signal_matrix[5][1],"\t", $signal_matrix[4][1], "\n";



##only need to search at the beginning of the file.  Add cluster ID to the matrix
for (my $m = 1; $m <= $#data_matrix; $m++){

	$data_matrix[$m][0] = $x + 1;

}

##replace the _at with NCBI_refseq in the list

for (my $i=1; $i <= $#data_matrix; $i++){

	my $my_string = $data_matrix[$i][3];

	## remove the _at at the end od each probeset
	$data_matrix[$i][1] = substr($my_string ,0, -3);

	##addthe NCBI_refseq to the end of each probe set, use teh dot operator to concatenate string
	my $ncbi= "_NCBI_refseq";
	$data_matrix[$i][1] = ($data_matrix[$i][1].$ncbi) ;


}


##print out the three rows that are needed for classii

for (my $i=1; $i <= $#data_matrix; $i++){


    my $GeneName = 'Control Genes';

	print OUTPUT "$data_matrix[$i][1]\t$GeneName\t$data_matrix[$i][0]\t";

	print OUTPUT "\n";

} ##end of one iteration




}##end of all iteration

close(OUTPUT);

exit;
