#!usr/bin/perl -w

# Burke Squires
# November 3, 2007
#
# read in amino acid functional group translations
# import PDB file
# import poly data
# Extract amino acid sequences
# loop though amino acids and print out atomic data file


use File::Basename;
use strict;
use warnings;
use BeginPerlBioinfo;

my @results;

my @AminoAcidFunctionalGroups = ( 
['ala', 'A', '5'],
['arg', 'R', '11'],
['asn', 'N', '8'],
['asp', 'D', '8'],
['cys', 'C', '6'],
['glu', 'E', '9'],
['gln', 'Q', '9'],
['gly', 'G', '4'],
['his', 'H', '10'],
['ile', 'I', '8'],
['leu', 'L', '8'],
['lys', 'K', '9'],
['met', 'M', '8'],
['phe', 'F', '11'],
['pro', 'P', '7'],
['ser', 'S', '6'],
['thr', 'T', '7'],
['trp ', 'W', '14'],
['tyr', 'Y', '12'],
['val', 'V', '7']);


# Give the name of your PDB file
my $pdbfile = '/Users/burkesquires/Desktop/3D/1RUZ.pdb';
my $datafile = '/Users/burkesquires/Desktop/3D/3d_data.txt';

# Get the file data, parse the record types
my @pdbfiledata = get_file_data($pdbfile);
my @datafiledata = get_file_data($datafile);

my %recordtypes = parsePDBrecordtypes(@pdbfiledata);

# Extract the name and the sequence of each chain
my %chains = extractSEQRES2($recordtypes{'SEQRES'});
foreach my $key (keys %chains) {
	# Change the sequence from 3-letter to 1-letter amino acid codes
	$chains{$key} = iub3to1($chains{$key});
	# For debugging purposes
	#print "$key:$chains{$key}\n";
}



# 
# Now make a annotation strings that contain the helix, sheet, and turn information
#

my %annotation = ();

foreach my $chain_name (sort keys %chains) {

	# Make a blank annotation string the same length as the sequence of the chain
	$annotation{$chain_name} = ' ' x length($chains{$chain_name});

	print "Chain name = \"$chain_name\"\n";
	print $chains{$chain_name},"\n";
	print "$annotation{$chain_name}\n";
}

exit;

################################################################################
# Subroutines
################################################################################

sub stripspaces {
	my($string) = @_;

	$string =~ s/^\s*//;
	$string =~ s/\s*$//;

	return $string;
}


# extractSEQRES2
#
#-given an scalar containing SEQRES lines,
#    return an array containing the chains of the sequence
#
# Modified version that reports the chain name plus the sequence, 
# returning the values as a hash

sub extractSEQRES2 {

    use strict;
    use warnings;

    my($seqres) = @_;

    my $lastchain;
    my $sequence = '';
    my %results = (  );
    # make array of lines

    my @record = split ( /\n/, $seqres);
    
    foreach my $line (@record) {
        # Chain is in column 12, residues start in column 20
        my ($thischain) = stripspaces(substr($line, 11, 1));
        my($residues)  = substr($line, 19, 52);
    
        # Check if a new chain, or continuation of previous chain
        if(not defined $lastchain ) {
            $sequence = $residues;
        }elsif("$thischain" eq "$lastchain") {
            $sequence .= $residues;
        # Finish gathering previous chain (unless first record)
        }elsif ( $sequence ) {
            $results{$lastchain} = $sequence;
            $sequence = $residues;
        }
        $lastchain = $thischain;
    }

    # save last chain
    $results{$lastchain} = $sequence;
    
    return %results;
}
