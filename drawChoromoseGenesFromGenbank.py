from Bio.Graphics import BasicChromosome
from reportlab.lib.units import cm
from Bio import SeqIO

entries = [ ("Chr I", "NC_004325.1.gbk"),
			("Chr II", "NC_000910.2.gbk"),
			("Chr III", "NC_000521.3.gbk"),
			("Chr IV", "NC_004318.1.gbk"),
			("Chr V", "NC_004326.1.gbk"),
			("Chr VI", "NC_004327.2.gbk"),
			("Chr VII", "NC_004328.2.gbk"),
			("Chr VIII", "NC_004329.2.gbk"),
			("Chr IX", "NC_004330.1.gbk"),
			("Chr X", "NC_004314.2.gbk"),
			("Chr XI", "NC_004315.2.gbk"),
			("Chr XII", "NC_004316.3.gbk"),
			("Chr XIII", "NC_004331.2.gbk"),
			("Chr XIV", "NC_004317.2.gbk"),
			("Chr MT", "NC_002375.1.gbk")]

for (name, filename) in entries:
   record = SeqIO.read(filename,"genbank")
   print name, len(record)

max_len = 3291871 #Could compute this
telomere_length = 10000 #For illustration

chr_diagram = BasicChromosome.Organism()
chr_diagram.page_size = (100*cm, 100*cm) #A4 landscape

for index, (name, filename) in enumerate(entries):
    record = SeqIO.read(filename,"genbank")
    length = len(record)
    features = [f for f in record.features if f.type=="gene"]
    #Record an Artemis style integer color in the feature's qualifiers,
    #1 = Black, 2 = Red, 3 = Green, 4 = blue, 5 =cyan, 6 = purple
    for f in features: f.qualifiers["color"] = [index+2]

    cur_chromosome = BasicChromosome.Chromosome(name)
    #Set the scale to the MAXIMUM length plus the two telomeres in bp,
    #want the same scale used on all five chromosomes so they can be
    #compared to each other
    cur_chromosome.scale_num = max_len + 2 * telomere_length

    #Add an opening telomere
    start = BasicChromosome.TelomereSegment()
    start.scale = telomere_length
    cur_chromosome.add(start)

    #Add a body - again using bp as the scale length here.
    body = BasicChromosome.AnnotatedChromosomeSegment(length, features)
    body.scale = length
    cur_chromosome.add(body)

    #Add a closing telomere
    end = BasicChromosome.TelomereSegment(inverted=True)
    end.scale = telomere_length
    cur_chromosome.add(end)

    #This chromosome is done
    chr_diagram.add(cur_chromosome)

chr_diagram.draw("pfal_chrom.pdf", "Plasmodium falcipirum 3D7")