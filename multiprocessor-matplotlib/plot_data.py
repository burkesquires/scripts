#!/usr/bin/env python

import logging

module_logger = logging.getLogger('plotData module')

def parse_data(data, log_scale):
    from copy import deepcopy
    import numpy as np
    logging.info("Position data to parse: " + str(data))
    new_dict = deepcopy(data)
    new_dict.pop('data', None)
    x1 = np.asarray(new_dict.keys())
    y1 = np.asarray([v['as1'] for k, v in new_dict.items() if v['as1'] > 0])
    y2 = np.asarray([v['as2'] for k, v in new_dict.items() if v['as1'] > 0])
    ratio1 = np.asarray([v['ratio'] for k, v in new_dict.items() if v['as1'] > 0])
    if log_scale:
        error1 = np.asarray([v['se1'] for k, v in new_dict.items() if v['as1'] > 0])
        error2 = np.asarray([v['se2'] for k, v in new_dict.items() if v['as1'] > 0])
    else:
        error1 = np.asarray([v['sd1'] for k, v in new_dict.items() if v['as1'] > 0])
        error2 = np.asarray([v['sd2'] for k, v in new_dict.items() if v['as1'] > 0])
    if x1.size == y1.size == y2.size == ratio1.size == error1.size == error2.size:
        return x1, y1, y2, ratio1, error1, error2
    else:
        logging.error("Error plotting data: " + str(new_dict))


def plot_data(file_name, data_dict, assay1, assay2, num_of_rows, num_of_cols, threshold, graph_axis_increment, log_scale=False, default_layout=False):
    from matplotlib.backends.backend_pdf import PdfPages
    from matplotlib import pyplot as plot
    import numpy.ma as ma
    logging.info('Starting plotData.')
    # Generate the pages
    #num_plots = data_dict.check_threshold('ratio', threshold, 'both')
    num_plots_per_page = num_of_rows * num_of_cols
    i = 0
    row = 0
    col = 0
    p1 = []
    p3 = []
    page = 1
    font = {'size': 8}
    previous_length = graph_axis_increment
    plot.rc('font', **font)
    #pdf_pages = PdfPages(file_name + '.pdf')
    # sort by the length of each bin; an approximate for the length of each sequence
    #for temp in sorted(dictAccessionSize.items(), key=lambda x: x[1]):
    data_list = data_dict.remove_below_threshold(threshold, 'ratio')
    num_plots = data_dict.get_count_above_threshold()
    for accession in data_list:
        data = data_dict.get_value(accession)
        length = data_dict.get_value(accession, 'data', 'length')
        if data != '':
            if log_scale:
                axis_bins = 'x'
            else:
                axis_bins = 'both'
            plot.locator_params(axis=axis_bins, nbins=5)
            # if previous page is full start a new page
            if i % num_plots_per_page == 0:
                fig = plot.figure(figsize=(8.27, 11.69), dpi=100)
            bin_length = data_dict.get_value(accession, 'data', 'bin_size')
            if previous_length != bin_length:
                if not default_layout: plot.tight_layout( h_pad=0.5)
                plot.savefig('./output/%s-%s.pdf' % (file_name, page))
                page += 1
                #pdf_pages.savefig(fig)
                row = 0
                col = 0
                fig = plot.figure(figsize=(8.27, 11.69), dpi=100)
                i = 0
            ax = plot.subplot2grid((num_of_rows, num_of_cols), (row, col))
            
			x1, y1, y2, ratios, error1, error2 = parse_data(data, log_scale)
            mask1 = ma.where(y1 >= y2)
            mask2 = ma.where(y2 >= y1)
            error_config1 = { 'capsize': 1, 'makeredgewidth':0.5, 'ecolor': 'k', 'elinewidth': 0.01, 'linestyle': 'dashed'}
            error_config2 = { 'capsize': 1, 'makeredgewidth':0.5, 'ecolor': '#B6B6B4', 'elinewidth': 0.01, 'linestyle': 'dashed'}
            logging.info('x1: %s\ny1: %s\n, y2: %s\n, error 1: %s\nerror 2: %s\n' % (x1, y1, y2, error1, error2))
            # Plot the data using masks to make suresmaller values are always in front


            if mask1[0].size != 0:
                p1 = plot.bar(x1[mask1], y1[mask1], align='center', color='r', edgecolor='r', log=log_scale, yerr=error1[mask1], error_kw=error_config1)
            p2 = plot.bar(x1, y2, align='center', color='b' , edgecolor='b', log=log_scale, yerr=error2, error_kw=error_config2)
            if mask2[0].size != 0:
                p3 = plot.bar(x1[mask2], y1[mask2], align='center', color='r', edgecolor='r', log=log_scale, yerr=error1[mask2], error_kw=error_config1)
            plot.title(data_dict.get_value(accession, 'data','title'), font)
            plot.xlim(0, bin_length)
            plot.ylim(0)
            if col == 0: plot.ylabel('Intensity', font)
            # Create the legend
            plot.axvline(x=length, color='k', linestyle=':')
            protein_length_artist = plot.Line2D((0, 1), (0, 0), color='k', marker='', linestyle=':')
            bin_size_artist = plot.Line2D((0, 1), (0, 0), color='k', marker='', linestyle='')
            if len(p1) > 0:
                plot.figlegend((p1[0], p2[0], protein_length_artist, bin_size_artist),
                               (assay1, assay2, 'End of protein', 'Bin size: %s' % bin_length), 'lower center', ncol=4)
            else:
                plot.figlegend((p3[0], p2[0], protein_length_artist, bin_size_artist),
                               (assay1, assay2, 'End of protein', 'Bin size: %s' % bin_length), 'lower center', ncol=4)
            if col == (num_of_cols - 1):
                col = 0
                row += 1
            else:
                col += 1
            i += 1
            if (i % num_plots_per_page) == 0 or (i == num_plots):
                #plot.tight_layout()
                plot.savefig('./output/%s-%s.pdf' % (file_name, page))
                page += 1
                #pdf_pages.savefig(fig)
                col = 0
                row = 0
            previous_length = bin_length
        else:
            logging.debug("%s not found in data dictionary." % temp[0])
    logging.info("Finished plotting data.")
    #pdf_pages.close()
