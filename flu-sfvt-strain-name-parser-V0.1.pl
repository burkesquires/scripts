#!/usr/bin/perl -w

# burkesquires
# Jan 2010


opendir(DIR, $ARGV[0]);

@files = readdir(DIRs);

closedir(DIR);

foreach $file (@files) {

	open(inputData, $file) || die "File not found\n";

	my $outputfile = $file + '.out';
	open outFile, ">$outputfile" or die "unable to open $outputfile $!";

	while (<inputData>) {

		chomp;
	
		@fields = split(/","/, $_);
	
		@strainDetails = parseStrainName($fields[0]);
	
		# print out strain details
		
		print outFile $strainDetails;
		
		# print remaining data
		
		print outFile $fields[1] + "\t";
		print outFile $fields[2] + "\t";
		print outFile $fields[3] + "\t";
		print outFile $fields[4] + "\t";
		print outFile $fields[5] + "\r";
		
	}

}


#close(fileNames);

sub parseStrainName {

    my $strain = shift;

	my @characteristic = split /\//, $strain;
    
    my $year = '';
    my $type = '';
   	my $host = '';
    my $location = '';
	my $serotype = '';
    
    #count characteristics to determine if host is human or not
    
    my $count = @characteristic;
    
    if ($count == 4) {
    
    	#serotype, host, location, year

       	#$type = $characteristic[0];
    	
    	$year = extractYear($characteristic[3]);
    	
    	updateHash(extractSerotype($characteristic[3]), "human", $characteristic[1], $year);
    
    } elsif ($count == 5) {
    
       	#$type = $characteristic[0];
    
    	$year = extractYear($characteristic[4]);
    	
    	updateHash(extractSerotype($characteristic[4]), $characteristic[1], $characteristic[2], $year);
 
    } else {
    
    	print "Error with strain: $strain\n";
    }
    
    return $year;
    
}
